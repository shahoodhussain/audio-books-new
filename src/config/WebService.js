import _ from "lodash";
import Util from "../util";
export const BASE_URL = "https://server.appsstaging.com/2522/audio_books/api/";
// export const ASSETS_URL = "https://dev76.onlinetestingserver.com/";
export const API_TIMEOUT = 20000;
// export const NEW_API_KEY = "1d399038bef14b0497d028fc27999696";

export const API_LOG = true;

export const ERROR_SOMETHING_WENT_WRONG = {
  message: "Something went wrong, Please try again later",
  error: "Something went wrong, Please try again later"
};
export const ERROR_NETWORK_NOT_AVAILABLE = {
  message: "Please connect to the working Internet",
  error: "Please connect to the working Internet"
};
export const ERROR_TOKEN_EXPIRE = {
  message: "Session Expired, Please login again!",
  error: "Session Expired, Please login again!"
};
export const ERROR_CANCEL_ERROR = {
  message: "Upload cancelled",
  error: "Upload cancelled"
};

export const REQUEST_TYPE = {
  GET: "get",
  POST: "post",
  DELETE: "delete",
  PUT: "put"
};

// API USER ROUTES
export const SIGNUP_URL = {
  route: "auth/signup",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};
export const VERIFY_USER_URL = {
  route: "verify-otp",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};
export const RESEND_CODE_URL = {
  route: "auth/signup/resend-otp",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};
export const COMPLETE_PROFILE_URL = {
  route: "profile",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
export const VERIFY_PASSWORD_OTP_URL = {
  route: "auth/forgot-password",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};
export const FORGOT_PASSWORD_URL = {
  route: "auth/password/forgot",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};
export const UPDATE_PASSWORD_URL = {
  route: "auth/password/reset",
  access_token_required: false,
  type: REQUEST_TYPE.POST
}; 
export const LOGIN_URL = {
  route: "auth/login",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};
export const LOGOUT_URL = {
  route: "auth/logout",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
export const PROFILE_URL = {
  route: "auth/profile",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
const EDIT_PROFILE_URL = {
  route: "profile",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
export const CHANGE_PASSWORD_URL = {
  route: "auth/password/change",
  access_token_required: true,
  type: REQUEST_TYPE.POST
}; 
export const APP_USAGE_CONTENT_URL = {
  route: "content",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
export const GET_BOOKS_URL = {
  route: "home",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
export const GET_CATEGORIES_URL = {
  route: "categories",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
const GET_CATEGORIES_BOOKS_URL = {
  route: "books",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
const BOOKMARK = {
  route: "books/save",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
const GET_ALL_CARDS = {
  route: "card/list",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
const ADD_CARD = {
  route: "card/add",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
const CHECKOUT = {
  route: "checkout",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
const DELETE_CARD = {
  route: "card/delete",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};

export const callRequest = function(
  url,
  data,
  parameter,
  header = {},
  ApiSauce,
  baseUrl = BASE_URL
) {
  // note, import of "ApiSause" has some errors, thats why I am passing it through parameters

  let _header = header;
  if (url.access_token_required) {
    const _access_token = Util.getCurrentUserAccessToken();
    if (_access_token) {
      _header = {
        ..._header,
        ...{
          Authorization: `Bearer ${_access_token}`
        }
      };
    }
  }

  const _url =
    parameter && !_.isEmpty(parameter)
      ? `${url.route}?${parameter}`
      : url.route;

  if (url.type === REQUEST_TYPE.POST) {
    return ApiSauce.post(_url, data, _header, baseUrl);
  } else if (url.type === REQUEST_TYPE.GET) {
    return ApiSauce.get(_url, data, _header, baseUrl);
  } else if (url.type === REQUEST_TYPE.PUT) {
    return ApiSauce.put(_url, data, _header, baseUrl);
  } else if (url.type === REQUEST_TYPE.DELETE) {
    return ApiSauce.delete(_url, data, _header, baseUrl);
  }
  // return ApiSauce.post(url.route, data, _header);
};

export default {
  SIGNUP_URL,
  RESEND_CODE_URL,
  VERIFY_USER_URL,
  COMPLETE_PROFILE_URL,
  FORGOT_PASSWORD_URL,
  LOGIN_URL,
  UPDATE_PASSWORD_URL,
  LOGOUT_URL,
  APP_USAGE_CONTENT_URL,
  CHANGE_PASSWORD_URL,
  GET_BOOKS_URL,
  GET_CATEGORIES_URL,
  GET_CATEGORIES_BOOKS_URL,
  BOOKMARK,
  GET_ALL_CARDS,
  ADD_CARD,
  CHECKOUT,
  DELETE_CARD,
  EDIT_PROFILE_URL
}