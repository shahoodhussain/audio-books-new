// @drawer images
const drawerIcon = require("../../assets/images/drawer/drawer_icon.png");
const favorites = require("../../assets/images/drawer/favorites.png");
const settings = require("../../assets/images/drawer/settings.png");
const Address = require("../../assets/images/drawer/address.png");
const EditPencil = require("../../assets/images/drawer/edit.png");
const Home = require("../../assets/images/tabs/home.png");
const Logout = require("../../assets/images/drawer/logout.png");
const Order = require("../../assets/images/drawer/order.png");
const Payment = require("../../assets/images/drawer/payment.png");
const PrivacyPolicy = require("../../assets/images/drawer/privacy_policy.png");
const TermsCondition = require("../../assets/images/drawer/terms_condition.png");

// user avatar
const avatar = require("../../assets/images/user/avatar.png");

// @login screen images
const closeEye = require("../../assets/images/login/close_eye.png");
const openEye = require("../../assets/images/login/open_eye.png");
const PasswordLock = require("../../assets/images/login/password_lock.png");

//@splash
const Splash = require("../../assets/images/general/splash.jpg");

//@app background image
const AppBackground = require("../../assets/images/general/app_background.png");
const AppLogo = require("../../assets/images/general/app_logo.png");

//@app starter 
const Email = require("../../assets/images/login/email.png");
const Google = require("../../assets/images/login/google.png");
const Facebook = require("../../assets/images/login/fb.png");
const Apple = require("../../assets/images/login/apple.png");

//@login agreement modal
const CheckMark = require("../../assets/images/general/check_mark.png");
const EmptyBox = require("../../assets/images/general/empty_box.png");

//@navigation general images
const HeaderBack = require("../../assets/images/navigation/back.png");

//@general
const Camera = require("../../assets/images/general/camera.png");
const Phone = require("../../assets/images/general/phone.png");
const User = require("../../assets/images/tabs/user.png");
const Pin = require("../../assets/images/general/pin.png");
const Restaurant = require("../../assets/images/general/restaurant.png");
const Search = require("../../assets/images/general/search.png");
const Call = require("../../assets/images/general/call.png");
const Close = require("../../assets/images/general/close.png");

//@cart images
const Cart = require("../../assets/images/cart/cart.png");
const Voucher = require("../../assets/images/cart/voucher.png");

//@home images
const FastFood = require("../../assets/images/home/fast_food.png");
const SeaFood = require("../../assets/images/home/sea_food.png");
const MexicanFood = require("../../assets/images/home/maxican_food.png");
const Dessert = require("../../assets/images/home/Deseart.png");
const BreakFast = require("../../assets/images/home/breakfast.png");
const Chicken = require("../../assets/images/home/chicken.png");
const Burgur = require("../../assets/images/home/burgers.png");
const SandWich = require("../../assets/images/home/sandwiches.png");
const Restaurant1 = require("../../assets/images/home/restaurant1.png");
const Restaurant2 = require("../../assets/images/home/restaurant2.png");
const Restaurant3 = require("../../assets/images/home/restaurant3.png");
const Restaurant4 = require("../../assets/images/home/restaurant4.png");
const StarSelected = require("../../assets/images/home/star_yellow.png");
const StartDisable = require("../../assets/images/home/star_gray.png");
const Online = require("../../assets/images/home/online.png");


const Product1 = require("../../assets/images/restaurantproducts/Products1.png");
const Product2 = require("../../assets/images/restaurantproducts/Products2.png");
const Product3 = require("../../assets/images/restaurantproducts/Products3.png");
const Product4 = require("../../assets/images/restaurantproducts/Products4.png");
const ButtonSelected = require("../../assets/images/restaurantproducts/button_selected.png");
const ButtonUnselected = require("../../assets/images/restaurantproducts/button_unselected.png");

const Trash = require("../../assets/images/general/trash.png");

//@checkout 
const Check = require("../../assets/images/checkout/check.png");
const MasterCard = require("../../assets/images/checkout/master_card.png");
const VisaCard = require("../../assets/images/checkout/visa_card.png");

const Clander = require("../../assets/images/payment/calender.png");
const Card = require("../../assets/images/payment/card.png");
const Cvv = require("../../assets/images/payment/cvv.png");

const BookMark = require("../../assets/images/home/bookmark.png");
const Download = require("../../assets/images/home/direct-download.png");
const Play = require("../../assets/images/home/play.png");

const Books = require("../../assets/images/tabs/books.png");
const Categories = require("../../assets/images/tabs/categories.png");
const Library = require("../../assets/images/tabs/library.png");

//@player images
const Forward = require("../../assets/images/player/forward.png");
const Rewind = require("../../assets/images/player/rewind.png");
const Options = require("../../assets/images/player/options.png");
const SeekRight = require("../../assets/images/player/seek_right.png");
const SeekLeft = require("../../assets/images/player/seek_left.png");

const Edit = require("../../assets/images/user/edit.png");
const Avatar = require("../../assets/images/user/profile.png");
const Pause = require("../../assets/images/player/pause.png");

const Visa = require("../../assets/images/card/visa.png");

export default {

  //home
  FastFood,
  SeaFood,
  MexicanFood,
  Dessert,
  BreakFast,
  Chicken,
  Burgur,
  SandWich,
  Restaurant1,
  Restaurant2,
  Restaurant3,
  Restaurant4,
  StarSelected,
  StartDisable,
  Online,
  ButtonSelected,
  ButtonUnselected,

  Product1,
  Product2,
  Product3,
  Product4,

  Check,
  Edit,
  MasterCard,
  VisaCard,

  // drawer icons
  drawerIcon,
  settings,
  favorites,

  // user images
  avatar,

  // login images
  openEye,
  closeEye,
  PasswordLock,

  Splash,
  AppBackground,
  AppLogo,
  Email,
  Google,
  Facebook,
  Apple,
  Camera,
  Phone,
  User,

  CheckMark,
  EmptyBox,
  HeaderBack,
  Cart,
  Pin,
  Restaurant,
  Search,
  Trash,
  Voucher,
  Call,
  Close,

  Address,
  EditPencil,
  Home,
  Logout,
  Order,
  Payment,
  PrivacyPolicy,
  TermsCondition,
  Clander,
  Card,
  Cvv,

  Download,
  Play,
  BookMark,
  Library,
  Books,
  Categories,

  Forward,
  Rewind,
  SeekLeft,
  SeekRight,
  Options,
  Edit,
  Avatar,
  Pause,
  Visa
}
