import { Dimensions, Platform, PixelRatio } from "react-native";

const { width, height } = Dimensions.get("window");

const screenWidth = width; // < height ? width : height;
const screenHeight = height; // < height ? height : width;

// const isIphoneX = height >= 812 && Platform.OS === "ios";

const guidelineBaseWidth = 375;
const guidelineBaseHeight = 667;

const scale = size => (screenWidth / guidelineBaseWidth) * +size;
const scaleVertical = size => (screenHeight / guidelineBaseHeight) * size;
const fontScaling = screenWidth / 320;

const ratio = (iosSize, androidSize) =>
	Platform.select({
		ios: scaleVertical(iosSize),
		android: androidSize || iosSize
	});

const generatedFontSize = (iosFontSize, androidFontSize) =>
	Platform.select({
		ios: scale(iosFontSize),
		android: androidFontSize || iosFontSize
	});

const normalize = (size) => {
	const newSize = size * fontScaling 
	if (Platform.OS === 'ios') {
		return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 4
	} else {
		return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
	}
}

export default {
	ratio,
	normalize,
	screenWidth,
	screenHeight,
	generatedFontSize,
	smallMargin: ratio(8),
	baseMargin: ratio(16),
	doubleBaseMargin: ratio(32),
	tabBarHeight: 49, // Default tab bar height in iOS 10 (source react-navigation)
	defaultUIHeight: ratio(45),
	viewMaxWidthForLandscape: ratio(400),
	icon: {
		tiny: ratio(8),
		small: ratio(16),
		normal: ratio(24), // Default tab icon size (source react-navigation)
		medium: ratio(32),
		large: ratio(40),
		xLarge: ratio(50),
		xxLarge: ratio(60),
		xxxLarge: ratio(100)
	},
	image: {
		small: ratio(20),
		medium: ratio(40),
		large: ratio(60),
		logImage: ratio(60),
		coverWidth: screenWidth,
		coverHeight: screenWidth / 2
	}
};
