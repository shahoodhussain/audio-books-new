const theme = {
    primary: '#d60f23',
    primaryDark: '#231f20',
    fontBlack: '#1B1B1D',
    fontWhite: '#fff',
    secondary:'#dfe1ef',
    // primaryBackground: '#fcbe00',
    primaryBackground: 'rgb(10,46,36)',
    tabBackground: 'rgba(235,133,46,0.3)',
    heading: '#fff',
    headingLight: '#000',
    text: '#575757',
    textLight: '#999',
    error: '#f04',
    shadow: '#333333',
    success: '#00e676',
    facebookColor: '#314c8c',
    googleColor: '#dc1c2f',
    gradient: {
        firstColor: '#0033E9',
        secondColor: '#366ECE',
        thirdColor: '#BD10E0',
        gradientGreen: '#366ECE'
    },
    borderDashedColor: '#fff',
    white: '#ffffff',
    //green border
    checkMarkGreenBorder: '#3BB54A',
    inputPlaceHolderColor: '#CBCBCB',
    pinkColor: '#BD10E0',
    lightTextColor: '#464646',
    lightText: '#00000070',
    black: '#000000',
    transparent: 'transparent',
    rgbaWhite: 'rgba(255,255,255,0.85)',
    rippleColorAndroid: '#d60f23',
    red: '#d8002a',
    yellowBackground: '#ff9f32',
    themeFontColor: 'rgba(235,133,46,1)',
};

export const darkTheme = {};

export default theme;