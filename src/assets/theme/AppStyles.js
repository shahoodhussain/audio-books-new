import {StatusBar} from "react-native";

const shadow1 = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.18,
  shadowRadius: 1.00,
  elevation: 1,
};
const shadow2 = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.20,
  shadowRadius: 1.41,
  elevation: 2,
};
const shadow3 = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.22,
  shadowRadius: 2.22,
  elevation: 3,
};
const shadow4 = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.62,
  elevation: 4,
};
const shadow5 = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,
  elevation: 5,
};
const shadow6 = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 3,
  },
  shadowOpacity: 0.27,
  shadowRadius: 4.65,
  elevation: 6,
};
const shadow7 = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 3,
  },
  shadowOpacity: 0.29,
  shadowRadius: 4.65,
  elevation: 7,
};

const emptyListContainerStyle = {
  flex: 1,
  justifyContent: "center",
  alignItems: "center"
};

// styleConstantValue
const constant = {
  formInputSize: 17,
  labelSize: 15
};
const letterSpace3 = {
  letterSpacing: 3
};

const materialInput = {
  marginTop: 5,
  // fontFamily: "Lato-Bold",
  // color: Colors.text.primary
};
const separator = {
  borderBottomWidth: 1,
  // borderColor: Colors.gary9
};

const logoImage = { width: 96, height: 112 };

const textInputMaterialProps = {
  autoCorrect: false,
  // selectionColor: Colors.grey3,
  autoCapitalize: "sentences",
  enablesReturnKeyAutomatically: true,
  // tintColor: Colors.text.primary,
  // baseColor: Colors.text.primary,
  fontSize: constant.formInputSize,
  labelFontSize: constant.labelSize,
  style: materialInput
};

const forwardIcon = {
  width: 9,
  height: 15
};

const textInputMaterialProps2 = {
  autoCorrect: false,
  // selectionColor: Colors.grey3,
  autoCapitalize: "sentences",
  enablesReturnKeyAutomatically: true,
  // tintColor: Colors.text.primary,
  // baseColor: Colors.text.primary,
  fontSize: constant.formInputSize,
  labelFontSize: constant.labelSize,
  style: materialInput,
  // inputContainerStyle: materialInputContainer
};

const textInputMaterialNoUndelineProps = {
  inputContainerStyle: { borderBottomWidth: 0 }
};

const statusBarHeight = StatusBar.currentHeight;

export default {
  shadow1,
  shadow2,
  shadow3,
  shadow4,
  shadow5,
  shadow6,
  shadow7,
  emptyListContainerStyle,

  // styleConstantValue
  constant,
  letterSpace3,

  // styles
  logoImage,
  materialInput,
  textInputMaterialProps,
  textInputMaterialProps2,
  textInputMaterialNoUndelineProps,
  forwardIcon,
  separator,
  statusBarHeight
};
