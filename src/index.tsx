import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import configureStore from './store';
import Router from './router';
import DropdownAlert from 'react-native-dropdownalert';
import { AlertHelper } from './services/AlertService';
import Util from './util';
import { StripeProvider } from '@stripe/stripe-react-native';

const reducers = require('./redux/reducers').default;

export default class Main extends PureComponent {
    state = {
        isLoading: true,
        store: configureStore(reducers, () => {
            this.setState({ isLoading: false }, () => Util.setStore(this.state.store));
        }),
    };

    // @function render drop down alert
    // @desc setup alert box to call in app
    _setupDropDownRender() {
        return (
            <DropdownAlert
                defaultContainer={{
                    padding: 8,
                    flexDirection: 'row',
                    paddingTop: Util.isPlatformAndroid() ? 22 : 8
                }}
                translucent={false}
                updateStatusBar={false}
                closeInterval={3000}
                showCancel={true}
                messageNumOfLines={6}
                ref={ref => AlertHelper.setDropDown(ref)}
                onClose={() => AlertHelper.invokeOnClose()}
            />
        );
    }

    render() {
        return (
            <StripeProvider
                publishableKey="pk_test_51H0UoCJELxddsoRYqANwUqQLd24vQYATeVTsN7Sm1xnAD68ARNm6bK0vsdCSqisOhSMNCATShUvDmXdzeyW0Cezz00RbGzoMup"
                // urlScheme="your-url-scheme" // required for 3D Secure and bank redirects
                // merchantIdentifier="merchant.com.{{YOUR_APP_NAME}}" // required for Apple Pay
            >
                {!this.state.isLoading && (
                    <Provider store={this.state.store}>
                        <Router />
                    </Provider>
                )}
                {this._setupDropDownRender()}
            </StripeProvider>
        );
    }
}
