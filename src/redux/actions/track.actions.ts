import ActionTypes from "../ActionTypes";

export function saveLastState(state: Object) {
    return {
        type: ActionTypes.SAVE_LAST_STATE,
        state,
    }
}

export function saveDownloadedChapter(chapter: Object) {
    return {
        type: ActionTypes.SAVE_CHAPTER,
        chapter
    }
}