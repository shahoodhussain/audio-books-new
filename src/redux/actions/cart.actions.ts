import ActionTypes from "../ActionTypes";

export function addBookToCart(book: Object) {
    return {
        book,
        type: ActionTypes.ADD_TO_CART
    }
}

export function clearCartData() {
    return {
        type: ActionTypes.CLEAR_CART_DATA
    }
}