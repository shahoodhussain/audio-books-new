import ActionTypes from "../ActionTypes";

export function getPaymentCards(responseCallBack: Function) {
    return {
        responseCallBack,
        type: ActionTypes.GET_PAYMENT_CARD.REQUEST
    }
}

export function addCard(cardToken: String, responseCallBack: Function) {
    return {
        cardToken,
        responseCallBack,
        type: ActionTypes.ADD_CARD
    }
}

export function checkout(card_id: String, books: [], responseCallBack: Function) {
    return {
        books,
        card_id,
        responseCallBack,
        type: ActionTypes.CHECKOUT.REQUEST,
    }
}

export function deleteCard(cardId: String, responseCallBack: Function) {
    return {
        cardId,
        responseCallBack,
        type: ActionTypes.DELETE_CARD,
    }
}
