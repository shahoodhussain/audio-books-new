import ActionTypes from "../ActionTypes";

export function loadingState() {
    return {
        loading: false,
        type: ActionTypes.LOADING_STATE
    }
}

export function fetchCategories(offset: Number, responseCallBack: Function) {
    return { type: ActionTypes.GET_CATEGORIES.REQUEST, offset, responseCallBack };
}

export function receivedCategories(data: Object, offset: Number) {
    return { type: ActionTypes.GET_CATEGORIES.SUCCESS, data, offset };
}
