import ActionTypes from "../ActionTypes";
import { AppInfoActionTypes, loadingActionInterface, APP_INFO_SHOW_LOADING, PRIVACY_POLICY, privacyActionInterface, APP_USAGE_POLICIES, contentTypeInterface } from "../interfaces";

export function loadingAction(payload: loadingActionInterface): AppInfoActionTypes {
  return {
    type: APP_INFO_SHOW_LOADING,
    payload,
  };
}

export function privacyRequest(payload: privacyActionInterface): AppInfoActionTypes {
  return {
    type: PRIVACY_POLICY,
    payload,
  };
}

export function appUsageContentRequest( contentType: String ): AppInfoActionTypes {
  return {
    type: ActionTypes.APP_USAGE_POLICIES.REQUEST,
    content: contentType
  }
}

export function appUsageContentSuccess(data: String): AppInfoActionTypes {
  return {
    type: ActionTypes.APP_USAGE_POLICIES.SUCCESS,
    content: data
  }
}
export function appUsageContentFailure() {
  return {
    type: ActionTypes.APP_USAGE_POLICIES.SUCCESS,
  }
}
