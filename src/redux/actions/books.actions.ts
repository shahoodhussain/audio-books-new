import ActionTypes from "../ActionTypes";

export function loadingState() {
    return {
        loading: false,
        type: ActionTypes.LOADING_STATE
    }
}

export function fetchHomeBooks() {
    return { type: ActionTypes.GET_ALL_BOOKS.REQUEST };
}

export function receivedBooks(data: Object) {
    return { type: ActionTypes.GET_ALL_BOOKS.SUCCESS, data };
}

export function fetchBooksByCategory(offset: Number, categoryID: Number, searchQuery: String, responseCallBack: Function) {
    return { type: ActionTypes.GET_CATEGORY_BOOKS.REQUEST, responseCallBack, categoryID, offset, searchQuery };
}

export function bookmarkAction(bookId: Number, responseCallBack: Function) {
    return {
        type: ActionTypes.SAVE_BOOKMARK.REQUEST,
        bookId,
        responseCallBack
    }
}

export function getLibraryBooks(offset: Number, responseCallBack: Function) {
    return {
        type: ActionTypes.GET_LIBRARY.REQUEST,
        offset,
        responseCallBack
    }
}

export function getPurchasedBooks(offset: Number, responseCallBack: Function) {
    return {
        type: ActionTypes.GET_PURCHASED_BOOKS,
        offset,
        responseCallBack
    }
}