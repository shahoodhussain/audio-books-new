import ActionTypes, {
  VERIFY_PASSWORD_OTP,
  PROFILE,
  CLEAR_USER_TEMP_DATA,
} from "../ActionTypes";
import { completeProfileRequestInterface, forgotPasswordRequestInterface, loginRequestInterface, UserActionTypes, verifyUserSuccessInterface } from "../interfaces";

export function loadingState() {
  return {
    loading: false,
    type: ActionTypes.LOADING_STATE
  }
}

// logout request actions
export function userLogoutRequest() {
  return {
    type: ActionTypes.LOGOUT.REQUEST
  };
}
export function userLogoutSuccess() {
  return {
    type: ActionTypes.LOGOUT.SUCCESS
  };
}

// user signup request actions
export function registerUserRequest(payload: UserActionTypes) {
  return {
    payload,
    type: ActionTypes.REGISTER_USER.REQUEST
  };
}
export function registerUserSuccess(data: Object) {
  return {
    data,
    type: ActionTypes.REGISTER_USER.SUCCESS
  };
}

// signup otp verification request actions
export function verifyUserRequest(payload: UserActionTypes, isResetingPassword: Boolean) {
  return {
    payload,
    isResetingPassword,
    type: ActionTypes.VERIFY_USER.REQUEST
  };
}
export function verifyUserSuccess(data: verifyUserSuccessInterface, resetPassword: Boolean): UserActionTypes {
  return {
    data: data.data,
    bearer_token: data.bearer_token,
    resetPassword,
    type: ActionTypes.VERIFY_USER.SUCCESS
  };
}

// resend verification code request actions
export function resendVerificationCodeRequest(email: UserActionTypes) {
  return {
    email,
    type: ActionTypes.RESEND_VERIFICATION_CODE.REQUEST
  };
}
export function resendVerificationCodeFailure() {
  return {
    type: ActionTypes.RESEND_VERIFICATION_CODE.FAILURE
  };
}

// forgot password request actions
export function forgotPasswordRequest(payload: forgotPasswordRequestInterface) {
  return {
    payload,
    type: ActionTypes.FORGOT_PASSWORD.REQUEST
  };
}
export function forgotPasswordSuccess(data: Object) {
  return {
    data,
    type: ActionTypes.FORGOT_PASSWORD.SUCCESS
  };
}

// clear user data after reset password
export function clearTempUserData() {
  return {
    type: CLEAR_USER_TEMP_DATA
  };
}
// verify forgot password opt request
export function verifyPasswordOtpRequest(payload: Object, responseCallback: Function) {
  return {
    payload,
    responseCallback,
    type: VERIFY_PASSWORD_OTP.REQUEST
  };
}
// update password request 
export function updatePasswordRequest(payload: Object) {
  return {
    payload,
    type: ActionTypes.UPDATE_PASSWORD.REQUEST
  };
}

// user login request actions
export function userLoginRequest(payload: loginRequestInterface) {
  return {
    payload,
    type: ActionTypes.LOGIN.REQUEST
  };
}
export function userLoginSuccess(data: Object, access_token: string) {
  return {
    data,
    access_token,
    type: ActionTypes.LOGIN.SUCCESS
  };
}

// complete user profile request actions
export function completeProfileRequest(payload: completeProfileRequestInterface, authToken: String) {
  return {
    payload,
    authToken,
    type: ActionTypes.COMPLETE_PROFILE.REQUEST
  };
}
export function completeProfileSuccess(data: Object, access_token: string) {
  return {
    data,
    access_token,
    type: ActionTypes.COMPLETE_PROFILE.SUCCESS
  };
}
export function completeProfileFailure() {
  return {
    type: ActionTypes.COMPLETE_PROFILE.FAILURE
  };
}

// change password request actions
export function changePasswordRequest(payload: Object) {
  return {
    payload,
    type: ActionTypes.CHANGE_PASSWORD.REQUEST
  };
}

// user profile request actions
export function userProfileRequest(responseCallback: Function) {
  return {
    responseCallback,
    type: PROFILE.REQUEST
  };
}
export function userProfileSuccess(data: Object) {
  return {
    data,
    type: PROFILE.SUCCESS
  };
}
// edit user profile request actions
export function editUserProfileRequest(payload: Object, responseCallback: Function) {
  return {
    payload,
    responseCallback,
    type: ActionTypes.EDIT_PROFILE.REQUEST
  };
}

export function editUserProfileSuccess(data: Object) {
  return {
    data,
    type: ActionTypes.EDIT_PROFILE.SUCCESS
  };
}
