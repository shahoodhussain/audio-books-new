const REQUEST = "REQUEST";
const SUCCESS = "SUCCESS";
const CANCEL = "CANCEL";
const FAILURE = "FAILURE";

interface actionType {
  REQUEST: string,
  SUCCESS: string,
  CANCEL: string,
  FAILURE: string
}

function createRequestTypes(base: string) {
  const res: actionType = {
    REQUEST: undefined,
    SUCCESS: undefined,
    CANCEL: undefined,
    FAILURE: undefined
  };
  [REQUEST, SUCCESS, FAILURE, CANCEL].forEach(type => {
    res[type] = `${base}_${type}`;
  });
  return res;
}

export const NETWORK_INFO = "NETWORK_INFO";
export const SHOW_LOADING = "SHOW_LOADING";
export const LOADING_STATE = "APP_INFO_SHOW_LOADING";
export const PRIVACY_POLICY = "PRIVACY_POLICY";
export const CLEAR_USER_TEMP_DATA = "CLEAR_USER_TEMP_DATA";
export const APP_USAGE_POLICIES = createRequestTypes("APP_USAGE_POLICIES");

// USER ACTIONS
export const REGISTER_USER = createRequestTypes("USER_SIGNUP");
export const VERIFY_USER = createRequestTypes("VERIFY_USER");
export const VERIFY_PASSWORD_OTP = createRequestTypes("VERIFY_PASSWORD_OTP");
export const FORGOT_PASSWORD = createRequestTypes("FORGOT_PASSWORD");
export const RESEND_VERIFICATION_CODE = createRequestTypes("RESEND_VERIFICATION_CODE");
export const UPDATE_PASSWORD = createRequestTypes("UPDATE_PASSWORD");
export const LOGIN = createRequestTypes("LOGIN");
export const COMPLETE_PROFILE = createRequestTypes("COMPLETE_PROFILE");
export const RESET_PASSWORD = createRequestTypes("RESET_PASSWORD");
export const LOGOUT = createRequestTypes("LOGOUT");
export const PROFILE = createRequestTypes("USER_PROFILE");
export const CHANGE_PASSWORD = createRequestTypes("CHANGE_PASSWORD");
const EDIT_PROFILE = createRequestTypes("EDIT_PROFILE");

//NotificationActions
export const GET_ALL_NOTIFICATIONS = createRequestTypes("GET_ALL_NOTIFICATIONS");

const GET_ALL_BOOKS = createRequestTypes("GET_ALL_BOOKS");
const GET_CATEGORIES = createRequestTypes("GET_CATEGORIES");
const GET_CATEGORY_BOOKS = createRequestTypes("GET_CATEGORY_BOOKS");
const SAVE_BOOKMARK = createRequestTypes("SAVE_BOOKMARK");
const GET_LIBRARY = createRequestTypes("GET_LIBRARY");
const ADD_TO_CART = "ADD_TO_CART";
const GET_PAYMENT_CARD = createRequestTypes("GET_PAYMENT_CARD");
const ADD_CARD = "ADD_CARD";
const CHECKOUT = createRequestTypes("CHECKOUT");
const CLEAR_CART_DATA = "CLEAR_CART_DATA";
const DELETE_CARD = "DELETE_CARD";
const GET_PURCHASED_BOOKS = "GET_PURCHASED_BOOKS";
const SAVE_LAST_STATE = "SAVE_LAST_STATE";
const SAVE_CHAPTER = "SAVE_CHAPTER";

export default {
  LOADING_STATE,
  REGISTER_USER,
  RESEND_VERIFICATION_CODE,
  VERIFY_USER,
  COMPLETE_PROFILE,
  FORGOT_PASSWORD,
  LOGIN,
  UPDATE_PASSWORD,
  LOGOUT,
  APP_USAGE_POLICIES,
  CHANGE_PASSWORD,
  GET_ALL_BOOKS,
  GET_CATEGORIES,
  GET_CATEGORY_BOOKS,
  SAVE_BOOKMARK,
  GET_LIBRARY,
  ADD_TO_CART,
  GET_PAYMENT_CARD,
  ADD_CARD,
  CHECKOUT,
  CLEAR_CART_DATA,
  DELETE_CARD,
  GET_PURCHASED_BOOKS,
  SAVE_LAST_STATE,
  SAVE_CHAPTER,
  EDIT_PROFILE
}