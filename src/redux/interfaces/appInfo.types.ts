import ActionTypes from "../ActionTypes";

export const APP_INFO_SHOW_LOADING = "APP_INFO_SHOW_LOADING";
export const PRIVACY_POLICY = "PRIVACY_POLICY";
export const TERMS_CONDITION = "PRIVACY_POLICY";

export interface loadingInterface {
    isLoading: boolean
}

export interface loadingActionInterface {
    type: typeof APP_INFO_SHOW_LOADING,
    payload: loadingInterface
}

export interface privacyInterface {
    privacyData: string
}

export interface privacyActionInterface {
    type: typeof PRIVACY_POLICY,
    payload: privacyInterface
}

export interface contentTypeInterface {
    type: typeof ActionTypes.APP_USAGE_POLICIES.REQUEST
    content: String
}

export type AppInfoActionTypes = loadingActionInterface | privacyActionInterface | contentTypeInterface