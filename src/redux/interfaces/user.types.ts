import ActionTypes from "../ActionTypes";

export interface loadingInterface {
    type: typeof ActionTypes.LOADING_STATE
    loading: boolean
    data: null
}

export interface signupRequestInterface {
    type: typeof ActionTypes.REGISTER_USER.REQUEST
    loading: boolean,
    email: String,
    password: String
    data: null
}

export interface resendOtpRequest {
    type: typeof ActionTypes.RESEND_VERIFICATION_CODE.REQUEST
    email: String
    data: null
}

export interface verifyUser {
    type: typeof ActionTypes.VERIFY_USER.REQUEST
    otp_code: String
    otp_reference: String
    actionType: String
    data: null
}

export interface verifyUserSuccessInterface {
    type: typeof ActionTypes.VERIFY_USER.SUCCESS
    bearer_token: String
    resetPassword: Boolean
    data: {
        address: String
        email: String
        id: Number
        image: String
        is_social: Boolean
        is_verified: Boolean
        name: String
        phone_no: String
        profile_completed: Boolean
        social_type: String
    }
}

export interface completeProfileRequestInterface {
    type: typeof ActionTypes.COMPLETE_PROFILE.REQUEST
    name: String
    phone_no: String
    address: String
    image: String
}

export interface forgotPasswordRequestInterface {
    type: typeof ActionTypes.FORGOT_PASSWORD.REQUEST
    email: String
}

export interface loginRequestInterface {
    type: typeof ActionTypes.LOGIN.REQUEST
    email: String
    password: String
    device_type: String
    device_token: String
}

export type UserActionTypes = signupRequestInterface 
    | loadingInterface 
    | resendOtpRequest 
    | verifyUser 
    | verifyUserSuccessInterface
    | completeProfileRequestInterface
    | forgotPasswordRequestInterface
    | loginRequestInterface
