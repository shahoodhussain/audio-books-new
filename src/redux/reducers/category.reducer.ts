import ActionTypes from '../ActionTypes';

const initialState = {
    booksCategories: [],
    currentOffset: 0,
    fetching: false
}

export default (state = initialState, action: Object) => {
    switch (action.type) {
        case ActionTypes.GET_CATEGORIES.REQUEST:
            return state
        case ActionTypes.GET_CATEGORIES.SUCCESS: {
            return {
                ...state,
                booksCategories: [...state.booksCategories, ...action.data],
                currentOffset: action.offset,
                fetching: false
            }
        }
        case ActionTypes.LOADING_STATE:
            return { ...state, fetching: false };
        default:
            return state;
    }
}