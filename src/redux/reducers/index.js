import { combineReducers } from "redux";
import user from "./user";
import appInfo from "./appInfo.reducer";
import books from "./books.reducer";
import categories from "./category.reducer";
import cart from "./cart.reducer";
import trackState from "./track.reducer";

export default combineReducers({
  user,
  appInfo,
  books,
  categories,
  cart,
  trackState
})