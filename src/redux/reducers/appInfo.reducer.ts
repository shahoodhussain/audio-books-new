import Immutable from "seamless-immutable";
import ActionTypes from "../ActionTypes";
import { AppInfoActionTypes } from "../interfaces";

const initialState = Immutable({
  isLoading: false,
  content: "",
  termsData: "",
  privacyData: "",
  aboutData: "",
});

export default (state = initialState, action: AppInfoActionTypes) => {
  switch (action.type) {
    case ActionTypes.APP_USAGE_POLICIES.REQUEST: {
      return {
        ...state,
        isLoading: true
      }
    }
    case ActionTypes.APP_USAGE_POLICIES.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        content: action.content
      }
    }
    case ActionTypes.APP_USAGE_POLICIES.FAILURE: {
      return {
        ...state,
        isLoading: false
      }
    }
    default:
      return state;
  }
};
