import Immutable from 'seamless-immutable';
import ActionTypes from '../ActionTypes'

const initialState = Immutable({
    trackState: null,
    chapters: []
})

export default (state = initialState, action: Object) => {
    switch (action.type) {
        case ActionTypes.SAVE_LAST_STATE: 
            if( state.chapters.length === 0 ) {
                return Immutable.merge(state, {
                    chapters: [...state.chapters, {...action.state}]
                })
            }
            const isChapterAvailable = state.chapters.find(stateChapter => stateChapter.id === action.state.id);
            if(isChapterAvailable) {
                const newState = state.chapters.map(obj => isChapterAvailable.id === obj.id ? isChapterAvailable : obj);
                return Immutable.merge(state, {
                    chapters: newState
                })
            } else {
                return Immutable.merge(state, {
                    chapters: [...state.chapters, {...action.state}]
                })
            }
        case ActionTypes.SAVE_CHAPTER:
            const isDownloaded = state.chapters.find(stateChapter => stateChapter.id === action.chapter.id);
            if(isDownloaded) {
                const newState = state.chapters.map(obj => isDownloaded.id === obj.id ? action.chapter : obj);
                return Immutable.merge(state, {
                    chapters: newState
                })
            } else {
                return Immutable.merge(state, {
                    chapters: [...state.chapters, action.chapter]
                })
            }
        default:
            return state;
    }
}