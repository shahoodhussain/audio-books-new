import Immutable from 'seamless-immutable';
import ActionTypes from '../ActionTypes'

const initialState = Immutable({
    cartItems: [],
})

export default (state = initialState, action: Object) => {
    switch (action.type) {
        case ActionTypes.ADD_TO_CART:
            if(state.cartItems.length === 0) return {
                ...state,
                cartItems: [action.book]
            }
            const isBooksAdded = state.cartItems.find(element => element.id === action.book.id);
            if(isBooksAdded) return state;
            else {
                return {
                    ...state,
                    cartItems: [...state.cartItems, action.book]
                }
            }
        case ActionTypes.CLEAR_CART_DATA: 
            return {
                ...state,
                cartItems: []
            }
        default:
            return state;
    }
}