import Immutable from "seamless-immutable";
import ActionTypes, {
  PROFILE,
  RESET_PASSWORD,
  CLEAR_USER_TEMP_DATA,
} from "../ActionTypes";
import { UserActionTypes } from "../interfaces";

const initialState = Immutable({
  userData: null,
  access_token: null,
  loading: false,
  finish_tutorials: false,
  shouldAnimate: true,
  resetingPassword: false,
  isForgotPassword: false,
  userType: null,
  tempUserData: null
});

export default (state: Object = initialState, action: UserActionTypes) => {
  switch (action.type) {
    case ActionTypes.REGISTER_USER.REQUEST: {
      return Immutable.merge(state, {
        loading: true
      });
    }
    case ActionTypes.LOGOUT.REQUEST: {
      return { ...state, loading: true };
    }
    case ActionTypes.LOGOUT.SUCCESS: {
      return {
        ...initialState,
      }
    }
    case ActionTypes.VERIFY_USER.REQUEST: {
      return {
        ...state,
        loading: true
      };
    }
    case ActionTypes.VERIFY_USER.SUCCESS: {
      return {
        ...state,
        tempUserData: {
          ...action.data,
          bearer_token: action.bearer_token
        },
        loading: false
      };
    }
    case ActionTypes.RESEND_VERIFICATION_CODE.REQUEST: {
      return { ...state, loading: true };
    }
    case ActionTypes.LOGIN.REQUEST: {
      return { ...state, loading: true }
    }
    case ActionTypes.LOGIN.SUCCESS: {
      return Immutable.merge(state, {
        userData: action.data,
        access_token: action.access_token,
        loading: false
      });
    }
    case ActionTypes.COMPLETE_PROFILE.REQUEST: {
      return { loading: true };
    }
    case ActionTypes.COMPLETE_PROFILE.SUCCESS: {
      return {
        ...state,
        userData: action.data,
        access_token: action.access_token,
        loading: false
      };
    }
    case RESET_PASSWORD.FAILURE: {
      return Immutable.merge(state, {
        loading: false
      });
    }
    case RESET_PASSWORD.SUCCESS: {
      return Immutable.merge(state, {
        tempUserId: action.data.user_id,
        isForgotPassword: action.isResetPassword,
        loading: false
      });
    }
    case CLEAR_USER_TEMP_DATA: {
      return Immutable.merge(state, initialState);
    }
    case PROFILE.SUCCESS: {
      return Immutable.merge(state, {
        userData: action.data,
      });
    }
    case ActionTypes.EDIT_PROFILE.SUCCESS: {
      return Immutable.merge(state, {
        userData: {...state.userData, ...action.data},
      });
    }
    case ActionTypes.LOADING_STATE: {
      return {
        ...state,
        loading: action.loading
      }
    }
    case ActionTypes.FORGOT_PASSWORD.REQUEST: {
      return {
        ...state,
        loading: true
      }
    }
    case ActionTypes.UPDATE_PASSWORD.REQUEST: {
      return {
        ...state,
        loading: true
      }
    }
    case ActionTypes.CHANGE_PASSWORD.REQUEST: {
      return {
        ...state,
        loading: true
      }
    }
    default:
      return state;
  }
};