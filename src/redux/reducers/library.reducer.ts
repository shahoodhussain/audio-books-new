import Immutable from 'seamless-immutable';
import ActionTypes from '../ActionTypes'

const initialState = Immutable({
    books: null,
    fetching: false
})

export default (state = initialState, action: Object) => {
    switch (action.type) {
        case ActionTypes.GET_ALL_BOOKS.REQUEST:
            return {
                ...state,
                fetching: true
            }
        case ActionTypes.GET_ALL_BOOKS.SUCCESS: {
            console.log('action data', action)
            return {
                ...state,
                books: action.data,
                fetching: false
            }
        }
        case ActionTypes.LOADING_STATE:
            return { ...state, fetching: false };
        default:
            return state;
    }
}