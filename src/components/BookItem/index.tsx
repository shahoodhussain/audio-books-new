import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
// import RNPhotoManipulator from 'react-native-photo-manipulator';
import { Fonts, Theme } from '../../assets/theme';
import { APP_ROUTES } from '../../router/RoutesConfig';
import { navigate } from '../../services/NavigationService';

export class BookItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageData: null
        }
    }

    render() {
        const { title, image, author, name, thumbnail = '' } = this.props.item;
        const routeName = name ? APP_ROUTES.BOOKS.routeName : APP_ROUTES.BOOK_DETAILS.routeName
        return (
            <TouchableOpacity style={{marginRight: 15}} activeOpacity={1}
            onPress={()=> navigate(routeName, { item: this.props.item, name, shouldUpdate: this.props.shouldUpdate })}>
                <View style={{borderRadius: 15, overflow: 'hidden', width: 130, height: 180}}>
                    <Image 
                        source={{uri: image ? image : thumbnail}}//imageLinks?.smallThumbnail.replace('http', 'https')}}
                        style={{flex: 1, resizeMode: 'cover'}}
                        // style={{width: 230, height: 220, top: -20, left: -45, resizeMode: 'cover'}}
                    />
                </View>
                <View style={{width: 130, marginTop: 5}}>
                    <Text numberOfLines={2} style={{color: Theme.white, fontFamily: Fonts.type.Regular, fontSize: Fonts.size.small}}>{title ? title : name}</Text>
                    <Text numberOfLines={2} style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>{author?.name}</Text>
                    {/* <Text numberOfLines={2} style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>{authors.length > 0 && authors[0]}</Text> */}
                </View>
            </TouchableOpacity>
        )
    }
}

export default BookItem
