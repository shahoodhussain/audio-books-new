import React from 'react'
import { Image, StyleSheet } from 'react-native'
import { Images } from '../../assets/theme'

const AppLogo = () => {
    return (
        <Image source={Images.AppLogo} style={styles.image}/>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: 200,
        height: 180,
        resizeMode: 'contain'
    }
})
export default AppLogo
