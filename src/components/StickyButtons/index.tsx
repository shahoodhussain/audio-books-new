import React, { useState } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { Fonts, Images, Theme } from '../../assets/theme'

const StickyButtons = () => {
    const [selectedTab, setselectedTab] = useState(1);

    return (
        <View style={styles.container}>
            <TouchableOpacity
            activeOpacity={0.7}
            onPress={()=> setselectedTab(1)}
            style={[styles.button, {backgroundColor: selectedTab === 1 ? '#a1001b' : Theme.red}]}>
                <Image 
                source={Images.Pin}
                style={styles.icon}/>
                <Text style={styles.text}>Search By Delivery Address</Text>
            </TouchableOpacity>
            <TouchableOpacity
            activeOpacity={0.7}
            onPress={()=> setselectedTab(2)}
            style={[styles.button, {backgroundColor: selectedTab === 2 ? '#a1001b' : Theme.red}]}>
                <Image 
                source={Images.Restaurant}
                style={styles.icon}/>
                <Text style={styles.text}>Search By Restaurant Name</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        paddingVertical: 10,
        flexDirection: 'row',
        paddingHorizontal: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
    },
    text: {
        flex: 1,
        marginLeft: 15,
        color: Theme.white,
        fontSize: Fonts.size.xxSmall,
        fontFamily: Fonts.type.Medium
    }
})

export default StickyButtons
