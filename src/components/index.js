// @flow
import Spacer from "./Spacer";
// import TabIcon from "./TabIcon";
import SearchBar from "./SearchBar";
// import Loader from "./Loader";
// import EmptyStateText from "./EmptyStateText";
// import DrawerItem from "./DrawerItem";
import BackgroundImageWrapper from "./BackgroundImage";
import AppLogo from "./AppLogo";
import NavigationComponents from "./NavigationComponents";
import TextField from "./AnimatedTextField";
import ButtonView from "./ButtonView";
import StickyButtons from "./StickyButtons";
import BannerCarousel from "./ImageSlider";
// import ItemList from "./ItemsList";
// import RestaurantItem from "./RestaurantItem";
import RestaurantServices from "./RestaurantInfo/services";
import RestaurantProduct from "./RestaurantProduct";
import ProductDetails from "./ProductDetailsModal";
import BookItem from "./BookItem";
import AudioPlayer from "./TrackPlayer";
import DemoPlayer from "./DemoModal";

export {
  Spacer,
  // TabIcon,
  // SearchBar,
  // Loader,
  // EmptyStateText,
  // DrawerItem,
  BackgroundImageWrapper,
  AppLogo,
  NavigationComponents,
  TextField,
  ButtonView,
  StickyButtons,
  SearchBar,
  BannerCarousel,
  // ItemList,
  // RestaurantItem,
  RestaurantServices,
  RestaurantProduct,
  ProductDetails,
  BookItem,
  AudioPlayer,
  DemoPlayer
};
