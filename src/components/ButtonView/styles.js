// @flow
import { StyleSheet } from "react-native";
import { Metrics, Colors, Theme } from "../../assets/theme";

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    padding: Metrics.baseMargin,
    backgroundColor: Theme.transparent
  },
  appButton: {
    flex: 1,
    backgroundColor: Theme.white,
    justifyContent: 'center',
    alignItems: 'center',
    // borderRadius: 20,
  },
});
