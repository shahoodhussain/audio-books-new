import React, { useState, useEffect } from 'react'
import { View, Text, Image,TouchableOpacity } from 'react-native'
import Modal from "react-native-modal";
import { Fonts, Images, Theme } from '../../assets/theme';
import TrackPlayer, { State, useProgress, Capability } from 'react-native-track-player';
import util from '../../util';
import Slider from '@react-native-community/slider';

const DemoPlayer = ({trackDetails, onClose}) => {
    const [isPlaying, setPlaying] = useState(true);
    const progress = useProgress();

    useEffect(() => {
        setPlaying(true);
        async function setupPlayer() {
            await TrackPlayer.destroy();
            try {
                TrackPlayer.updateOptions({
                    stopWithApp: true,
                    capabilities: [
                        Capability.Play,
                        Capability.Pause,
                        Capability.Stop,
                    ],
                    compactCapabilities: [
                        Capability.Play,
                        Capability.Pause,
                        Capability.Stop,
                    ],
                });
                await TrackPlayer.setupPlayer();
            } catch (e) {
                console.log('mmmmmmmm',e);
                util.DialogAlert('Unable to play book');
            }
            if(trackDetails?.demo_file) {
                let demo = trackDetails?.demo_file;
                demo.artist = '';
                demo.id = trackDetails?.demo_file.id.toString()
                TrackPlayer.add(demo);
                TrackPlayer.play();
            }
        }
        setupPlayer();
        return ()=> TrackPlayer.destroy();
    }, []);

    const onPlayPausePress = async () => {
        const state = await TrackPlayer.getState();
        if (state === State.Playing) {
            TrackPlayer.pause();
            setPlaying(false);
        }
        if (state === State.Paused) {
            TrackPlayer.play();
            setPlaying(true);
        }
    };

    return (
        <Modal isVisible={true}
        onDismiss={()=> onClose()}>
            <View style={{ backgroundColor: Theme.primaryBackground, borderRadius: 20 }}>
                <TouchableOpacity style={{
                    alignItems: 'flex-end',
                    paddingVertical: 5,
                    marginVertical: 10,
                    marginHorizontal: 20
                }}
                onPress={()=> onClose()}>
                    <Image 
                        source={Images.Close}
                        style={{width: 20, height: 20, resizeMode: 'contain', tintColor: Theme.themeFontColor}}
                    />
                </TouchableOpacity>
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginVertical: 10,
                }}>
                    <Image 
                        source={{uri: trackDetails?.image}}
                        style={{
                            width: 220,
                            height: 220,
                            resizeMode: 'contain',
                        }}
                    />
                </View>
                <View style={{
                    alignItems: 'center'
                }}>
                    <Slider
                        style={{width: '90%', height: 20}}
                        value={(progress.position)}
                        minimumValue={0}
                        maximumValue={trackDetails?.demo_file?.duration}
                        thumbTintColor={Theme.themeFontColor}
                        minimumTrackTintColor={Theme.themeFontColor}
                        maximumTrackTintColor={Theme.white}
                        onSlidingComplete={async(value)=> {
                            await TrackPlayer.seekTo(value);
                        }}
                    />
                </View>
                <View style={{
                    marginTop: 10,
                    marginHorizontal: 30,
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>
                    <Text style={{color: Theme.white, fontSize: Fonts.size.small, fontFamily: Fonts.type.Medium}}>
                        {util.secondsToHHMMSS(Math.floor(progress.position || 0))}
                    </Text>
                    <Text style={{color: Theme.white, fontSize: Fonts.size.small, fontFamily: Fonts.type.Medium}}>
                        {util.secondsToHHMMSS(Math.floor(progress.duration - progress.position || 0))}
                    </Text>
                </View>
                <View style={{
                    alignItems: 'center',
                    marginVertical: 20
                }}>
                    <TouchableOpacity style={{width: 50, height: 50, backgroundColor: Theme.themeFontColor, borderRadius: 30, justifyContent: 'center', alignItems: 'center'}}
                    activeOpacity={1}
                    onPress={()=> onPlayPausePress()}>
                        <Image 
                            source={!isPlaying ? Images.Play : Images.Pause}
                            style={{width: 20, height: 20, resizeMode: 'contain'}}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

export default DemoPlayer
