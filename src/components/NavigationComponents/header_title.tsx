import React from 'react'
import { StyleSheet, Text, TextStyle } from 'react-native'
import { Fonts, Theme } from '../../assets/theme'

const HeaderTitle = (props: { title: string, style: TextStyle }) => {
    return (
        <Text style={[styles.text, {...props.style}]}>{props.title}</Text>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: Fonts.size.xLarge,
        fontFamily: Fonts.type.CiceroMedium,
        color: Theme.themeFontColor
    }
})

export default HeaderTitle