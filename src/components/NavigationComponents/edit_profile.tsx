import React from 'react'
import { Image, TouchableOpacity } from 'react-native'
import { Images } from '../../assets/theme'
import { APP_ROUTES } from '../../router/RoutesConfig'
import { navigate } from '../../services/NavigationService'

const EditProfile = () => {
    return (
        <TouchableOpacity
        onPress={()=> navigate(APP_ROUTES.COMPLETE_PROFILE.routeName)}>
            <Image 
                source={Images.Edit}
                style={{width: 30, height: 30, resizeMode: 'contain'}}
            />
        </TouchableOpacity>
    )
}

export default EditProfile
