import React from 'react'
import { TouchableOpacity, Image, StyleSheet, Text, View } from 'react-native'
import { Fonts, Images, Theme } from '../../assets/theme'
import { APP_ROUTES } from '../../router/RoutesConfig'
import { navigate } from '../../services/NavigationService'
import { useSelector } from "react-redux";

const CartIcon = () => {
    const { cartItems = [] } = useSelector(state => state.cart)
    return (
        <TouchableOpacity onPress={()=>navigate(APP_ROUTES.CART.routeName)}
        hitSlop={styles.hitSlop}
        style={styles.button}>
            {
                cartItems.length > 0 && (
                    <View style={styles.totalView}>
                        <Text style={styles.textLength}>
                            {cartItems.length}
                        </Text>
                    </View>
                )
            }
            <Image source={Images.Cart} style={styles.image}/>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        margin: 5,
        height: 40,
        width: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    hitSlop: {
        left: 20,
        top: 10,
        bottom: 10,
        right: 20,
    },
    image: {
        top: 3,
        width: 20,
        height: 20,
        resizeMode: 'contain',
        tintColor: Theme.themeFontColor
    },
    totalView: {
        position: 'absolute',
        top: 0,
        right: 2,
        height: 15,
        width: 15,
        borderRadius: 10,
        backgroundColor: Theme.themeFontColor,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 10,
    },
    textLength: {
        color: Theme.white,
        fontSize: Fonts.size.xxxSmall-1,
        fontFamily: Fonts.type.Light
    }
})

export default CartIcon
