import React from 'react'
import { View, Text, Image } from 'react-native'
import { Images } from '../../assets/theme'

const HeaderSearchIcon = () => {
    return (
        <View style={{backgroundColor: 'rgb(235,113,46)', borderRadius: 20, height: 33, width: 33, justifyContent: 'center', alignItems: 'center'}}>
            <Image 
                source={Images.Search}
                style={{width: 17, height: 17, resizeMode: 'contain'}}
            />
        </View>
    )
}

export default HeaderSearchIcon
