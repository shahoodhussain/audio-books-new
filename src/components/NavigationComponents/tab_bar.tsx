import React from "react";
import { Text, TouchableOpacity, SafeAreaView, Image, Platform } from 'react-native';
import { Fonts, Images, Theme } from "../../assets/theme";
import { APP_ROUTES } from "../../router/RoutesConfig";

export default function CustomTabBar({ state, descriptors, navigation }) {
    return (
        <SafeAreaView style={{ flexDirection: 'row', backgroundColor: Theme.themeFontColor, alignItems: 'center'}}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                let iconSource = null;
                switch (route.name) {
                    case APP_ROUTES.TABBOOKS.routeName:
                        iconSource = Images.Books
                        break;
                    case APP_ROUTES.TABCATEGORY.routeName:
                        iconSource = Images.Categories
                        break;
                    case APP_ROUTES.TABHOME.routeName:
                        iconSource = Images.Home
                        break;
                    case APP_ROUTES.TABLIBRARY.routeName:
                        iconSource = Images.Library
                        break;
                    case APP_ROUTES.TABPROFILE.routeName:
                        iconSource = Images.User
                        break;
                    default:
                        break;
                }
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        // The `merge: true` option makes sure that the params inside the tab screen are preserved
                        navigation.navigate({ name: route.name, merge: true });
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                return (
                    <TouchableOpacity
                        key={route.key}
                        // accessibilityRole="button"
                        // accessibilityState={isFocused ? { selected: true } : {}}
                        // accessibilityLabel={options.tabBarAccessibilityLabel}
                        // testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginVertical: Platform.OS === 'ios' ? 5 : 0 }}
                    >
                        <Image 
                            source={iconSource}
                            style={{width: 20, height: 20, resizeMode: 'contain', tintColor: isFocused ? Theme.white : Theme.white, marginVertical: 5, opacity: isFocused ? 1 : 0.7}}
                        />
                        {/* <Text style={{ color: isFocused ? '#673ab7' : '#222' }}> */}
                        <Text style={{ color: isFocused ? Theme.white : Theme.white, fontSize: isFocused ? Fonts.size.normal : Fonts.size.xxSmall, opacity: isFocused ? 1 : 0.7 }}>
                            {label}
                        </Text>
                    </TouchableOpacity>
                );
            })}
        </SafeAreaView>
    );
}
