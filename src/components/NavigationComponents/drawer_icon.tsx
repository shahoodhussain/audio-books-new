import React from 'react'
import { StyleSheet, TouchableOpacity, Image, ColorValue } from 'react-native'
import { Images, Theme } from '../../assets/theme'

export const DrawerIcon = (
        { toggleDrawer, tintColor = Theme.white } : { toggleDrawer: Function, tintColor: ColorValue }
    ) => (
    <TouchableOpacity onPress={()=>toggleDrawer()} hitSlop={styles.hitSlop}>
        <Image source={Images.drawerIcon} style={[styles.image, {tintColor}]}/>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    image: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        tintColor: Theme.white
    },
    hitSlop: {
        left: 20,
        right: 20,
        bottom: 10,
        top: 5
    }
})

export default DrawerIcon
