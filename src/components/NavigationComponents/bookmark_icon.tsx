import React from 'react'
import { TouchableOpacity, Image, StyleSheet, ActivityIndicator } from 'react-native'
import { Images, Theme } from '../../assets/theme'
import { useDispatch } from "react-redux";
import { bookmarkAction, fetchHomeBooks } from '../../redux/actions/books.actions';

const BookMarkIcon = ({route}) => {
    const [isLoading, setisLoading] = React.useState(false)
    const [isBookMark, setBookMark] = React.useState(false)
    const dispatch = useDispatch();
    return (
        <TouchableOpacity onPress={()=>{
            setisLoading(!isLoading)
            dispatch(bookmarkAction(
                route.item?.id,
                (response) => {
                    setisLoading(false)
                    dispatch(fetchHomeBooks())
                    if(response === 'success') setBookMark(!isBookMark)
                }
            ));
        }}
        hitSlop={styles.hitSlop}
        disabled={isLoading}
        style={styles.button}>
            {
                isLoading ? <ActivityIndicator color={Theme.white} size={'small'}/> : <Image source={Images.BookMark} 
                style={[styles.image, (isBookMark || route.item?.is_saved) && styles.favImage]}/>
            }
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    hitSlop: {
        left: 20,
        top: 10,
        bottom: 10,
        right: 20,
    },
    image: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
    },
    favImage: {
        tintColor: Theme.primaryBackground
    }
})

export default BookMarkIcon
