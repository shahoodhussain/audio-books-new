import HeaderBack from "./header_back";
import HeaderTitle from "./header_title";
import NavHeader from './nav_header';
import DrawerItem from './drawer_icon';
import CartItem from './cart_icon';
import Drawer from './drawer';
import HeaderSearchIcon from './search_icon';
import TabBar from './tab_bar';
import Options from './options_icon';
import EditIcon from './edit_profile';
import BookMarkIcon from './bookmark_icon';

export default {
    HeaderBack,
    HeaderTitle,
    NavHeader,
    DrawerItem,
    CartItem,
    Drawer,
    HeaderSearchIcon,
    TabBar,
    Options,
    EditIcon,
    BookMarkIcon
}