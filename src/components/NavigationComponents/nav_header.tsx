import React from 'react'
import { View, Text } from 'react-native'
// import { useHeaderHeight,HeaderHeightContext } from '@react-navigation/elements';

const NavHeader = () => {
    // const headerHeight = useHeaderHeight();
    return (
        <HeaderHeightContext.Consumer>
            {
                headerHeight => {
                    console.log(headerHeight);
                    return(
                        <View style={{paddingTop: headerHeight, backgroundColor: 'blue'}}>
                            <Text style={{color: 'red'}}>hello</Text>
                        </View>
                    )
                }
            }
        </HeaderHeightContext.Consumer>
    )
}

export default NavHeader
