import React from 'react'
import { View, Text, Image } from 'react-native'
import { Images, Theme } from '../../assets/theme'

const Options = () => {
    return (
        <Image 
            source={Images.Options}
            style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: Theme.themeFontColor}}
        />
    )
}

export default Options
