import React, { Component } from 'react'
import { Image, ImageBackground, Text, TouchableOpacity, View } from 'react-native'
import { BackgroundImageWrapper, DrawerItem } from '..';
import { Fonts, Images, Theme } from '../../assets/theme';
// import {
//     DrawerContentScrollView,
// } from '@react-navigation/drawer';
import Animated from "react-native-reanimated";

export class Drawer extends Component {
    render() {
        return (
            <View style={{flex: 1, borderTopRightRadius: 40, borderBottomRightRadius: 40, overflow: 'hidden'}}>
                <BackgroundImageWrapper isNavigation={false} transparentBackground={true}>
                    <>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: Theme.white}}>
                            <TouchableOpacity
                            style={{position: 'absolute', right: 20, top: 20}}>
                                <Image
                                    source={Images.Close}
                                    style={{width: 15, height: 15, resizeMode: 'contain',}}
                                />
                            </TouchableOpacity>
                            <View style={{borderWidth: 5, borderColor: Theme.lightText, borderRadius: 80,}}>
                                <ImageBackground 
                                    source={Images.avatar}
                                    style={{width: 120, height: 120, borderRadius: 60}}
                                >
                                    <Image source={Images.EditPencil} style={{width: 30, height: 30, resizeMode: 'contain', position: 'absolute', right: 5, bottom: 5}}/>
                                </ImageBackground>
                            </View>
                            <Text style={{color: Theme.red, fontSize: Fonts.size.normal, fontFamily: Fonts.type.Medium}}>John Smith</Text>
                        </View>
                        <Animated.View style={[{flex: 2}]}>
                            {/* <DrawerContentScrollView {...this.props}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{}}>
                                {
                                    this.props.drawerItems.map((drawerItem, index) => <DrawerItem key={drawerItem.id} {...drawerItem} /> )
                                }
                                <DrawerItem id="logout" key='logout' label="Logout" routeName="logout" icon={Images.Logout} />
                            </DrawerContentScrollView> */}
                        </Animated.View>
                    </>
                </BackgroundImageWrapper>
            </View>
        )
    }
}

export default Drawer
