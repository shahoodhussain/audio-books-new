import React from 'react'
import { TouchableOpacity, Image, StyleSheet, Text, TextStyle, ImageProps } from 'react-native'
import { Images, Fonts, Theme } from '../../assets/theme';
import { goBack } from '../../services/NavigationService';

const HeaderBack = (props: { onPress: () => void, title: string, textStyle: TextStyle, imageStyle: ImageProps} ) => {
    let backImage = Images.HeaderBack;
    return(
        <TouchableOpacity onPress={()=> goBack()} style={styles.button}>
            <Image source={backImage} style={[styles.image, {...props.imageStyle}]}/>
            <Text style={[styles.text, {...props.textStyle}]}>{props.title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        paddingRight: 7,
        paddingVertical: 7,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        marginRight: 3
    },
    text: {
        fontSize: Fonts.size.large,
        fontFamily: Fonts.type.CiceroMedium,
        color: Theme.white
    }
})
export default HeaderBack
