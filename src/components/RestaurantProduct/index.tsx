import React, { Component } from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Fonts, Theme } from '../../assets/theme';

export class Item extends Component {
    render() {
        const { id, title, image, description, price } = this.props.item;
        return (
            <TouchableOpacity style={styles.container} activeOpacity={0.7}
            onPress={this.props.onItemSelect}>
                <View style={styles.imageContainer}>
                    <Image 
                    source={image}
                    style={styles.image}/>

                </View>
                <View style={styles.contentContainer}>
                    <View style={styles.titleAndPrice}>
                        <Text style={styles.title}>{title}</Text>
                        <Text style={styles.price}>${price}</Text>
                    </View>
                    <Text style={styles.description}>{description}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: Theme.lightText,
        padding: 10,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: Theme.lightText
    },
    imageContainer: {
        borderRadius: 15,
        backgroundColor: Theme.white + '40',
        borderWidth: 2,
        borderColor: Theme.white,
        overflow: 'hidden'
    },
    image: {
        height: 100, width: 100,
        resizeMode: 'cover',
    },
    contentContainer: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    titleAndPrice: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'flex-start'
    },
    title: {
        color: Theme.white, fontFamily: Fonts.type.Medium, fontSize: Fonts.size.small
    },
    price: {
        color: Theme.primaryBackground,
        fontFamily: Fonts.type.SemiBold,
        fontSize: Fonts.size.medium
    },
    description: {
        color: Theme.white,
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.xSmall
    }
})

export default Item
