import React from 'react'
import { View, Text, StyleSheet, ImageBackground, Platform, Image, TouchableOpacity } from 'react-native'
import { AppStyles, Fonts, Images, Theme } from '../../assets/theme'
import StarRating from 'react-native-star-rating';
import { navigate } from '../../services/NavigationService';
import { APP_ROUTES } from '../../routes/routeNames';

const RestaurantItem = ({ item }) => {
    return (
        <TouchableOpacity
        activeOpacity={0.8}
        onPress={()=> navigate(APP_ROUTES.RESTAURANTS_DETAILS.routeName, {...item})}
        style={styles.container}>
            <View style={styles.backgroundContainer}>
                <ImageBackground
                    source={item.imageUri}
                    style={styles.backgroundImage}
                    resizeMode='cover'
                >
                    <View style={styles.statusContainer}>
                        <Image 
                            style={styles.onlineImage}
                            source={Images.Online}
                        />
                        <Text style={styles.statusText}>Online</Text>
                    </View>
                </ImageBackground>
                <View style={styles.contentContainer}>
                    <Text style={styles.title}>{item.title}</Text>
                    <View style={styles.ratingsContainer}>
                        <StarRating
                            disabled={false}
                            maxStars={5}
                            rating={item.ratings}
                            emptyStar={Images.StartDisable}
                            fullStar={Images.StarSelected}
                            starSize={15}
                            containerStyle={{
                                width: 100,
                                marginRight: 10,
                            }}
                            // selectedStar={(rating) => this.onStarRatingPress(rating)}
                        />
                        <Text style={styles.ratingsText}>{item.ratings} <Text>({item.totalReviews})</Text></Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 15,
        marginVertical: 10,
        borderRadius: 10,
        // overflow: 'hidden',
        borderWidth: 5,
        borderColor: Theme.white,
        ...AppStyles.shadow7,
        shadowOffset: {
            height: 4.5,
            width: 0,
        },
        shadowOpacity: 0.69,
        shadowRadius: 3.8,
    },
    backgroundContainer: {
        width: '100%',
        height: 200,
        backgroundColor: Theme.white
    },
    backgroundImage: {
        height: 140,
        alignItems: 'flex-end',
        borderTopStartRadius: 7,
        borderTopEndRadius: 7,
        overflow: 'hidden'
    },
    statusContainer: {
        margin: 7,
        flexDirection: 'row',
        backgroundColor: Theme.white,
        paddingHorizontal: 8,
        paddingVertical: 5,
        borderRadius: 7,
        alignItems: 'center',
        ...AppStyles.shadow7
    },
    onlineImage: {
        width: 14,
        height: 14,
        resizeMode: 'contain',
        marginRight: 5
    },
    statusText: {
        fontSize: Fonts.size.xxxSmall,
        fontFamily: Fonts.type.Regular
    },
    contentContainer: {
        flex: 1,
        marginHorizontal: 20,
        justifyContent: 'center'
    },
    title: {
        fontSize: Fonts.size.small,
        fontFamily: Fonts.type.Regular
    },
    ratingsContainer: {
        marginTop: Platform.select({ ios: 5, android: 0 }),
        // alignItems: 'baseline',
        flexDirection: 'row'
    },
    ratingsText: {
        color: Theme.lightText,
        fontSize: Fonts.size.xxSmall,
        fontFamily: Fonts.type.Regular
    }
})

export default RestaurantItem