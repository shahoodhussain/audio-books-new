import React, { Component } from 'react'
import { View, StyleSheet, FlatList, Image, NativeScrollEvent, NativeSyntheticEvent } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Metrics, Theme } from '../../assets/theme';

const carouselItems = [
    {
        title:"Item 1",
        text: "Text 1",
        illustration: 'https://i.imgur.com/2nCt3Sbl.jpg',
        image: require('../../assets/images/home/offer.png')
    },
    {
        title:"Item 2",
        text: "Text 2",
        illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
        image: require('../../assets/images/home/offer.png')

    },
    {
        title:"Item 3",
        text: "Text 3",
        illustration: 'https://i.imgur.com/MABUbpDl.jpg',
        image: require('../../assets/images/home/offer.png')

    },
    {
        title:"Item 4",
        text: "Text 4",
        illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
        image: require('../../assets/images/home/offer.png')

    },
    {
        title:"Item 5",
        text: "Text 5",
        illustration: 'https://i.imgur.com/UYiroysl.jpg',
        image: require('../../assets/images/home/offer.png')

    },
]

export class BannerCarousel extends Component {

    state = {
        activeIndex: 0,
    }
    flatListRef: FlatList<{ title: string; text: string; illustration: string; }> | null = null;

    _renderItem({item,index}: any) {
        return (
            <View style={styles.itemContainer}>
                <Image 
                    // source={{uri: item.illustration}}
                    source={item.image}
                    style={styles.parallaxContainer}
                />
            </View>
        )
    }

    handleScroll(event: NativeSyntheticEvent<NativeScrollEvent>) {
        let index = Math.round(event.nativeEvent.contentOffset.x / Metrics.screenWidth);
        this.setState({ activeIndex: index })
    }

    render() {
        return (
            <View>
                <FlatList
                    data={carouselItems}
                    horizontal={true}
                    pagingEnabled
                    ref={ref=> this.flatListRef = ref}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={({title})=> title.toString()}
                    renderItem={(item)=> this._renderItem(item)}
                    onScroll={(e)=> this.handleScroll(e)}
                />
                <View style={styles.dotsContainer}>
                    {
                        carouselItems.map((a,i)=>{
                            return(
                                <TouchableOpacity
                                    onPress={()=> this.flatListRef?.scrollToIndex({animated: true, index: i})} 
                                    style={{...styles.dot, backgroundColor: this.state.activeIndex === i ? Theme.red : Theme.white }}
                                />
                            )
                        })
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        width: Metrics.screenWidth,
        height: 220,
    },
    parallaxContainer: {
        flex: 1,
        width: Metrics.screenWidth,
        resizeMode: 'cover',
    },
    dotsContainer: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 10,
        alignSelf: 'center'
    },
    dot: {
        width: 10,
        marginHorizontal: 3,
        borderRadius: 5,
        height: 10,
        borderColor: Theme.white,
        borderWidth: 2, 
    }
})

export default BannerCarousel
