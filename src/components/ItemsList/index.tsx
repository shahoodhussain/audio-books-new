import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, ImageSourcePropType } from 'react-native'
import { Fonts, Theme } from '../../assets/theme';
import RestaurantView from "./restaurant_item";
import MenuItem from "./menu_item";

interface itemObject {
    id: number,
    title: string,
    ratings: number,
    imageUri: ImageSourcePropType
}

interface componentProps {
    heading: string,
    showBackground: boolean,
    items: itemObject,
    isRestaurant: boolean,
}

export class ItemsList extends Component<componentProps> {

    _renderItem({ item, index } : { item: itemObject, index: number }) {
        if(this.props.isRestaurant) return <RestaurantView data={item}/>
        else return <MenuItem item={item} showBackground={this.props.showBackground}/>
    }

    render() {
        const { heading = '' } = this.props;
        return (
            <View>
                <Text style={styles.heading}>{heading}</Text>
                <FlatList
                    data={this.props.items}
                    horizontal={true}
                    // ref={ref=> this.flatListRef = ref}
                    showsHorizontalScrollIndicator={false}
                    // keyExtractor={({title})=> title.toString()}
                    renderItem={(item)=> this._renderItem(item)}
                    style={{marginTop: 10, paddingHorizontal: 10}}
                    ListFooterComponent={()=> <View style={{width: 20}}/>}
                    // onScroll={(e)=> this.handleScroll(e)}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    heading: {
        marginHorizontal: 15,
        marginVertical: 5,
        fontFamily: Fonts.type.Bold,
        fontSize: Fonts.size.small,
        color: Theme.white
    },
})

export default ItemsList
