import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ImageSourcePropType } from 'react-native'
import { AppStyles, Fonts, Theme } from '../../assets/theme';
import { APP_ROUTES } from '../../routes/routeNames';
import { navigate } from '../../services/NavigationService';

interface itemObject {
    id: number,
    title: string,
    ratings: number,
    imageUri: ImageSourcePropType
}

const MenuItem = (
    { item, showBackground } : { item: itemObject, showBackground: boolean }
) => {
    return (
        <TouchableOpacity style={styles.shadow7} activeOpacity={0.7}
        onPress={()=> navigate(APP_ROUTES.RESTAURANTS.routeName)}>
            <View style={styles.itemContainer}>
                {showBackground && <View style={styles.background}/>}
                <Image 
                    source={item.imageUri}
                    style={styles.image}
                />
                <Text style={styles.titleText}>{item.title}</Text>
            </View>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    shadow7: {
        ...AppStyles.shadow7,
        shadowOffset: {
            height: 4.5,
            width: 0,
        },
        shadowOpacity: 0.69,
        shadowRadius: 3.8,
        marginBottom: 10,
        backgroundColor: Theme.white,
        marginHorizontal: 5,
        borderWidth: 2,
        borderColor: Theme.white,
        borderRadius: 13,
    },
    itemContainer: {
        borderRadius: 13,
        width: 100,
        overflow: 'hidden',
        height: 120,
        // backgroundColor: Theme.white,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 70,
        height: 70,
        resizeMode: 'contain',
        marginBottom: 5
    },
    background: {
        position: 'absolute',
        top: -115,
        backgroundColor: Theme.yellowBackground,
        height: 180,
        width: 180,
        borderRadius: 90
    },
    titleText: {
        fontFamily: Fonts.type.Light,
        fontSize: Fonts.size.xxxSmall
    }
})

export default MenuItem
