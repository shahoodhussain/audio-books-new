import React from 'react'
import { Text, View, StyleSheet, Image, ImageSourcePropType, Platform, TouchableOpacity } from 'react-native'
import { AppStyles, Fonts, Images, Metrics, Theme } from '../../assets/theme';
import StarRating from 'react-native-star-rating';
import { APP_ROUTES } from '../../routes/routeNames';
import { navigate } from '../../services/NavigationService';


interface itemObject {
    id: number,
    title: string,
    ratings: number,
    imageUri: ImageSourcePropType
}

const RestuartantItem = (
    { data } : { data: itemObject }
) => {
    return (
        <TouchableOpacity style={styles.shadow7}
        activeOpacity={0.8}
        onPress={()=> navigate(APP_ROUTES.RESTAURANTS.routeName)}>
            <View style={styles.itemContainer}>
                <Image 
                    source={data.imageUri}
                    style={styles.image}
                />
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={data.ratings}
                        emptyStar={Images.StartDisable}
                        fullStar={Images.StarSelected}
                        starSize={15}
                        // selectedStar={(rating) => this.onStarRatingPress(rating)}
                    />
                    <Text style={styles.titleText}>{data.title}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    shadow7: {
        ...AppStyles.shadow7,
        shadowOffset: {
            height: 4.5,
            width: 0,
        },
        shadowOpacity: 0.69,
        shadowRadius: 3.8,
        marginBottom: 10,
        backgroundColor: Theme.white,
        marginHorizontal: 5,
        borderWidth: 5,
        borderColor: Theme.white,
        borderRadius: 13,
    },
    itemContainer: {
        borderRadius: 13,
        width: 140,
        overflow: 'hidden',
        height: Platform.select({ ios: 140, android: 150 }),
        // backgroundColor: Theme.white,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 150,
        height: 85,
        resizeMode: 'cover',
        marginBottom: 5
    },
    titleText: {
        marginTop: 5,
        fontFamily: Fonts.type.Light,
        fontSize: Fonts.size.xxxSmall
    }
})

export default RestuartantItem
