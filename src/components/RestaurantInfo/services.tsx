import React, { Component } from 'react'
import { FlatList, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Fonts, Theme } from '../../assets/theme'

const services = [
    'Fast Foods',
    'Sea Foods',
    'Mexican Foods',
    'Desearts',
]

export class RestaurantServices extends Component {

    state = {
        selectedType: services[0]
    }

    _renderItem(item) {
        const isSelected = item === this.state.selectedType;
        return(
            <TouchableOpacity style={[ styles.itemContainer, { backgroundColor: isSelected ? Theme.white : Theme.transparent } ]}
            onPress={()=> this.setState({ selectedType: item })}>
                <Text style={[styles.text, { color: isSelected ? Theme.black : Theme.white }]}>{item}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <FlatList 
                data={services}
                renderItem={({item})=> this._renderItem(item)}
                horizontal={true}
                contentContainerStyle={{ justifyContent: 'center', alignItems: 'center'}}
                style={{backgroundColor: '#a1001b', paddingHorizontal: 10 }}
                ListFooterComponent={()=> <View style={{width: 10}}/>}
                showsHorizontalScrollIndicator={false}
            />
        )
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        justifyContent: 'center',
        marginHorizontal: 5,
        paddingHorizontal: 15,
        borderRadius: 8,
        height: 45,
    },
    text: {
        color: Theme.white,
        fontFamily: Fonts.type.Medium,
        fontSize: Fonts.size.xxSmall
    }
})

export default RestaurantServices
