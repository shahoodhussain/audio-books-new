import React, { useState, useEffect } from 'react'
import { Text, View, Image, Platform, TouchableOpacity, LayoutAnimation } from 'react-native'
import { Fonts, Images, Theme } from '../../assets/theme';
import Slider from '@react-native-community/slider';
import TrackPlayer, { State, useProgress, Event } from 'react-native-track-player';
import util from '../../util';
import { saveLastState } from "../../redux/actions/track.actions";
import { useDispatch } from "react-redux";
import { ActivityIndicator } from 'react-native';

let lastPosition = 5;

const AudioPlayer = ({ bookId, allTracks, trackState = [], chapterPlay = null }) => {
    const progress = useProgress();
    const [isPlaying, setPlaying] = useState(true);
    const [loading, setLoading] = useState(false);
    const [duration, setDuration] = useState(0);

    const dispatch = useDispatch();

    const getTrackDuration = async () => {
        setDuration(await TrackPlayer.getDuration())
    }

    useEffect(() => {
        async function saveState() {
            if(progress.position > lastPosition) {
                lastPosition = lastPosition + 5;
                const data = await TrackPlayer.getCurrentTrack();
                const trackObject = allTracks.find((trk, index) => index === data)
                console.log('current track object', trackObject)
                trackObject.lastProgress = progress.position;
                trackObject.bookId = bookId;
                dispatch(saveLastState(trackObject))
            }
        }
        saveState();
    }, [progress.position])

    useEffect(async () => {
        TrackPlayer.add(allTracks)
        // setLoading(true)
        if(chapterPlay) {
            console.log('chapterPlay',chapterPlay)
            const chapterProgress = trackState.find(chapter => chapter.id === chapterPlay.id);
            console.log('chapterProgress',chapterProgress)
            const queue = await TrackPlayer.getQueue()
            const skipToChapter = queue ? queue.findIndex(que => que.chapter_no === chapterPlay?.chapter_no) : null;
            await TrackPlayer.skip(skipToChapter)
            if(typeof chapterProgress !== 'undefined' && chapterProgress?.lastProgress) await TrackPlayer.seekTo(chapterProgress?.lastProgress)
            await TrackPlayer.play().then(()=> setPlaying(true));
        }
        getTrackDuration();
        setTimeout(() => {
            TrackPlayer.addEventListener(Event.PlaybackState, ({state})=> {
                if(state === State.Paused || state === State.Stopped) setPlaying(false)
                else if(state === State.Playing) {
                    setPlaying(true);
                    setLoading(false)
                }
                // else if(state === State.Buffering) setLoading(true)
                else setLoading(false)
            })
        }, 2000);
        TrackPlayer.addEventListener(Event.PlaybackTrackChanged, trackChanged => {
            lastPosition = 5;
        })
    }, [allTracks]);

    const onPlayPausePress = async () => {
        const state = await TrackPlayer.getState();
        LayoutAnimation.easeInEaseOut();
        if (state === State.Playing) {
            TrackPlayer.pause();
            setPlaying(false);
        }
        if (state === State.Paused) {
            TrackPlayer.play();
            setPlaying(true);
        }
    };

    const onTrackChange = async (skipType) => {
        const currentTrack = await TrackPlayer.getCurrentTrack();
        if(skipType === 'prev' && currentTrack !== 0) {
            await TrackPlayer.skipToPrevious();
            return;
        }
        if(currentTrack === allTracks.length - 1) return null;
        if(skipType === 'next') await TrackPlayer.skipToNext();
        getTrackDuration();
    }

    return (
        <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
            <Slider
                style={{width: '100%', height: 20, marginBottom: 5}}
                value={(progress.position)}
                minimumValue={0}
                maximumValue={duration}
                thumbTintColor={Theme.themeFontColor}
                minimumTrackTintColor={Theme.themeFontColor}
                maximumTrackTintColor={Theme.white}
                onSlidingComplete={async(value)=> {
                    await TrackPlayer.seekTo(value);
                }}
            />
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: Platform.OS === 'android' ? 15 : 5}}>
                <Text style={{color: Theme.white, fontSize: Fonts.size.small, fontFamily: Fonts.type.Medium}}>
                    {util.secondsToHHMMSS(Math.floor(progress.position || 0))}
                </Text>
                <Text style={{color: Theme.white, fontSize: Fonts.size.small, fontFamily: Fonts.type.Medium}}>
                    {util.secondsToHHMMSS(Math.floor(progress.duration - progress.position || 0))}
                </Text>
            </View>
            <View style={{width: '80%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: Platform.OS === 'android' ? 30 : 20}}>
                <TouchableOpacity
                onPress={() => onTrackChange('prev')}>
                    <Image 
                        source={Images.Rewind}
                        style={{width: 25, height: 25, resizeMode: 'contain'}}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                onPress={()=> TrackPlayer.seekTo(progress.position - 10)}>
                    <Image 
                        source={Images.SeekLeft}
                        style={{width: 25, height: 25, resizeMode: 'contain'}}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={{width: 50, height: 50, backgroundColor: Theme.themeFontColor, borderRadius: 30, justifyContent: 'center', alignItems: 'center'}}
                activeOpacity={1}
                onPress={()=> onPlayPausePress()}>
                    {
                        loading ? <ActivityIndicator size={'small'} color={Theme.white}/> : <Image 
                            source={!isPlaying ? Images.Play : Images.Pause}
                            style={{width: 20, height: 20, resizeMode: 'contain'}}
                        />
                    }
                </TouchableOpacity>
                <TouchableOpacity
                onPress={()=> TrackPlayer.seekTo(progress.position + 10)}>
                    <Image 
                        source={Images.SeekRight}
                        style={{width: 25, height: 25, resizeMode: 'contain'}}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                onPress={()=> onTrackChange('next')}>
                    <Image 
                        source={Images.Forward}
                        style={{width: 25, height: 25, resizeMode: 'contain'}}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default AudioPlayer
