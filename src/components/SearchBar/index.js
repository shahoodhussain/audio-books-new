import React from "react";
import { View, Image, TextInput, ActivityIndicator } from "react-native";
import { Images, Theme } from "../../assets/theme";
import styles from "./styles";

export default class SearchBar extends React.Component {
  render() {
    const { placeholder = 'Search', isSearching, onSearchText } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.searchWrapper}>
          <TextInput
            placeholder={placeholder}
            placeholderTextColor={Theme.rgbaWhite}
            style={styles.textInput}
            returnKeyType="search"
            onChangeText={text => {
              // onSearchText(text);
            }}
          />
          {isSearching ? (
            <ActivityIndicator
              size="small"
              color={Theme.white}
            />
          ) : <Image source={Images.Search} style={styles.icon} /> }
        </View>
      </View>
    );
  }
}
