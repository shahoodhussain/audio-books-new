// @flow
import { StyleSheet } from "react-native";
import { Metrics, Fonts, Theme } from "../../assets/theme";
import Util from "../../util";

export default StyleSheet.create({
  container: {
    width: Metrics.screenWidth,
    backgroundColor: '#360008'
  },
  searchWrapper: {
    alignItems: 'center',
    marginHorizontal: 25,
    flexDirection: 'row'
  },
  icon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  textInput: {
    fontSize: Fonts.size.xSmall,
    fontFamily: Fonts.type.Regular,
    flex: 1,
    color: Theme.white,
    paddingRight: 10,
    height: Util.isPlatformAndroid() ? 60 : 56,
    // paddingHorizontal: 10,
  }
});
