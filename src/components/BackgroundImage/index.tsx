import React from 'react'
import { ImageBackground } from 'react-native'
// import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { Images, Metrics } from '../../assets/theme'

const BackgroundWrapper = (
    { isNavigation = false, transparentBackground = false, children }
    :
    { isNavigation: Boolean, transparentBackground: Boolean, children: React.ReactElement }
) => {
    const image = transparentBackground ? Images.AppBackgroundTransparent : Images.AppBackground;
    const styles = isNavigation ? {
        width: Metrics.screenWidth,
        height: Metrics.screenHeight
    } : {
        flex: 1,
    };

    return (
        <ImageBackground
        style={styles}
        imageStyle={{resizeMode: 'stretch'}}
        source={image}>
                {/* <KeyboardAwareScrollView
                bounces={false}
                enableAutomaticScroll={false}
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{flexGrow: 1}}> */}
                {children}
            {/* </KeyboardAwareScrollView> */}
        </ImageBackground>
    )
}

export default BackgroundWrapper
