// @flow
import { StyleSheet } from "react-native";
import { Metrics } from "../../assets/theme";

export default StyleSheet.create({
  container: {
    height: Metrics.defaultUIHeight
  }
});
