import React, { Component } from 'react'
import { Text, View, ImageBackground, StyleSheet, Image, FlatList, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import Modal from "react-native-modal";
import { ButtonView } from '..';
import { Fonts, Images, Theme } from '../../assets/theme';

export class ProductDetails extends Component {
    state = {
        options: [
            {
                id: 1,
                title: 'Option 1',
                selected: false
            },
            {
                id: 2,
                title: 'Option 2',
                selected: false
            },
            {
                id: 3,
                title: 'Option 3',
                selected: false
            },
            {
                id: 4,
                title: 'Option 4',
                selected: false
            },
            {
                id: 5,
                title: 'Option 5',
                selected: false
            },
            {
                id: 6,
                title: 'Option 6',
                selected: false
            },
        ]
    }

    selectItem(itemId) {
        this.state.options.map(item => {
            if(item.id === itemId) {
                item.selected = !item.selected;
                this.forceUpdate();
                return;
            }
        })
    }

    _renderItem({ item }) {
        return(
            <TouchableOpacity style={styles.optionItem}
            onPress={()=> this.selectItem(item.id)}>
                <Image source={item.selected ? Images.ButtonSelected : Images.ButtonUnselected}
                style={styles.optionSelection}/>
                <Text style={styles.optionTitle}>{item.title}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        console.log('adsasd', this.props)
        const { image, price, title, description } = this.props.itemDetails
        return (
            <Modal isVisible={true}
            onBackButtonPress={this.props.onClose}
            onBackdropPress={this.props.onClose}>
                <View style={styles.containerWrapper}>
                    <ScrollView style={{ flex: 1 }}
                    nestedScrollEnabled={true}
                    showsVerticalScrollIndicator={false}>
                        <ImageBackground
                        source={image}
                        style={styles.backgroundImage}>
                            <View style={styles.infoContainer}>
                                <View style={styles.infoStack}>
                                    <Text style={styles.title}>{title}</Text>
                                    <Text style={styles.price}>${price}</Text>
                                </View>
                            </View>
                        </ImageBackground>
                        <View style={styles.contentContainer}>
                            <Text style={styles.desc}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</Text>
                            <View style={{marginTop: 20}}>
                                <Text style={styles.variant}>Variant</Text>
                                <FlatList 
                                    data={this.state.options}
                                    renderItem={(item)=> this._renderItem(item)}
                                    numColumns={2}
                                    nestedScrollEnabled={true}
                                    scrollEnabled={false}
                                />
                            </View>
                            <View style={{marginTop: 20}}>
                                <Text style={styles.variant}>Special Instruction</Text>
                                <TextInput 
                                    placeholder="Instructions"
                                    numberOfLines={4}
                                    multiline={true}
                                    style={styles.input}
                                />
                            </View>
                            <View style={styles.buttonContainer}>
                                <ButtonView
                                // shouldAnimate
                                // disabled={userData.loading}
                                style={styles.button}
                                onPress={()=> null}>
                                    <Text style={styles.loginText}>Add to Cart</Text>
                                </ButtonView>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        paddingHorizontal: 20,
        paddingTop: 10,
        backgroundColor: Theme.white
    },
    containerWrapper: {
        marginTop: 30,
        overflow: 'hidden',
        borderRadius: 20,
        flex: 1,
        borderWidth: 2,
        borderColor: Theme.white,
    },
    infoContainer: {
        flexDirection: 'row',
        backgroundColor: '#00000099',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    infoStack: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    backgroundImage: {
        width: '100%',
        height: 200,
        justifyContent: 'flex-end'
    },
    title: {
        color: Theme.white,
        fontFamily: Fonts.type.Bold,
        fontSize: Fonts.size.normal
    },
    price: {
        color: Theme.primaryBackground,
        fontFamily: Fonts.type.SemiBold,
        fontSize: Fonts.size.normal
    },
    statusContainer: {
        height: 25,
        marginBottom: 5,
        alignSelf: 'flex-end',
        flexDirection: 'row',
        backgroundColor: Theme.white,
        paddingHorizontal: 8,
        borderRadius: 7,
        alignItems: 'center',
    },
    onlineImage: {
        width: 14,
        height: 14,
        resizeMode: 'contain',
        marginRight: 5
    },
    statusText: {
        fontSize: Fonts.size.xxxSmall,
        fontFamily: Fonts.type.Regular
    },
    ratingsText: {
        marginLeft: 5,
        color: Theme.white,
        fontSize: Fonts.size.xxSmall,
        fontFamily: Fonts.type.Regular
    },
    desc: {
        color: Theme.lightTextColor,
        fontSize: Fonts.size.xSmall,
        fontFamily: Fonts.type.Regular
    },
    variant: {
        color: Theme.red,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.SemiBold
    },
    optionItem: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center'
    },
    optionSelection: {
        width: 18, height: 18,
        resizeMode: 'contain',
        marginRight: 10
    },
    optionTitle: {
        color: Theme.black,
        fontSize: Fonts.size.small,
        fontFamily: Fonts.type.Regular
    },
    input: {
        height: 80,
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    buttonContainer: {
        marginVertical: 20,
        height: 55
    },
    button: {
        flex: 1,
        backgroundColor: Theme.red,
        borderRadius: 40
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.Medium,
        fontSize: Fonts.size.normal
    }
})

export default ProductDetails
