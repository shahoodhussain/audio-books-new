import React from 'react'
import { DrawerItem } from '@react-navigation/drawer';
import { Image, Share } from 'react-native'
import CompStyles from './styles'
import { navigate } from '../../services/NavigationService';
import { APP_ROUTES } from '../../routes/routeNames';
import { Fonts, Theme } from '../../assets/theme';
import { userLogoutSuccess } from '../../redux/actions/UserActions';
import { useDispatch } from "react-redux";

const shareApp = async () => {
    try {
        const result = await Share.share({
            message: 'Hey there! This is the invitation link, use it to join my app.',
        });
        if (result.action === Share.sharedAction) {
            if (result.activityType) {
                // shared with activity type of result.activityType
            } else {
                // shared
            }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
    } catch (error) {
        // alert(error.message);
    }
};

const Item = ({id, label, icon, routeName}) => {
    const dispatch = useDispatch();

    const _dispatchDrawerAction = () => {
        switch (routeName) {
            case APP_ROUTES.INVITE_FRIENDS.routeName:
                shareApp();
                break;
            case 'logout':
                dispatch(userLogoutSuccess()) 
                break;
            default:
                navigate(routeName)
        }
    };
    return (
        <DrawerItem
            key={id}
            label={label}
            icon={()=> <Image source={icon} style={CompStyles.iconImage}/>}
            onPress={() => _dispatchDrawerAction()}
            style={{marginVertical: 2}}
            labelStyle={{color: Theme.white, fontFamily: Fonts.type.Medium, fontSize: Fonts.size.small}}
        />
    )
}

export default Item
