import { StyleSheet } from "react-native";

export default StyleSheet.create({
    iconImage: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
    }
})