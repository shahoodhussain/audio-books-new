import React, { useState, createRef } from 'react'
import { View, Text, TextInput, Animated, LayoutAnimation, Platform } from 'react-native'

interface ComponentProps {
    placeholderText: string,
    placeholderStyle: {
        fontSize: number
    },
    inputStyles: {},
    onChangeText: Function,
    value: string,
    secureTextEntry: boolean,
    keyboardType: any
}

const TextField: React.FC<ComponentProps> = ({
    placeholderText, placeholderStyle = {}, inputStyles = {}, onChangeText, value, secureTextEntry = false, keyboardType = 'default'
}) => {
    const { fontSize = 14 } = placeholderStyle;
    const [ isFocused, setFocused] = useState(false);
    const [ placeholderFS, setPlaceholderFC ] = useState(new Animated.Value(fontSize));

    const inputRef = createRef();

    const onTextPress = () => {
        LayoutAnimation.easeInEaseOut();
        setFocused(true);
        // inputRef.current.focus();
        animateFontSize(fontSize - 2);
    }

    const onInputBlur = () => {
        LayoutAnimation.easeInEaseOut();
        console.log(value);
        if(value.length === 0) {
            setFocused(false);
            animateFontSize(14);
        } 
    }

    const animateFontSize = (toValue: number) => {
        Animated.timing(placeholderFS, {
            toValue,
            duration: 330,
            useNativeDriver: false
        }).start()
    }

    return (
        <View key={placeholderText}>
            <Animated.Text 
            style={{...placeholderStyle, fontSize: placeholderFS, marginBottom: isFocused ? 2 : 0}} 
            suppressHighlighting={true}
            onPress={onTextPress}>
                {placeholderText}
            </Animated.Text>
            {isFocused && <TextInput
                // returnKeyType={}
                key={placeholderText}
                keyboardType={keyboardType}
                nativeID={Math.random().toString()}
                autoFocus={true}
                ref={inputRef}
                onChangeText={text=>onChangeText(text)}
                selectTextOnFocus={false}
                secureTextEntry={secureTextEntry}
                style={{
                    height: Platform.OS === 'android' ? 50 : 20,
                    width: '100%',
                    display: isFocused ? 'flex' : 'none',
                    ...inputStyles
                }}
                spellCheck={false}
                onBlur={onInputBlur}
            />}
        </View>
    )
}

export default TextField
