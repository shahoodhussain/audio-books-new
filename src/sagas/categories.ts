import { take, put, call, fork } from "redux-saga/effects";
import APIURL, { callRequest } from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";
import ActionTypes from "../redux/ActionTypes";

function* booksCategories() {
    while (true) {
        const { offset, responseCallBack } = yield take( ActionTypes.GET_CATEGORIES.REQUEST );
        try {
            const response = yield call( callRequest, APIURL.GET_CATEGORIES_URL, null, `offset=${offset}&limit=10`, {}, ApiSauce );
            if ( response.status === 1 ) {
                if(responseCallBack) responseCallBack(response.data, offset);
            } else {
                if(responseCallBack) responseCallBack([], offset);
            }
        } catch (error) {
            if(responseCallBack) responseCallBack([], offset);
            Util.DialogAlert(error.message)
        }
    }
}

export default function* root() {
    yield fork(booksCategories);
}