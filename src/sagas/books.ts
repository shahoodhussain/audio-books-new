import { take, put, call, fork } from "redux-saga/effects";
import APIURL, { callRequest } from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";
import ActionTypes from "../redux/ActionTypes";
import { receivedBooks, loadingState } from "../redux/actions/books.actions";

function* homeBooks() {
    while (true) {
        yield take( ActionTypes.GET_ALL_BOOKS.REQUEST );
        try {
            const response = yield call( callRequest, APIURL.GET_BOOKS_URL, null, "", {}, ApiSauce );
            if ( response.status === 1 ) {
                yield put( receivedBooks(response.data) );
            } else {
                Util.DialogAlert(response.message)
                yield put(loadingState());
            }
        } catch (error) {
            yield put(loadingState());
            Util.DialogAlert(error.message)
        }
    }
}

function* booksByCategory() {
    while (true) {
        const { responseCallBack, categoryID, offset, searchQuery } = yield take( ActionTypes.GET_CATEGORY_BOOKS.REQUEST );
        const params = `offset=${offset}&limit=${10}${categoryID?'&category='+categoryID:''}&search=${searchQuery}`;
        try {
            const response = yield call( callRequest, APIURL.GET_CATEGORIES_BOOKS_URL, null, params, {}, ApiSauce );
            if ( response.status === 1 ) {
                if( responseCallBack ) responseCallBack(response.data);
            } else {
                if( responseCallBack ) responseCallBack([]);
                Util.DialogAlert(response.message)
            }
        } catch (error) {
            Util.DialogAlert(error.message)
        }
    }
}

function* bookMark() {
    while (true) {
        const { responseCallBack, bookId } = yield take( ActionTypes.SAVE_BOOKMARK.REQUEST );
        try {
            const response = yield call( callRequest, APIURL.BOOKMARK, {id: bookId}, "", {}, ApiSauce );
            if ( response.status === 1 ) {
                if( responseCallBack ) responseCallBack(response.message);
            } else {
                if( responseCallBack ) responseCallBack(response.message);
                Util.DialogAlert(response.message)
            }
        } catch (error) {
            if( responseCallBack ) responseCallBack(false);
            Util.DialogAlert(error.message)
        }
    }
}

function* libraryBooks() {
    while (true) {
        const { responseCallBack, offset = 0 } = yield take( ActionTypes.GET_LIBRARY.REQUEST );
        try {
            const response = yield call( callRequest, APIURL.GET_CATEGORIES_BOOKS_URL, null, `offset=${offset}&limit=10&saved=1`, {}, ApiSauce );
            if ( response.status === 1 ) {
                if( responseCallBack ) responseCallBack(response.data);
            } else {
                if( responseCallBack ) responseCallBack([]);
                Util.DialogAlert(response.message)
            }
        } catch (error) {
            if( responseCallBack ) responseCallBack([]);
            Util.DialogAlert(error.message)
        }
    }
}

function* purchasedBooks() {
    while (true) {
        const { responseCallBack, offset = 0 } = yield take( ActionTypes.GET_PURCHASED_BOOKS );
        try {
            const response = yield call( callRequest, APIURL.GET_CATEGORIES_BOOKS_URL, null, `offset=${offset}&limit=10&purchased=1`, {}, ApiSauce );
            if ( response.status === 1 ) {
                if( responseCallBack ) responseCallBack(response.data);
            } else {
                if( responseCallBack ) responseCallBack([]);
                Util.DialogAlert(response.message)
            }
        } catch (error) {
            if( responseCallBack ) responseCallBack([]);
            Util.DialogAlert(error.message)
        }
    }
}

export default function* root() {
    yield fork(homeBooks);
    yield fork(booksByCategory);
    yield fork(bookMark);
    yield fork(libraryBooks);
    yield fork(purchasedBooks);
}