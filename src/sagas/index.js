import { fork } from "redux-saga/effects";
import user from "./user"; 
import appInfo from "./appInfo";
import books from "./books";
import categories from "./categories";
import card from "./card";

export default function* root() {
    yield fork(user);
    yield fork(appInfo);
    yield fork(books);
    yield fork(categories);
    yield fork(card);
}
