import { take, put, call, fork } from "redux-saga/effects";
import ActionTypes, {
  VERIFY_PASSWORD_OTP,
  PROFILE,
  EDIT_PROFILE,
} from "../redux/ActionTypes";
import {
  userLoginSuccess,
  userLogoutSuccess,
  editUserProfileSuccess,
  registerUserSuccess,
  userProfileSuccess,
  editUserProfileFailure,
  verifyUserSuccess,
  completeProfileSuccess,
  loadingState
} from "../redux/actions/UserActions";
import APIURL, {
  VERIFY_PASSWORD_OTP_URL,
  PROFILE_URL,
  EDIT_PROFILE_URL,
  callRequest
} from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";
import { goBack, navigate } from "../services/NavigationService";
import { ROUTES } from "../router/RoutesConfig";

function* login() {
  while (true) {
    const { payload } = yield take(ActionTypes.LOGIN.REQUEST);
    try {
      const response = yield call( callRequest, APIURL.LOGIN_URL, payload, "", {}, ApiSauce );
      yield put(loadingState())
      if ( response.status === 1 ) {
        yield put( userLoginSuccess(response.data, response.bearer_token) );
      } else {
        if( response && response.is_verified === false ) {
          // Util.DialogAlert(response.message, "Info", "info");
          // yield put( registerUserSuccess(response && response.user_id) );
          // NavigationService.navigate(otpRoute)
        } else if( response && response.is_profile_complete === false ) {
          // Util.DialogAlert(response.message, "Info", "info");
          // yield put( registerUserSuccess(response && response.user_id) );
          // NavigationService.navigate(completeProfileRoute)
        } else {
          Util.DialogAlert(response.message);
        }
      }
    } catch (error) {
      Util.DialogAlert(error.message)
      yield put(loadingState())
    }
  }
}

function* registerUser() {
  while (true) {
    const { payload } = yield take(ActionTypes.REGISTER_USER.REQUEST);
    try {
      const response = yield call( callRequest, APIURL.SIGNUP_URL, payload, "", {}, ApiSauce );
      yield put(loadingState())
      if (response.status === 1) {
        Util.DialogAlert(response.message, "Success", "success");
        navigate(ROUTES.ACCOUNT_VERIFICATION.routeName, { startOnMount: true, email: payload.email, otp_refence: response.data?.otp_reference });
      } else Util.DialogAlert(response.message)
    } catch (error) {
      Util.DialogAlert(error.message)
      yield put(loadingState())
    }
  }
}

function* verifyUser() {
  while (true) {
    const { payload, isResetingPassword } = yield take(ActionTypes.VERIFY_USER.REQUEST);
    try {
      const response = yield call( callRequest, APIURL.VERIFY_USER_URL, payload, "", {}, ApiSauce );
      yield put(loadingState())
      if ( response.status === 1 ) {
        yield put( verifyUserSuccess(response, isResetingPassword) );
        navigate(isResetingPassword ? ROUTES.RESET_PASSWORD.routeName : ROUTES.COMPLETE_PROFILE.routeName, { payload });
      } else Util.DialogAlert(response.message)
    } catch (error) {
      yield put(loadingState())
      Util.DialogAlert(error.message)
    }
  }
}

function* resendverificationCode() {
  while (true) {
    const { email } = yield take(ActionTypes.RESEND_VERIFICATION_CODE.REQUEST);
    try {
      const response = yield call( callRequest, APIURL.RESEND_CODE_URL, { email }, "", {}, ApiSauce );
      yield put(loadingState())
      if ( response.status === 1 ) {
        Util.DialogAlert(response.message, "Success", "success")
      } else Util.DialogAlert(response.message)
    } catch (error) {
      Util.DialogAlert(error.message)
      yield put(loadingState())
    }
  }
}

function* completeProfile() {
  while (true) {
    const { payload, authToken } = yield take(ActionTypes.COMPLETE_PROFILE.REQUEST);
    try {
      const response = yield call( callRequest, APIURL.COMPLETE_PROFILE_URL, payload, "", {Authorization: 'Bearer '+authToken}, ApiSauce );
      yield put(loadingState())
      if ( response.status === 1 ) {
        Util.DialogAlert(response.message, "Success", "success")
        yield put( completeProfileSuccess(response.data, authToken) );
      } else Util.DialogAlert(response.message)
    } catch (error) {
      yield put(loadingState())
      Util.DialogAlert(error.message)
    }
  }
}

function* changePassword() {
  while (true) {
    const { payload } = yield take(ActionTypes.CHANGE_PASSWORD.REQUEST);
    try {
      const response = yield call( callRequest, APIURL.CHANGE_PASSWORD_URL, payload, "", {}, ApiSauce );
      yield put(loadingState())
      if ( response.status === 1) {
        Util.DialogAlert(response.message, "Success", "success")
        goBack();
      } else Util.DialogAlert(response.message)
    } catch (err) {
      Util.DialogAlert(err.message)
      yield put(loadingState())
    }
  }
}

function* verifyPasswordOtp() {
  while (true) {
    const { payload, responseCallback } = yield take(VERIFY_PASSWORD_OTP.REQUEST);
    try {
      const response = yield call( callRequest, VERIFY_PASSWORD_OTP_URL, payload, "", {}, ApiSauce );
      if (response.status==1) {
        if (responseCallback) responseCallback(response.data, null);
      } else {
        Util.DialogAlert(response.message)
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      Util.DialogAlert(err.message)
    }
  }
}

function* logout() {
  while (true) {
    yield take(ActionTypes.LOGOUT.REQUEST);
    try {
      const response = yield call( callRequest, APIURL.LOGOUT_URL, null, "", {}, ApiSauce );
      yield put(userLogoutSuccess());
    } catch (err) {
      yield put(userLogoutSuccess());
    }
  }
}

function* updateUserProfile() {
  while (true) {
    const { payload, responseCallback } = yield take( ActionTypes.EDIT_PROFILE.REQUEST );
    try {
      const response = yield call( callRequest, APIURL.EDIT_PROFILE_URL, payload, "", {}, ApiSauce );
      if (response.status === 1) {
        if (responseCallback) responseCallback(response.data, null);
        yield put(editUserProfileSuccess(response.data))
      } else {
        if (responseCallback) responseCallback(null, null);
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      Util.DialogAlert(err.message)
    }
  }
}

function* forgotPassword() {
  while (true) {
    const { payload } = yield take( ActionTypes.FORGOT_PASSWORD.REQUEST );
    try {
      const response = yield call( callRequest, APIURL.FORGOT_PASSWORD_URL, payload, "", {}, ApiSauce );
      yield put(loadingState())
      if ( response.status === 1 ) {
        Util.DialogAlert(response.message, "Success", "success")
        payload.otp_refence = response.data?.otp_reference;
        navigate(ROUTES.ACCOUNT_VERIFICATION.routeName, { resetPassword: true, otp_refence: payload.otp_refence, email: payload.email })
      } else Util.DialogAlert(response.message)
    } catch (error) {
      yield put(loadingState())
      Util.DialogAlert(error.message)
    }
  }
}

function* updatePassword() {
  while (true) {
    const { payload } = yield take( ActionTypes.UPDATE_PASSWORD.REQUEST );
    try {
      const response = yield call( callRequest, APIURL.UPDATE_PASSWORD_URL, payload, "", {}, ApiSauce );
      yield put(loadingState())
      if ( response.status === 1) {
        Util.DialogAlert(response.message, "Password Updated", "success")
        navigate(ROUTES.LOGIN.routeName)
      } else Util.DialogAlert(response.message)
    } catch (error) {
      Util.DialogAlert(error.message)
      yield put(loadingState())
    }
  }
}

function* getProfileData() {
  while (true) {
    const { payload, responseCallback } = yield take( PROFILE.REQUEST );
    try {
      const response = yield call( callRequest, PROFILE_URL, payload, "", {}, ApiSauce );
      if (response.status==1) {
        if (responseCallback) responseCallback(response, null);
        yield put(userProfileSuccess(response.data));
      } else {
        Util.DialogAlert(response.message)
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      Util.DialogAlert(err.message)
    }
  }
}

export default function* root() {
  yield fork(logout);
  yield fork(login);
  yield fork(registerUser);
  yield fork(verifyUser);
  yield fork(updateUserProfile);
  yield fork(forgotPassword);
  yield fork(updatePassword);
  // yield fork(verifyPasswordOtp);
  // yield fork(getProfileData);
  yield fork(resendverificationCode);
  yield fork(completeProfile);
  yield fork(changePassword);
}
