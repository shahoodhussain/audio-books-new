import { take, put, call, fork } from "redux-saga/effects";
import { callRequest } from "../config/WebService";
import APIURL from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";
import ActionTypes from "../redux/ActionTypes";
import { clearCartData } from "../redux/actions/cart.actions";
import { navigate } from "../services/NavigationService";
import { APP_ROUTES } from "../router/RoutesConfig";


function* getAllCards() {
    while (true) {
        const { responseCallBack } = yield take(ActionTypes.GET_PAYMENT_CARD.REQUEST);
        try {
            const response = yield call( callRequest, APIURL.GET_ALL_CARDS, null, "", {}, ApiSauce );
            if ( response.status === 1 ) {
                responseCallBack(response.data)
            } else {
                Util.DialogAlert(response.message)
                responseCallBack([])
            }
        } catch (error) {
            Util.DialogAlert(error.message)
            responseCallBack([])
        }
    }
}

function* addNewCard() {
    while (true) {
        const { cardToken, responseCallBack } = yield take(ActionTypes.ADD_CARD);
        try {
            const response = yield call( callRequest, APIURL.ADD_CARD, {source_token: cardToken}, "", {}, ApiSauce );
            if ( response.status === 1 ) {
                Util.DialogAlert(response.message, 'Success', 'success')
                responseCallBack(response)
            } else {
                Util.DialogAlert(response.message)
                responseCallBack(null)
            }
        } catch (error) {
            Util.DialogAlert(error.message)
            responseCallBack(null)
        }
    }
}

function* checkout() {
    while (true) {
        const { books, card_id, responseCallBack } = yield take(ActionTypes.CHECKOUT.REQUEST);
        try {
            const response = yield call( callRequest, APIURL.CHECKOUT, {card_id, books}, "", {}, ApiSauce );
            if ( response.status === 1 ) {
                Util.DialogAlert(response.message, 'Success', 'success')
                responseCallBack(response)
                yield put(clearCartData())
                navigate(APP_ROUTES.HOME.routeName)
            } else {
                Util.DialogAlert(response.message)
                responseCallBack(null)
            }
        } catch (error) {
            Util.DialogAlert(error.message)
            responseCallBack(null)
        }
    }
}

function* deleteCard() {
    while (true) {
        const { cardId, responseCallBack } = yield take(ActionTypes.DELETE_CARD);
        try {
            const response = yield call( callRequest, APIURL.DELETE_CARD, {card_id: cardId}, "", {}, ApiSauce );
            if ( response.status === 1 ) {
                Util.DialogAlert(response.message, 'Success', 'success')
                responseCallBack(response)
            } else {
                Util.DialogAlert(response.message)
                responseCallBack(null)
            }
        } catch (error) {
            Util.DialogAlert(error.message)
            responseCallBack(null)
        }
    }
}

export default function* root() {
    yield fork(getAllCards);
    yield fork(addNewCard);
    yield fork(checkout);
    yield fork(deleteCard);
}