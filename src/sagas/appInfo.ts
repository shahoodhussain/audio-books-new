import { take, put, call, fork } from "redux-saga/effects";
import { callRequest } from "../config/WebService";
import APIURL from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";
import ActionTypes from "../redux/ActionTypes";
import { appUsageContentFailure, appUsageContentSuccess } from "../redux/actions/appInfo.actions";

function* usageContent() {
    while (true) {
        const { content } = yield take(ActionTypes.APP_USAGE_POLICIES.REQUEST);
        try {
            const response = yield call( callRequest, APIURL.APP_USAGE_CONTENT_URL, {type: content}, "", {}, ApiSauce );
            if ( response.status === 1 ) {
                yield put( appUsageContentSuccess(response.data.content) );
            } else yield put(appUsageContentFailure())
        } catch (error) {
            Util.DialogAlert(error.message)
            yield put(appUsageContentFailure())
        }
    }
}

export default function* root() {
    yield fork(usageContent);
}