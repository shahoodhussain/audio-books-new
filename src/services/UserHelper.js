import { userSignOutRequest } from "../redux/actions/UserActions";
import Util from "../util";
import { SESSION_EXPIRED_ERROR } from "../constants";

const logoutUserHelper = () => {
  Util.getStore().dispatch(userSignOutRequest());
  setTimeout(() => {
    Util.topAlertError(SESSION_EXPIRED_ERROR);
  }, 1000);
};

export { logoutUserHelper };
