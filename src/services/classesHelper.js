const generateFormData = (payload) => {
  const formData = new FormData();
  for( let key in payload ) {
    formData.append(key, payload[key])
  }
  return formData;
}


export { generateFormData };
