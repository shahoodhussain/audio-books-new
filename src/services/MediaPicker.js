// @flow
// import ImagePicker from "react-native-image-picker";
import ImagePicker from 'react-native-image-crop-picker';
import { Linking, Alert } from "react-native";
import { IMAGE_QUALITY, IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT } from "../constants";
import Util from "../util";

const IMAGE_PICKER_OPTIONS = {
  quality: IMAGE_QUALITY,
  maxWidth: IMAGE_MAX_WIDTH,
  maxHeight: IMAGE_MAX_HEIGHT,
  title: "Select Image",
  cancelButtonTitle: "cancel",
  takePhotoButtonTitle: "Camera",
  allowsEditing: true,
  chooseFromLibraryButtonTitle: "Gallery",
  mediaType: "photo",
  permissionDenied: {
    title: "Permission Denied"
  }
};

const LOG = __DEV__ && false;

class MediaPicker {
  showImagePicker(cb, options = IMAGE_PICKER_OPTIONS) {
    this.openShowImagePicker(options, cb);
  }

  pickImageFromCamera(cb) {
    ImagePicker.openCamera({
      width: IMAGE_PICKER_OPTIONS.maxWidth,
      height: IMAGE_PICKER_OPTIONS.maxHeight,
      cropping: true,
    }).then(image => {
      console.log('iamge', image);
      // this.resizeImage(image.uri, cb);
    }).catch(error => {
      console.log('error', error);
    })
  }

  openShowImagePicker(cb) {
    ImagePicker.openPicker({
      width: IMAGE_PICKER_OPTIONS.maxWidth,
      height: IMAGE_PICKER_OPTIONS.maxHeight,
      cropping: true
    }).then(image => {
      console.log('image', image);
      if(image.path && cb) {
        const imageUrl = Util.isPlatformAndroid() ? "file://" + image.path : image.path;
        cb(imageUrl, image);
      }
    }).catch(error => {
      // this.openSettingModal();
      console.log('error', error)
    })
  }

  openShowVideoPicker(options, cb) {
    ImagePicker.showImagePicker(options, response => {
      console.log("openShowVideoPicker", response);
      if (response.uri && cb) {
        cb(Util.isPlatformAndroid() ? "file://" + response.path : response.uri);
      }
      if (response.error) {
        this.openSettingModal();
      }
    });
  }

  openSettingModal() {
    Alert.alert(
      "Permission required",
      "Need permissions to access gallery and camera",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        {
          text: "Open Settings",
          onPress: () => Linking.openURL('app-settings:')
        }
      ],
      { cancelable: false }
    );
  }

  clearMediaFiles() {
    ImagePicker.clean().then(() => {
      console.log('removed all tmp images from tmp directory');
    }).catch(e => {
      alert(e);
    });
  }
}

export default new MediaPicker();
