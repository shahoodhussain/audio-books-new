import React, { Component } from 'react'
import { Text, View, FlatList, Image, TouchableOpacity, RefreshControl } from 'react-native'
import { Fonts, Images, Metrics, Theme } from '../../assets/theme';
import { APP_ROUTES } from '../../router/RoutesConfig';
import { navigate } from '../../services/NavigationService';
import StarRating from 'react-native-star-rating';
import { connect } from "react-redux";
import { getPurchasedBooks } from "../../redux/actions/books.actions";

export class BookPurchases extends Component {
    state = {
        loading: true,
        allBooks: [],
        currentOffset: 0,
        lastOffset: 0,
    }

    componentDidMount() {
        this.getBookmarks()
    }

    getBookmarks() {
        const { currentOffset, allBooks, lastOffset } = this.state;
        this.setState({ loading: true })
        this.props.getPurchasedBooks(
            currentOffset,
            (books) => {
                console.log('books----',books)
                this.setState({
                    allBooks: [...allBooks, ...books],
                    loading: false,
                    currentOffset: books.length > 0 ? currentOffset + books.length + 1 : lastOffset,
                    lastOffset: currentOffset + books.length
                })
            }
        );
    }

    _renderBookItem(item) {
        console.log('item', item)
        const { id, title, is_purchased, is_saved, image, demo_file, chapter, author, category, rating } = item;
        return(
            <TouchableOpacity style={{flex: 1, marginHorizontal: 10, marginBottom: 20}} activeOpacity={1}
            onPress={()=> navigate(APP_ROUTES.BOOK_DETAILS.routeName, { item })}>
                <View style={{overflow: 'hidden', height: 180, flex: 1, flexDirection: 'row'}}>
                    <Image
                        source={{uri: image}}
                        style={{width: 140, resizeMode: 'cover', borderRadius: 15 }}
                    />
                    <View style={{flex: 1, marginLeft: 20, justifyContent: 'center'}}>
                        <Text numberOfLines={2} style={{color: Theme.white, fontFamily: Fonts.type.Regular, fontSize: Fonts.size.medium}}>{title}</Text>
                        <Text numberOfLines={2} style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>{author?.name}</Text>
                        <Text
                        style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>
                            {category?.name}
                        </Text>
                        <Text
                        style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>
                            Total Chapters: {chapter.length}
                        </Text>
                        <View style={{width: Metrics.screenWidth / 3.5, marginVertical: 5}}>
                            <StarRating
                                disabled={true}
                                maxStars={5}
                                rating={Math.round(rating)}
                                emptyStar={Images.StartDisable}
                                fullStar={Images.StarSelected}
                                starSize={17}
                            />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const { loading, allBooks } = this.state
        return (
            <FlatList 
                data={allBooks}
                renderItem={({item})=> this._renderBookItem(item)}
                style={{ marginTop: 10, paddingHorizontal: 10, flex: 1,minHeight: 100}}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={()=> <Text style={{
                    alignSelf: 'center',
                    marginTop: 50,
                    color: Theme.white,
                    fontSize: Fonts.size.large,
                    fontFamily: Fonts.type.CiceroMedium
                }}>You don't have any bookmarks</Text>}
                ListFooterComponent={()=> <View style={{width: 30}}/>}
                refreshControl={<RefreshControl refreshing={loading} onRefresh={()=> this.getBookmarks()} tintColor={Theme.white} colors={[Theme.black]}/>}
            />
        )
    }
}

const actions = { getPurchasedBooks };

export default connect(null, actions)(BookPurchases);