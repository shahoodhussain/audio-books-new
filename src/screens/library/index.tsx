import React, { Component } from 'react'
import { Text, TouchableOpacity, View, Image } from 'react-native'
import { Fonts, Images, Metrics, Theme } from '../../assets/theme'
import { APP_ROUTES } from '../../router/RoutesConfig'
import { navigate } from '../../services/NavigationService'
import StarRating from 'react-native-star-rating';
import { connect } from "react-redux";
import { getLibraryBooks } from "../../redux/actions/books.actions";
import BookMarks from './bookmarks'
import BookPurchases from './purchases'

export class Library extends Component {
    state = {
        selected: 1,
        bookMarks: [],
        loading: true,
        allBooks: [],
        currentOffset: 0,
        lastOffset: 0,
    }

    componentDidMount() {
        this.getBookmarks()
    }

    getBookmarks() {
        const { currentOffset, allBooks, lastOffset } = this.state;
        this.setState({ loading: true })
        this.props.getLibraryBooks(
            currentOffset,
            (books) => {
                this.setState({
                    allBooks: [...allBooks, ...books],
                    loading: false,
                    currentOffset: books.length > 0 ? currentOffset + books.length + 1 : lastOffset,
                    lastOffset: currentOffset + books.length
                })
            }
        );
    }

    _renderBookItem(item) {
        const { id, title, is_purchased, is_saved, image, demo_file, chapter, author, category, rating } = item;
        return(
            <TouchableOpacity style={{flex: 1, marginHorizontal: 10, marginBottom: 20}} activeOpacity={1}
            onPress={()=> navigate(APP_ROUTES.BOOK_DETAILS.routeName, { item })}>
                <View style={{overflow: 'hidden', height: 180, flex: 1, flexDirection: 'row'}}>
                    <Image
                        source={{uri: image}}
                        style={{width: 140, resizeMode: 'cover', borderRadius: 15 }}
                    />
                    <View style={{flex: 1, marginLeft: 20, justifyContent: 'center'}}>
                        <Text numberOfLines={2} style={{color: Theme.white, fontFamily: Fonts.type.Regular, fontSize: Fonts.size.medium}}>{title}</Text>
                        <Text numberOfLines={2} style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>{author?.name}</Text>
                        <Text
                        style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>
                            {category?.name}
                        </Text>
                        <Text
                        style={{color: '#ffffff70', fontFamily: Fonts.type.Regular, fontSize: Fonts.size.xxSmall}}>
                            Total Chapters: {chapter.length}
                        </Text>
                        <View style={{width: Metrics.screenWidth / 3.5, marginVertical: 5}}>
                            <StarRating
                                disabled={true}
                                maxStars={5}
                                rating={Math.round(rating)}
                                emptyStar={Images.StartDisable}
                                fullStar={Images.StarSelected}
                                starSize={17}
                                // selectedStar={(rating) => this.onStarRatingPress(rating)}
                            />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const { selected, bookMarks, allBooks, loading } = this.state
        return (
            <View style={{flex: 1}}>
                <View style={{flexDirection: 'row', marginHorizontal: 20, marginTop: 10, flex: 0}}>
                    <TouchableOpacity style={{borderBottomColor: Theme.themeFontColor, borderBottomWidth: selected === 1 ? 3 : 0, marginBottom: 20, marginRight: 10}}
                    activeOpacity={1}
                    onPress={()=> this.setState({ selected: 1 })}>
                        <Text
                            style={{
                                color: selected === 1 ? Theme.white : Theme.rgbaWhite,
                                fontFamily: Fonts.type.CiceroMedium,
                                fontSize: Fonts.size.xLarge,
                            }}
                        >Bookmarks</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{borderBottomColor: Theme.themeFontColor, borderBottomWidth: selected === 2 ? 3 : 0, marginBottom: 20, marginLeft: 10}}
                    activeOpacity={1}
                    onPress={()=> this.setState({ selected: 2 })}>
                        <Text
                            style={{
                                color: selected === 2 ? Theme.white : Theme.rgbaWhite,
                                fontFamily: Fonts.type.CiceroMedium,
                                fontSize: Fonts.size.xLarge,
                            }}
                        >Purchases</Text>
                    </TouchableOpacity>
                </View>
                {
                    selected === 1 ? <BookMarks /> : <BookPurchases />
                }
            </View>
        )
    }
}

const actions = { getLibraryBooks };

export default connect(null, actions)(Library);