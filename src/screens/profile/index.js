import React, {Component} from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity, ActivityIndicator, LayoutAnimation} from 'react-native';
import { Fonts, Images, Theme } from '../../assets/theme';
import { connect } from "react-redux";
import { userLogoutRequest } from "../../redux/actions/UserActions";
import { navigate } from '../../services/NavigationService';
import { APP_ROUTES, ROUTES } from '../../router/RoutesConfig';

class EditProfile extends Component {
  state = {
    firstName: '',
    lastName: '',
    deliveryAddress: '',
    contactNumber: '',
    city: '',
    region: '',
    postalCode: '',
    country: '',
    cardNumber: '',
    expDate: '',
    cvv: '',
  };

  render() {
    const {
      firstName,
      lastName,
      deliveryAddress,
      contactNumber,
      city,
      region,
      postalCode,
      country,
      cardNumber,
      expDate,
      cvv,
    } = this.state;

    const { userData } = this.props.userData
    return (
      <View style={{flex: 1}}>
        <View style={{ alignItems: 'center'}}>
          <View style={{overflow: 'hidden', zIndex: 99}}>
            <Image 
              source={userData?.image ? {uri: userData?.image} : Images.Avatar}
              style={{width: 140, height: 140, resizeMode: 'cover', borderRadius: 75, borderWidth: 5, borderColor: Theme.white, backgroundColor: Theme.primaryBackground}}
            />
          </View>
          <View style={{position: 'absolute', height: 70, backgroundColor: Theme.themeFontColor, width: '100%', borderBottomLeftRadius: 30, borderBottomRightRadius: 30,}}/>
        </View>
        <View style={{marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: Theme.white, fontSize: Fonts.size.large, fontFamily: Fonts.type.CiceroMedium}}>
            {userData?.name}
          </Text>
        </View>
        <ScrollView style={{paddingHorizontal: 20, marginTop: 10}}
        showsVerticalScrollIndicator={false}>
          <TouchableOpacity
            style={{
              backgroundColor: Theme.themeFontColor, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginVertical: 10,
              elevation: 10,
            }}
            onPress={()=> navigate(APP_ROUTES.CARD.routeName)}
          >
            <Text
              style={{
                color: Theme.white,
                fontFamily: Fonts.type.Medium,
                fontSize: Fonts.size.medium
              }}
            >Payment Options</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: Theme.themeFontColor, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginVertical: 10,
              elevation: 10,
            }}
            onPress={()=> navigate(APP_ROUTES.CART.routeName)}
          >
            <Text
              style={{
                color: Theme.white,
                fontFamily: Fonts.type.Medium,
                fontSize: Fonts.size.medium
              }}
            >Cart</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: Theme.themeFontColor, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginVertical: 10,
              elevation: 10,
            }}
            activeOpacity={0.9}
            onPress={()=> navigate(ROUTES.CHANGE_PASSWORD.routeName)}
          >
            <Text
              style={{
                color: Theme.white,
                fontFamily: Fonts.type.Medium,
                fontSize: Fonts.size.medium
              }}
            >Change Password</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: Theme.themeFontColor, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginVertical: 10,
              elevation: 10,
            }}
            onPress={()=> navigate(ROUTES.TERMS_AND_CONDITIONS.routeName)}
            activeOpacity={0.9}
          >
            <Text
              style={{
                color: Theme.white,
                fontFamily: Fonts.type.Medium,
                fontSize: Fonts.size.medium
              }}
            >Terms & Conditions</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: Theme.themeFontColor, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginVertical: 10,
              elevation: 10,
            }}
            onPress={()=> navigate(ROUTES.PRIVACY_POLICY.routeName)}
            activeOpacity={0.9}
          >
            <Text
              style={{
                color: Theme.white,
                fontFamily: Fonts.type.Medium,
                fontSize: Fonts.size.medium
              }}
            >Privacy Policy</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: Theme.themeFontColor, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginVertical: 10,
              elevation: 10,
            }}
            activeOpacity={0.9}
            onPress={()=> {LayoutAnimation.linear(), this.props.userLogoutRequest()}}
            disabled={this.props.userData.loading}
          >
            {
              this.props.userData.loading ? <ActivityIndicator 
                color={Theme.white}
                size='large'
              /> : <Text
                style={{
                  color: Theme.white,
                  fontFamily: Fonts.type.Medium,
                  fontSize: Fonts.size.medium
                }}
              >Logout</Text>
            }
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  userData: user
});

const actions = {userLogoutRequest};

export default connect(mapStateToProps, actions)(EditProfile);
