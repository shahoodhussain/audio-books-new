import { StyleSheet } from "react-native";
import { AppStyles, Fonts, Metrics, Theme } from "../../assets/theme";

export default StyleSheet.create({
    container: {
        flexGrow: 1,
    },
    logoComponent: {
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.CiceroMedium,
        fontSize: Metrics.normalize(Fonts.size.medium),
        marginBottom: 30
    },
    cameraBorder: {
        borderWidth: 2,
        borderColor: Theme.error,
        borderRadius: 100,
        overflow: 'hidden',
        width: 130,
        height: 130,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageContainer: {
        backgroundColor: Theme.primaryDark,
        width: 200,
        height: 200,
        justifyContent: 'center',
    },
    cameraImage: {
        width: 80,
        height: 80,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    avatar: {
        resizeMode: 'contain',
        backgroundColor: 'red',
        flex: 1
    },
    bottomContainer: {
        // flex: 2,
        // marginTop: 50,
        // backgroundColor: 'red',
        flex: 1,
        marginHorizontal: 20,
    },
    iconStyles: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20
    },
    buttonIcon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        // marginHorizontal: 20,
        // marginRight: 10,
        // tintColor: Theme.white,
    },
    textWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    inputLabelStyle: {
        color: Theme.rgbaWhite,
        fontSize: Fonts.size.xSmall,
    },
    inputStyle: {
        height: 50,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Regular,
        fontWeight: '500',
        color: Theme.white,
        paddingRight: 10,
    },
    profilePictureText: {
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.small
    },
    formContainer: {
        flex: 1,
        marginHorizontal: 20
    },
    inputContainer: {
        marginVertical: 5
    },
    input: {
        borderColor: Theme.lightText,
        borderWidth: 0.5,
        height: 40,
        paddingHorizontal: 10,
        borderRadius: 6,
        marginVertical: 10,
    },
    inputArea: {
        height: 100,
    },
    buttonContainer: {
        overflow: 'hidden',
        alignItems: 'center'
    },
    button: {
        flexDirection: 'row',
        height: 65,
        overflow: 'hidden',
        marginVertical: 10,
        borderBottomWidth: 2,
        borderColor: Theme.white,
        justifyContent: 'center',
        alignItems: 'center'
    },
    updateText: {
        fontSize: Fonts.size.xSmall,
        fontFamily: Fonts.type.Regular,
    },
    signupContainer: {
        // flex: 1,
        marginTop: 20,
        alignItems: 'center'
    },
    signupButton: {
        width: '40%',
        overflow: 'hidden',
        backgroundColor: Theme.themeFontColor,
        paddingVertical: 14,
        borderRadius: 30,
        ...AppStyles.shadow7
    },
    signupText: {
        color: Theme.white,
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.type.Regular
    },
})