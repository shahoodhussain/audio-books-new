import React, { Component } from 'react'
import { Image, KeyboardAvoidingView, ScrollView, Text, TouchableOpacity, View, PermissionsAndroid, Platform } from 'react-native'
import { Images } from '../../assets/theme';
import { ButtonView } from '../../components';
import { Fumi } from 'react-native-textinput-effects';
import CompStyles from './styles';
import MediaPicker from "../../services/MediaPicker";
import { connect } from "react-redux";
import { completeProfileRequest, editUserProfileRequest } from "../../redux/actions/UserActions";
import { generateFormData } from '../../services/classesHelper'
import _ from 'lodash'
import util from '../../util';
import { goBack } from '../../services/NavigationService';

export class ProfileCreation extends Component {

    state = {
        imageUrl: null,
        name: this.props.userData?.userData?.name,
        number: this.props.userData?.userData?.phone_no,
        address: this.props.userData?.userData?.address,
        loading: false
    }

    INPUT_TYPES = [
        {
            name: 'name',
            iconBackgroundColor: '#161415',
            textBackgroundColor: '#231f20',
            lableStyle: CompStyles.inputLabelStyle,
            inputLabel: 'Full Name',
            inputStyle: CompStyles.inputStyle,
            secureTextEntry: false,
            buttonIcon: Images.User,
            eyeIcon: false,
            reference: null,
            keyboardType: 'default',
            value: this.props.userData?.userData?.name
        },
        {
            name: 'number',
            iconBackgroundColor: '#314c8c',
            textBackgroundColor: '#3e5896',
            lableStyle: CompStyles.inputLabelStyle,
            inputLabel: 'Phone Number',
            inputStyle: CompStyles.inputStyle,
            secureTextEntry: true,
            buttonIcon: Images.Phone,
            eyeIcon: true,
            reference: null,
            keyboardType: 'phone-pad',
            value: this.props.userData?.userData?.phone_no
        },
        {
            name: 'address',
            iconBackgroundColor: '#dc1c2f',
            textBackgroundColor: '#f3343e',
            lableStyle: CompStyles.inputLabelStyle,
            inputLabel: 'Street Address',
            inputStyle: CompStyles.inputStyle,
            secureTextEntry: true,
            buttonIcon: Images.Email,
            eyeIcon: true,
            reference: null,
            keyboardType: 'default',
            value: this.props.userData?.userData?.address
        },
    ];
    
    async selectImage() {
        if(util.isPlatformAndroid()) {
            try {
                const granted = await PermissionsAndroid.request(
                  PermissionsAndroid.PERMISSIONS.CAMERA,
                  {
                    title: "Cool Photo App Camera Permission",
                    message:
                      "Cool Photo App needs access to your camera " +
                      "so you can take awesome pictures.",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                  }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    MediaPicker.openShowImagePicker((imageUrl, response)=> {
                        if(imageUrl) {
                            this.setState({ imageUrl: imageUrl })
                        }
                    })
                } else {
                    util.DialogAlert('Photo access not allowed')
                    console.log("Camera permission denied");
                }
            } catch (err) {
                console.warn(err);
            }
        } else {
            MediaPicker.openShowImagePicker((imageUrl, response)=> {
                if(imageUrl) {
                    this.setState({ imageUrl: imageUrl })
                }
            })
        }
    }

    onChangeText(text, type) {
        this.setState({ [type]: text })
    }

    profileCompleted() {
        const { name, number, address, imageUrl } = this.state;
        if( _.isEmpty(name) ) {
            util.DialogAlert('Enter your full name', 'Error', 'error')
        } else if( !util.isNumber(number) ) {
            util.DialogAlert('Enter your phone number', 'Error', 'error')
        } else if( _.isEmpty(address) ) {
            util.DialogAlert('Enter your address', 'Error', 'error')
        }
        // else if( _.isEmpty(imageUrl) ) {
        //     util.DialogAlert('Please select your image', 'Error', 'error')
        // }
        else {
            this.setState({ loading: true })
            const payload = {
                name,
                phone_no: number,
                address,
                image: imageUrl,
            }
            this.props.editUserProfileRequest(generateFormData(payload), responseData => {
                console.log('responsedata', responseData)
                this.setState({ loading: false })
                goBack();
            })
        }
    }

    render() {
        const { imageUrl, loading } = this.state;
        const { image = ''} = this.props.userData?.userData;
        return (
            <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={{flex: 1}}
            >
                <ScrollView
                contentContainerStyle={{flexGrow: 1}}
                keyboardShouldPersistTaps="handled"
                >
                    <View style={CompStyles.logoComponent}>
                        <View style={CompStyles.cameraBorder}>
                            <TouchableOpacity style={CompStyles.imageContainer}
                            activeOpacity={1}
                            onPress={()=> this.selectImage()}>
                                <Image 
                                style={imageUrl ? CompStyles.avatar : CompStyles.cameraImage}
                                source={image ? {uri: image} : imageUrl ? {uri: imageUrl} : Images.Camera}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={CompStyles.bottomContainer}>
                        {
                            this.INPUT_TYPES.map((inputType, index) => {
                                return(
                                    <View
                                        style={CompStyles.button}
                                        key={index}>
                                        <View style={CompStyles.textWrapper}>
                                            <Fumi
                                                iconName={'university'}
                                                iconColor={'transparent'}
                                                passiveIconColor={'transparent'}
                                                iconSize={0}
                                                iconWidth={0}
                                                inputPadding={16}
                                                selectionColor='#fff'
                                                keyboardType={inputType.keyboardType}
                                                ref={ref => inputType.reference = ref}
                                                onSubmitEditing={()=> inputType.name !== 'address' && this.INPUT_TYPES[index+1].reference.focus()}
                                                returnKeyType={inputType.name !== 'address' ? 'next' : 'done'}
                                                label={inputType.inputLabel}
                                                labelStyle={inputType.lableStyle}
                                                inputStyle={inputType.inputStyle}
                                                style={{
                                                    backgroundColor: 'transparent',
                                                }}
                                                value={this.state[inputType.name]}
                                                onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                            />
                                        </View>
                                    </View>
                                )
                            })
                        }
                        <View style={CompStyles.signupContainer}>
                            <ButtonView
                            shouldAnimate
                            disabled={loading}
                            style={CompStyles.signupButton}
                            onPress={()=> this.profileCompleted()}>
                                <Text style={CompStyles.signupText}>Done</Text>
                            </ButtonView>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = ({ user }) => ({
    userData: user
});

const actions = { completeProfileRequest, editUserProfileRequest };

export default connect(
    mapStateToProps,
    actions
)(ProfileCreation);
