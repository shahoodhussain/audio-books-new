import React, { Component, } from 'react'
import { Text, View, StyleSheet, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native'
import { Theme, Fonts } from '../../assets/theme'
import { connect } from "react-redux";
import { APP_ROUTES } from '../../router/RoutesConfig';
import { navigate } from '../../services/NavigationService';

export class Cart extends Component {

    _renderCartItem(item) {
        const {id, title, author, price, image} = item;
        return (
            <View style={style.mainHead} key={id}>
                <View style={{}}>
                    <Image style={style.imageView} source={{ uri: image }} />
                </View>
                <View style={style.textHead}>
                    <Text style={style.textStyle}>{title}</Text>
                    <Text style={style.textStyle}>{author?.name}</Text>
                    <Text style={style.textStyle}>${price}</Text>
                </View>
            </View>
        )
    }

    totalPrice() {
        const { cartItems } = this.props;
        let totalPrice = 0;
        if(cartItems.length === 0) return totalPrice;
        cartItems.map(item => {
            totalPrice += item.price;
        })
        return totalPrice
    }

    _renderEmptyComponent() {
        return (
            <View style={{alignSelf: 'center', marginVertical: 10}}>
                <Text style={{
                    color: Theme.white,
                    fontSize: Fonts.size.medium,
                    fontFamily: Fonts.type.CiceroMedium,
                    textAlign: 'center'
                }}>No books in the card{'\n'}
                    Add items in cart.</Text>
            </View>
        )
    }

    render() {
        const { cartItems } = this.props
        if( cartItems.length === 0 ) {
            return this._renderEmptyComponent()
        }
        return (
            <View style={{flex:1}}>
                <FlatList
                    data={cartItems}
                    keyExtractor={(item)=> item?.id.toString()}
                    renderItem={({item})=> this._renderCartItem(item)}
                    ListEmptyComponent={()=> this._renderEmptyComponent()}
                    style={{ paddingTop: 20 }}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={()=> <View style={{width: 30}}/>}
                />
                {/* <View style={style.rowHead}>
                    <Text style={style.textHeading}>Sub Total</Text>
                    <Text style={style.textHeadingPrice}>$40.00</Text>
                </View> */}
                <View style={style.rowHead}>
                    <Text style={style.textHeading}>Total Price</Text>
                    <Text style={style.textHeadingPrice}>${this.totalPrice()}</Text>
                </View>
                <TouchableOpacity
                    style={{
                        backgroundColor: Theme.themeFontColor, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 30, marginHorizontal: 20, marginVertical: 10, elevation: 10,
                    }}
                    activeOpacity={0.9}
                    onPress={()=> navigate(APP_ROUTES.PAYMENT_OPTIONS.routeName, {isCheckout: true})}
                >
                    <Text
                    style={{
                        color: Theme.white,
                        fontFamily: Fonts.type.Medium,
                        fontSize: Fonts.size.medium
                    }}
                    >Proceed To Checkout</Text>
                </TouchableOpacity>
            </View>

        )
    }
}

const mapStateToProps = ({ cart }) => ({
    cartItems: cart.cartItems
});

export default connect(mapStateToProps)(Cart);

const style = StyleSheet.create({

    textHead: { flex: 1, paddingHorizontal: 10, alignSelf: 'center', },
    textStyle: {
        color: 'white',
        fontSize: 17,
        padding: 2
    },
    imageView: { height: 140, width: 100, borderRadius: 10 },
    mainHead: { flexDirection: 'row', marginHorizontal: 20, paddingBottom: 10, marginVertical: 10, borderBottomWidth: .5, borderBottomColor: 'white' },


    rowHead: { flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 },
    textHeading: { color: Theme.white,
        fontFamily: Fonts.type.Medium,
        fontSize: Fonts.size.normal },
    textHeadingPrice: { fontSize: 16, fontWeight: 'bold', color: 'white' },

    buttonHead: { backgroundColor: Theme.themeFontColor, width: '90%', alignSelf: 'center', borderRadius: 50, height: 50, marginVertical: 10, alignItems: 'center', justifyContent: 'center' },
    buttonText: { color: 'white', fontWeight: 'bold', textAlign: 'center' },

})
