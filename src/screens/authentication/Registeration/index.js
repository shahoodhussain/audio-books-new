import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, KeyboardAvoidingView, ScrollView, Platform } from 'react-native'
import { connect } from "react-redux";
import {ButtonView, AppLogo, BackgroundImageWrapper} from "../../../components";
import {Images} from "../../../assets/theme";
import Util from "../../../util";
import _ from "lodash";
import { registerUserRequest } from "../../../redux/actions/UserActions";
import CompStyles from "./styles";
import { Fumi } from 'react-native-textinput-effects';

const INPUT_TYPES = [
    {
        name: 'email',
        iconBackgroundColor: '#161415',
        textBackgroundColor: '#231f20',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Email',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: false,
        buttonIcon: Images.Email,
        eyeIcon: false,
        reference: null
    },
    {
        name: 'password',
        iconBackgroundColor: '#dc1c2f',
        textBackgroundColor: '#f3343e',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Password',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: true,
        buttonIcon: Images.PasswordLock,
        eyeIcon: true,
        reference: null
    },
    {
        name: 'confirmpassword',
        iconBackgroundColor: '#314c8c',
        textBackgroundColor: '#3e5896',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Confirm Password',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: true,
        buttonIcon: Images.PasswordLock,
        eyeIcon: true,
        reference: null
    },
];

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false,
            showConfirmPassword: false,
            email: '',
            password: '',
            confirmPassword: '',
        }
    }

    signUp() {
        const {email, password, confirmPassword} = this.state;
        if( _.isEmpty(email) ) {
            Util.DialogAlert('Enter email address', 'Error');
        } else if( !Util.isEmailValid(email) ) {
            Util.DialogAlert('Invalid email', 'Error');
        } else if( _.isEmpty(password) ) {
            Util.DialogAlert('Enter password', 'Error');
        } else if( !Util.isValidPasswordFormat(password) ) {
            Util.DialogAlert('Invalid password\nPassword must be 6 characters\nMust contain upper and lower case characters\nMust have special and number characters', 'Error');
        } else if( _.isEmpty(confirmPassword) ) {
            Util.DialogAlert('Enter confirmation password', 'Error');
        } else if( confirmPassword !== password ) {
            Util.DialogAlert('Password Mismatch', 'Error');
        } else {
            const payload = {
                email: email,
                password: password,
                device_token: '',
                device_type: Platform.OS,
            }
            this.props.registerUserRequest(payload);
        }
    }

    onChangeText(text, inputType) {
        switch (inputType) {
            case 'email':
                this.setState({ [inputType]: text })
                break;
            case 'password':
                this.setState({ [inputType]: text })
                break;
            case 'confirmpassword':
                this.setState({ confirmPassword: text })
                break;
            default:
                break;
        }
    }

    _renderImageIcon(fieldName) {
        const { showConfirmPassword, showPassword } = this.state;
        switch (fieldName) {
            case 'password':
                return <TouchableOpacity onPress={()=> this.setState({ showPassword: !showPassword })} style={CompStyles.iconWrapper}>
                        <Image source={showPassword ? Images.openEye : Images.closeEye} style={CompStyles.eyeIcon}/>
                    </TouchableOpacity>
            case 'confirmpassword':
                return <TouchableOpacity onPress={()=> this.setState({ showConfirmPassword: !showConfirmPassword })} style={CompStyles.iconWrapper}>
                        <Image source={showConfirmPassword ? Images.openEye : Images.closeEye} style={CompStyles.eyeIcon}/>
                    </TouchableOpacity>
            default:
                break;
        }
    }

    render() {
        const { userData } = this.props;
        const { showConfirmPassword, showPassword } = this.state;
        return (
            <BackgroundImageWrapper>
                <KeyboardAvoidingView
                    contentContainerStyle={CompStyles.flexGrow}
                    style={{flex: 1}}
                    behavior={'padding'}
                    keyboardShouldPersistTaps="handled"
                    enableOnAndroid={true}>
                    <ScrollView
                    contentContainerStyle={CompStyles.flexGrow}>
                        <View style={CompStyles.logoComponent}>
                            <Text style={CompStyles.loginText}>SIGNUP</Text>
                            <AppLogo />
                        </View>
                        <View style={CompStyles.bottomContainer}>
                            {
                                INPUT_TYPES.map((inputType, index) => {
                                    return(
                                        <View
                                            style={CompStyles.button}
                                            key={index}>
                                            <View style={CompStyles.iconStyles}>
                                                <Image 
                                                    source={inputType.buttonIcon}
                                                    style={CompStyles.buttonIcon}
                                                />
                                            </View>
                                            <View style={CompStyles.textWrapper}>
                                                <Fumi
                                                    iconName={'university'}
                                                    iconColor={'transparent'}
                                                    passiveIconColor={'transparent'}
                                                    iconSize={0}
                                                    iconWidth={0}
                                                    inputPadding={16}
                                                    selectionColor='#fff'
                                                    ref={ref => inputType.reference = ref}
                                                    onSubmitEditing={()=> inputType.name !== 'confirmpassword' && INPUT_TYPES[index+1].reference.focus()}
                                                    returnKeyType={inputType.name !== 'confirmpassword' ? 'next' : 'done'}
                                                    label={inputType.inputLabel}
                                                    labelStyle={inputType.lableStyle}
                                                    secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                                                    inputStyle={inputType.inputStyle}
                                                    style={{
                                                        backgroundColor: 'transparent',
                                                    }}
                                                    onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                                />
                                            </View>
                                            {inputType.eyeIcon && this._renderImageIcon(inputType.name)}
                                        </View>
                                    )
                                })
                            }
                            <View style={CompStyles.signupContainer}>
                                <ButtonView
                                shouldAnimate
                                disabled={userData.loading}
                                style={CompStyles.signupButton}
                                onPress={()=> this.signUp()}>
                                    <Text style={CompStyles.signupText}>Sign Up</Text>
                                </ButtonView>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </BackgroundImageWrapper>
        )
    }
}

const mapStateToProps = ({ user }) => ({
    userData: user
});

const actions = {registerUserRequest};

export default connect(mapStateToProps, actions)(SignUp);
