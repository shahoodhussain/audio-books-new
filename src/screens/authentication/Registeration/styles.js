import { StyleSheet } from "react-native";
import { AppStyles, Fonts, Metrics, Theme } from "../../../assets/theme";

export default StyleSheet.create({
    flexGrow: {
        flexGrow: 1
    },
    logoComponent: {
        // height: '42%',
        // flex: 1,
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    scrollView: {
        flexGrow: 1,
        paddingTop: 10
    },
    bottomContainer: {
        flex: 1,
        marginHorizontal: 20,
    },
    buttonsComponent: {
        marginTop: 20,
    },
    signupHeading: {
        fontSize: Fonts.size.large,
        fontFamily: Fonts.type.SemiBold
    },
    signupSubHeading: {
        fontFamily: Fonts.type.Regular
    },
    button: {
        flexDirection: 'row',
        height: 65,
        overflow: 'hidden',
        marginVertical: 10,
        borderBottomWidth: 2,
        borderColor: Theme.white,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconStyles: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20
    },
    buttonIcon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        // marginHorizontal: 20,
        // marginRight: 10,
        // tintColor: Theme.white,
    },
    inputLabelStyle: {
        color: Theme.rgbaWhite,
        fontSize: Fonts.size.xSmall,
    },
    inputStyle: {
        height: 50,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Regular,
        fontWeight: '500',
        color: Theme.white,
        paddingRight: 10,
    },
    textWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    buttonText: {
        color: Theme.white,
        paddingLeft: 20,
        fontFamily: Fonts.type.Medium,
        letterSpacing: 0.2
    },
    iconWrapper: {
        justifyContent: 'center',
    },
    eyeIcon: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        tintColor: Theme.white,
        alignSelf: 'center',
        marginRight: 10
    },
    signupContainer: {
        // flex: 1,
        marginTop: 20,
        alignItems: 'center',
        paddingVertical: 20
    },
    signupButton: {
        // flex: 0,
        overflow: 'hidden',
        backgroundColor: Theme.themeFontColor,
        width: '40%',
        paddingVertical: 14,
        borderRadius: 30,
        ...AppStyles.shadow7
    },
    signupText: {
        color: Theme.white,
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.type.Regular
    },
    signInComponent: {
        alignItems: 'center'
    },
    accountText: {
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.xSmall,
    },
    underLine: {
        textDecorationLine: 'underline'
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.CiceroMedium,
        fontSize: Metrics.normalize(Fonts.size.medium),
        marginBottom: 30
    }
})