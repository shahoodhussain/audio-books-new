import { Platform, StyleSheet } from "react-native";
import { AppStyles, Fonts, Metrics, Theme } from "../../../assets/theme";

export default StyleSheet.create({
    logoComponent: {
        flex: 1,
        // height: '30%',
        // marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'red'
    },
    bottomContainer: {
        // backgroundColor: 'blue',
        flex: 1,
        // marginTop: 20,
        // justifyContent: 'flex-end',
        marginHorizontal: 25,
    },
    signupHeading: {
        fontSize: Fonts.size.large,
        fontFamily: Fonts.type.SemiBold
    },
    signupSubHeading: {
        fontFamily: Fonts.type.Regular
    },
    button: {
        flexDirection: 'row',
        // borderRadius: 40,
        height: 65,
        overflow: 'hidden',
        marginVertical: 10,
        borderBottomWidth: 2,
        borderColor: Theme.white,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconStyles: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20
        // borderRightWidth: 1,
        // borderColor: Theme.shadow
    },
    buttonIcon: {
        // marginTop: 2,
        width: 20,
        height: 20,
        resizeMode: 'contain',
        // marginHorizontal: 20,
        // tintColor: Theme.primary,
    },
    inputLabelStyle: {
        color: Theme.rgbaWhite,
        fontSize: Fonts.size.xSmall,
    },
    inputStyle: {
        height: 50,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Regular,
        fontWeight: '500',
        color: Theme.white,
        paddingRight: 10,
    },
    textWrapper: {
        flex: 1,
        justifyContent: 'center',
    },
    buttonText: {
        color: Theme.white,
        paddingLeft: 20,
        fontFamily: Fonts.type.Medium,
        letterSpacing: 0.2
    },
    iconWrapper: {
        marginRight: 5,
        justifyContent: 'center',
    },
    eyeIcon: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        tintColor: Theme.white,
        alignSelf: 'center',
        marginRight: 10
    },
    forgotPasswordContainer: {
        alignItems: 'flex-end',
        // marginTop: 30,
        // flex: 0.5,
    },
    forgotText: {
        color: Theme.white,
        fontSize: Metrics.normalize(Fonts.size.small),
        fontFamily: Fonts.type.Regular
    },
    signupContainer: {
        marginTop: 10,
        height: Platform.select({ ios: 55, android: Metrics.normalize(45) }),
        alignItems: 'center',
    },
    signupButton: {
        backgroundColor: Theme.themeFontColor,
        width: '40%',
        borderRadius: 40,
        overflow: 'hidden',
        ...AppStyles.shadow7
    },
    socailButton: {
        backgroundColor: Theme.primaryDark,
        paddingVertical: 13,
        borderRadius: 6
    },
    signupText: {
        color: Theme.white,
        fontSize: Metrics.normalize(Fonts.size.normal),
        fontFamily: Fonts.type.Regular
    },
    socialSignupText: {
        color: Theme.white,
        fontSize: Fonts.size.small,
        fontFamily: Fonts.type.Regular
    },
    connectTextContainer: {
        alignItems: 'center'
    },
    signInComponent: {
        paddingVertical: 15,
        borderRadius: 30,
        alignItems: 'center'
    },
    accountText: {
        color: Theme.white,
        fontFamily: Fonts.type.SemiBold,
        fontSize: Metrics.normalize(Fonts.size.small),
    },
    underLine: {
        textDecorationLine: 'underline'
    },
    connectContainer: {
        // flex: 0.3,
        // justifyContent: 'flex-end',
        // marginTop: 30,
        marginBottom: 10,
        // alignItems: 'center'
    },
    socialButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 20
    },
    iconImage: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        marginHorizontal: 5
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.CiceroMedium,
        fontSize: Metrics.normalize(Fonts.size.medium),
        marginBottom: 30
    }
})