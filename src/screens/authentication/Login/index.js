import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, KeyboardAvoidingView, Platform, ScrollView } from 'react-native'
import { connect } from "react-redux";
import { updateNavigation, userLoginRequest, showLoaderRequest } from "../../../redux/actions/UserActions";
import { navigate } from "../../../services/NavigationService";
import { ButtonView, AppLogo, BackgroundImageWrapper } from "../../../components";
import { Images, Metrics } from "../../../assets/theme";
import { ROUTES } from "../../../router/RoutesConfig";
import Util from "../../../util";
import _ from "lodash";
import CompStyles from "./styles";
import { Fumi } from 'react-native-textinput-effects';

const INPUT_TYPES = [
    {
        name: 'email',
        iconBackgroundColor: '#161415',
        textBackgroundColor: '#231f20',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Email Address',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: false,
        buttonIcon: Images.Email,
        eyeIcon: false,
        reference: null
    },
    {
        name: 'password',
        iconBackgroundColor: '#dc1c2f',
        textBackgroundColor: '#f3343e',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Password',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: true,
        buttonIcon: Images.PasswordLock,
        eyeIcon: true,
        reference: null
    }
];

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            showPassword: false,
            loading: false,
        }
    }

    login() {
        const {email, password} = this.state;
        if( _.isEmpty(email) ) {
            Util.DialogAlert('Enter email address', 'Error');
        } else if( !Util.isEmailValid(email) ) {
            Util.DialogAlert('Invalid email', 'Error');
        } else if( _.isEmpty(password) ) {
            Util.DialogAlert('Enter password', 'Error');
        } else if( !Util.isValidPasswordFormat(password) ) {
            Util.DialogAlert('invalid password', 'Error');
        } else {
            const payload = {
                email,
                password,
                device_token: 'static token',
                device_type: Platform.OS
            }
            this.props.userLoginRequest(payload);
        }
    }

    onChangeText(text, inputType) {
        switch (inputType) {
            case 'email':
                this.setState({ [inputType]: text })
                break;
            case 'password':
                this.setState({ [inputType]: text })
                break;
            default:
                break;
        }
    }

    _renderImageIcon(fieldName) {
        const { showPassword } = this.state;
        switch (fieldName) {
            case 'password':
                return <TouchableOpacity onPress={()=> this.setState({ showPassword: !showPassword })} style={CompStyles.iconWrapper}>
                        <Image source={showPassword ? Images.openEye : Images.closeEye} style={CompStyles.eyeIcon}/>
                    </TouchableOpacity>
            default:
                break;
        }
    }

    render() {
        const {showPassword} = this.state;
        const {userData} = this.props;
        return (
            <BackgroundImageWrapper>
                <KeyboardAvoidingView
                    style={{flex: 1}}
                    behavior={Platform.OS === 'android' ? 'padding' : Metrics.screenHeight > 750 ? 'padding' : 'position'}
                    contentContainerStyle={{flexGrow: 1}}
                    keyboardShouldPersistTaps="handled"
                    enableOnAndroid={true}>
                    <ScrollView style={{flex: 1}}
                    contentContainerStyle={{flexGrow: 1}}>
                        <View style={CompStyles.logoComponent}>
                            <Text style={CompStyles.loginText}>LOGIN</Text>
                            <AppLogo />
                        </View>
                        <View style={CompStyles.bottomContainer}>
                            {
                                INPUT_TYPES.map((inputType, index) => {
                                    return(
                                        <View
                                            style={CompStyles.button}
                                            key={index}>
                                            <View style={CompStyles.iconStyles}>
                                                <Image 
                                                    source={inputType.buttonIcon}
                                                    style={CompStyles.buttonIcon}
                                                />
                                            </View>
                                            <View style={CompStyles.textWrapper}>
                                                <Fumi
                                                    // iconClass={FontAwesome5}
                                                    iconName={'university'}
                                                    iconColor={'transparent'}
                                                    passiveIconColor={'transparent'}
                                                    iconSize={0}
                                                    iconWidth={0}
                                                    inputPadding={16}autoCapitalize="none"
                                                    label={inputType.inputLabel}
                                                    labelStyle={inputType.lableStyle}
                                                    inputStyle={inputType.inputStyle}
                                                    style={{
                                                        backgroundColor: 'transparent',
                                                    }}
                                                    selectionColor='#fff'
                                                    onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                                    ref={ref => inputType.reference = ref}
                                                    onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                                                    returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                                                    secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                                                />
                                            </View>
                                            {inputType.eyeIcon && this._renderImageIcon(inputType.name)}
                                        </View>
                                    )
                                })
                            }
                            <View style={CompStyles.forgotPasswordContainer}>
                                <Text style={CompStyles.forgotText}
                                onPress={()=> userData.loading ? null : navigate(ROUTES.FORGOT_PASSWORD.routeName)}>
                                    Forgot Password?
                                </Text>
                            </View>
                            <View style={{flex: 1, justifyContent: 'space-between', marginTop: 20}}>
                                <View style={CompStyles.signupContainer}>
                                    <ButtonView
                                    shouldAnimate
                                    disabled={userData.loading}
                                    style={CompStyles.signupButton}
                                    onPress={()=> this.login()}>
                                        <Text style={CompStyles.signupText}>Login</Text>
                                    </ButtonView>
                                </View>
                                <View style={CompStyles.connectContainer}>
                                    <View style={CompStyles.signInComponent}>
                                        <Text style={CompStyles.accountText} suppressHighlighting selectionColor='transparent' onPress={()=> navigate(ROUTES.SIGNUP_ROUTE.routeName)}>Don't have an account? <Text style={CompStyles.underLine}>Signup</Text></Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </BackgroundImageWrapper>
        )
    }
}

const mapStateToProps = ({ user }) => ({
    userData: user
});

const actions = {updateNavigation, userLoginRequest, showLoaderRequest};

export default connect(mapStateToProps, actions)(Login);