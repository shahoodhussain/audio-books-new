import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, ScrollView } from 'react-native';
import { Images } from "../../../assets/theme";
import { connect } from "react-redux";
import { updatePasswordRequest } from "../../../redux/actions/UserActions";
import _ from 'lodash'
import Util from "../../../util";
import { AppLogo, BackgroundImageWrapper, ButtonView } from "../../../components";
import CompStyles from "./styles";
import { Fumi } from 'react-native-textinput-effects';

const INPUT_TYPES = [
    {
        name: 'password',
        iconBackgroundColor: '#161415',
        textBackgroundColor: '#231f20',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'New Password',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: true,
        buttonIcon: Images.PasswordLock,
        eyeIcon: true,
        reference: null
    },
    {
        name: 'confirmpassword',
        iconBackgroundColor: '#dc1c2f',
        textBackgroundColor: '#f3343e',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Confirm New Password',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: true,
        buttonIcon: Images.PasswordLock,
        eyeIcon: true,
        reference: null
    }
];

class ResetPasswordComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            password: "",
            confirmPassword: "",
            showPass: false,
            showConfirmPass: false,
            showPassword: false
        };
    }

    updatePassword() {
        const {password, confirmPassword} = this.state;
        if( _.isEmpty(password) ) {
            Util.DialogAlert("Please enter new password");
        } else if( _.isEmpty(confirmPassword) ) {
            Util.DialogAlert("Please enter confirm password");
        } else if( password != confirmPassword ) {
            Util.DialogAlert("Password does not match");
        } else {
            const params = this.props.route.params?.payload
            const payload = {
                email: params?.email,
                otp: params?.otp,
                reference_code: params?.reference_code,
                new_password: password,
            }
            this.props.updatePasswordRequest(payload);
        }
    }

    onChangeText(text, inputType) {
        switch (inputType) {
            case 'password':
                this.setState({ password: text })
                break;
            case 'confirmpassword':
                this.setState({ confirmPassword: text })
                break;
            default:
                break;
        }
    }

    _renderImageIcon(fieldName) {
        const { showPassword, showConfirmPass } = this.state;
        switch (fieldName) {
            case 'password':
                return <TouchableOpacity onPress={()=> this.setState({ showPassword: !showPassword })} style={CompStyles.iconWrapper}>
                        <Image source={showPassword ? Images.openEye : Images.closeEye} style={CompStyles.eyeIcon}/>
                    </TouchableOpacity>
            case 'confirmpassword':
                return <TouchableOpacity onPress={()=> this.setState({ showConfirmPass: !showConfirmPass })} style={CompStyles.iconWrapper}>
                        <Image source={showConfirmPass ? Images.openEye : Images.closeEye} style={CompStyles.eyeIcon}/>
                    </TouchableOpacity>
            default:
                break;
        }
    }

    render() {
        const { showPassword, showConfirmPass } = this.state;
		return (
            <BackgroundImageWrapper>
                <KeyboardAvoidingView 
                contentContainerStyle={CompStyles.container}
                style={{flex: 1}}
                behavior={'padding'}
                keyboardShouldPersistTaps="handled"
                enableOnAndroid={true}>
                    <ScrollView
                    contentContainerStyle={{flexGrow: 1}}>
                        <View style={CompStyles.logoComponent}>
                            <Text style={CompStyles.loginText}>RESET PASSWORD</Text>
                            <AppLogo />
                        </View>
                        <View style={CompStyles.formContainer}>
                            {
                                INPUT_TYPES.map((inputType, index) => {
                                    return(
                                        <View
                                            style={CompStyles.button}
                                            key={index}>
                                            <View style={CompStyles.iconStyles}>
                                                <Image 
                                                    source={inputType.buttonIcon}
                                                    style={CompStyles.buttonIcon}
                                                />
                                            </View>
                                            <View style={CompStyles.textWrapper}>
                                                <Fumi
                                                    // iconClass={FontAwesome5}
                                                    iconName={'university'}
                                                    iconColor={'transparent'}
                                                    passiveIconColor={'transparent'}
                                                    iconSize={0}
                                                    iconWidth={0}
                                                    inputPadding={16}
                                                    ref={ref => inputType.reference = ref}
                                                    selectionColor='#fff'
                                                    onEndEditing={()=> inputType.name !== 'confirmpassword' && INPUT_TYPES[index+1].reference.focus()}
                                                    returnKeyType={inputType.name === 'password' ? 'next' : 'done'}
                                                    label={inputType.inputLabel}
                                                    labelStyle={inputType.lableStyle}
                                                    secureTextEntry={(inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPass}
                                                    inputStyle={inputType.inputStyle}
                                                    style={{
                                                        backgroundColor: 'transparent',
                                                    }}
                                                    onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                                />
                                            </View>
                                            {inputType.eyeIcon && this._renderImageIcon(inputType.name)}
                                        </View>
                                    )
                                })
                            }
                            <View style={CompStyles.signupContainer}>
                                <ButtonView
                                shouldAnimate
                                disabled={this.props.userData.loading}
                                style={CompStyles.signupButton}
                                onPress={()=> this.updatePassword()}>
                                    <Text style={CompStyles.signupText}>Reset</Text>
                                </ButtonView>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </BackgroundImageWrapper>
		);
	}
}

const mapStateToProps = ({ user }) => ({
    userData: user
});
const actions = { updatePasswordRequest };

export default connect(
    mapStateToProps,
    actions
)(ResetPasswordComponent);
