import { StyleSheet } from "react-native";
import { AppStyles, Fonts, Metrics, Theme } from "../../../assets/theme";

export default StyleSheet.create({
    container: {
        flexGrow: 1
    },
    logoComponent: {
        // flex: 0.9,
        // height: '38%',
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer: {
        marginHorizontal: 20,
        flex: 1,
    },
    inputContainer: {
        backgroundColor: Theme.white,
        borderRadius: 6,
        marginVertical: 5,
        paddingHorizontal: 10,
    },
    textInputStyles: {
        // backgroundColor: Theme.white,
        height: 50,
        backgroundColor: 'green'
    },
    inputLabelStyle: {
        color: Theme.rgbaWhite,
        fontSize: Fonts.size.xSmall,
    },
    inputStyle: {
        height: 50,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Regular,
        fontWeight: '500',
        color: Theme.white,
        paddingRight: 10,
    },
    textWrapper: {
        flex: 1,
        justifyContent: 'center',
    },
    button: {
        flexDirection: 'row',
        height: 65,
        overflow: 'hidden',
        marginVertical: 10,
        borderBottomWidth: 2,
        borderColor: Theme.white,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconStyles: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20
    },
    buttonIcon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        // marginHorizontal: 20,
    },
    iconWrapper: {
        justifyContent: 'center',
    },
    eyeIcon: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        tintColor: Theme.white,
        alignSelf: 'center',
        marginRight: 10
    },
    verificationText: {
        fontSize: Fonts.size.xLarge,
        fontFamily: Fonts.type.SemiBold
    },
    signupContainer: {
        marginTop: 10,
        height: 55,
        alignItems: 'center',
    },
    signupButton: {
        backgroundColor: Theme.themeFontColor,
        borderRadius: 30,
        width: '40%',
        overflow: 'hidden',
        ...AppStyles.shadow7
    },
    signupText: {
        color: Theme.white,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Regular
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.CiceroMedium,
        fontSize: Metrics.normalize(Fonts.size.medium),
        marginBottom: 30
    }
})