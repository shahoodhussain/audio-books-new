import { Fonts, Metrics, Theme } from "../../../assets/theme";
const { StyleSheet } = require("react-native");

export default StyleSheet.create({
    flexGrow: {
        flexGrow: 1
    },
    backgroundContainer: {
        height: Metrics.screenHeight,
        paddingHorizontal: 20,
    },
    text: {
        marginVertical: 20,
        letterSpacing: 0.8,
        lineHeight: 23,
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.type.Medium,
        color: Theme.white,
        textAlign: 'auto'
    }
})