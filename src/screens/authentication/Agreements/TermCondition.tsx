import React, { Component } from 'react'
import { Text, ScrollView, RefreshControl } from 'react-native'
import { connect } from "react-redux";
import { appUsageContentRequest } from "../../../redux/actions/appInfo.actions";
import styles from "./styles";
import { BackgroundImageWrapper } from '../../../components';
import { Fonts, Images, Theme } from '../../../assets/theme';

export class TermCondition extends Component {

    componentDidMount() {
        this.props.appUsageContentRequest('terms_and_conditions')
    }

    render() {
        const { isLoading, content } = this.props.appInfo
        return (
            <BackgroundImageWrapper image={Images.AppBackground}>
                <ScrollView 
                    contentContainerStyle={styles.flexGrow} style={styles.backgroundContainer}
                    refreshControl={
                        <RefreshControl 
                            onRefresh={()=> this.props.appUsageContentRequest('terms_and_conditions')}
                            refreshing={isLoading}
                            tintColor={Theme.white}
                            size={Fonts.size.xLarge}
                        />
                    }
                >
                    <Text style={styles.text}>
                        {content}
                    </Text>
                </ScrollView>
            </BackgroundImageWrapper>
        )
    }
}


const mapStateToProps = ({ appInfo }) => ({
    appInfo: appInfo
});

const actions = { appUsageContentRequest };

export default connect(mapStateToProps, actions)(TermCondition);