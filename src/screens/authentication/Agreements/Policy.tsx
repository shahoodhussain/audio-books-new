import React, { Component } from 'react'
import { Text, ScrollView, RefreshControl } from 'react-native'
import { connect } from "react-redux";
import { appUsageContentRequest } from "../../../redux/actions/appInfo.actions";
import styles from "./styles";
import { BackgroundImageWrapper } from '../../../components';
import { Fonts, Images, Theme } from '../../../assets/theme';

export class Policy extends Component {
    componentDidMount() {
        this.loadContent()
    }

    loadContent() {
        this.props.appUsageContentRequest('privacy_policy');
    }

    render() {
        const { isLoading, content } = this.props.appInfo;
        return (
            <BackgroundImageWrapper image={Images.AppBackground}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={styles.flexGrow} style={styles.backgroundContainer}
                    refreshControl={
                        <RefreshControl 
                            onRefresh={()=> this.props.appUsageContentRequest('privacy_policy')}
                            refreshing={isLoading}
                            tintColor={Theme.white}
                            size={Fonts.size.xLarge}
                        />
                    }
                >
                    <Text style={styles.text}>
                        {content}
                    </Text>
                </ScrollView>
            </BackgroundImageWrapper>
        )
    }
}

const mapStateToProps = ({ appInfo }) => ({
    appInfo: appInfo
});

const actions = { appUsageContentRequest };

export default connect(mapStateToProps, actions)(Policy);