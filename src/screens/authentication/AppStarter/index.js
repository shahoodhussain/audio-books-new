import React, { Component } from 'react'
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native'
import { Images, Theme } from '../../../assets/theme'
import CompStyles from './styles'
import Modal from 'react-native-modal'
import { navigate } from '../../../services/NavigationService'
import { ROUTES } from '../../../router/RoutesConfig'
import { AppLogo, BackgroundImageWrapper } from '../../../components'

const BUTTON_TYPES = [
    {
        name: 'apple',
        title: 'Signup with Apple',
        buttonIcon: Images.Apple,
        buttonStyle: CompStyles.appleButton,
    },
    {
        name: 'google',
        title: 'Signup with Google',
        buttonIcon: Images.Google,
        buttonStyle: CompStyles.googleButton,
    },
    {
        name: 'facebook',
        title: 'Signup with Facebook',
        buttonIcon: Images.Facebook,
        buttonStyle: CompStyles.facebookButton,
    },
    {
        name: 'email',
        title: 'Signup with Email',
        buttonIcon: Images.Email,
        buttonStyle: CompStyles.emailButton,
    },
];

export class AppStarter extends Component {

    state = {
        showAgrermentModal: false,
        isTermsAgreementAccepted: false,
        isPolicyAccepted: false,
        agreementError: false,
        policyError: false
    }

    onLoginSelection(selectionType) {
        switch (selectionType) {
            case 'email':
                this.setState({ showAgrermentModal: !this.state.showAgrermentModal })
                break;
            case 'facebook':
                // navigate(ROUTES.LOGIN.routeName);
                break;
            case 'google':
                break;
            case 'apple':
                break;
            case 'terms':
                this.setState({ showAgrermentModal: !this.state.showAgrermentModal })
                setTimeout(() => {
                    navigate(ROUTES.TERMS_AND_CONDITIONS.routeName);
                }, 0);
                break;
            case 'policy':
                this.setState({ showAgrermentModal: !this.state.showAgrermentModal })
                setTimeout(() => {
                    navigate(ROUTES.PRIVACY_POLICY.routeName);
                }, 0);
                break;
            default:
                break;
        }
    }

    validateAgreement() {
        const { isTermsAgreementAccepted, isPolicyAccepted, agreementError, policyError } = this.state;
        if(!isTermsAgreementAccepted) {
            this.setState({ agreementError: !agreementError })
        } else if(!isPolicyAccepted) {
            this.setState({ policyError: !policyError })
        } else {
            this.setState({ showAgrermentModal: false }, ()=> navigate(ROUTES.SIGNUP_ROUTE.routeName));
        }
    }

    onModalClose() {
        this.setState({ showAgrermentModal: false, isTermsAgreementAccepted: false, isPolicyAccepted: false, agreementError: false, policyError: false })
    }

    _renderAgreementModal() {
        const { showAgrermentModal, isTermsAgreementAccepted, isPolicyAccepted, agreementError, policyError } = this.state;
        const TERMS = 'terms', POLICY = 'policy'; 
        return(
            <Modal isVisible={true}
                statusBarTranslucent={false}
                backdropOpacity={0.7}
                onBackButtonPress={()=> this.onModalClose()}
                onBackdropPress={()=> this.onModalClose()}
            >
                <View style={CompStyles.modalContainer}>
                    <View style={CompStyles.modalAgreementHeader}>
                        <Text style={CompStyles.agreementHeading}>Agreement</Text>
                    </View>
                    <View style={CompStyles.policyContainer}>
                        <Text style={CompStyles.agreementSubHeading}>I have read and agreed with</Text>
                        <View style={CompStyles.agreementButton}>
                            <TouchableOpacity style={CompStyles.tickBox}
                            onPress={()=> this.setState({ isTermsAgreementAccepted: !isTermsAgreementAccepted, agreementError: false })}>
                                <Image source={isTermsAgreementAccepted ? Images.CheckMark : Images.EmptyBox} style={CompStyles.acceptImage}/>
                            </TouchableOpacity>
                            <Text style={CompStyles.termsText} onPress={()=> this.onLoginSelection(TERMS)}>Terms and Conditions</Text>
                        </View>
                        <View style={CompStyles.agreementButton}>
                            <TouchableOpacity style={CompStyles.tickBox}
                            onPress={()=> this.setState({ isPolicyAccepted: !isPolicyAccepted, policyError: false })}>
                                <Image source={isPolicyAccepted ? Images.CheckMark : Images.EmptyBox} style={CompStyles.acceptImage}/>
                            </TouchableOpacity>
                            <Text style={CompStyles.termsText} onPress={()=> this.onLoginSelection(POLICY)}>Privacy Policy</Text>
                        </View>
                    </View>
                    {agreementError && <Text style={CompStyles.modalErrorMessages}>Please accept terms and conditions</Text>}
                    {policyError && <Text style={CompStyles.modalErrorMessages}>Please accept privacy policy</Text>}
                    <View style={CompStyles.buttonContainer}>
                        <TouchableOpacity
                            style={[CompStyles.modalButton, CompStyles.acceptButton]}
                            onPress={()=> this.validateAgreement()}
                        >
                            <Text style={CompStyles.modalButtonText}>Accept</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[CompStyles.modalButton, CompStyles.declineButton]}
                            onPress={()=> this.setState({showAgrermentModal: !showAgrermentModal})}
                        >
                            <Text style={[CompStyles.modalButtonText, CompStyles.textBlack]}>Reject</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }

    render() {
        const { showAgrermentModal } = this.state;
        return (
            <BackgroundImageWrapper isNavigation={false} transparentBackground={false}>
            <ScrollView contentContainerStyle={CompStyles.scrollView}
            showsVerticalScrollIndicator={false}>
                <View style={CompStyles.logoComponent}>
                    <AppLogo />
                </View>
                <View style={CompStyles.bottomContainer}>
                    <View style={CompStyles.buttonsComponent}>
                        {
                            BUTTON_TYPES.map((buttonType, index) => {
                                return(
                                    <TouchableOpacity
                                        style={[CompStyles.button, {...buttonType.buttonStyle}]}
                                        key={index}
                                        onPress={()=> this.onLoginSelection(buttonType.name)}
                                        activeOpacity={0.7}
                                    >
                                        <View style={CompStyles.iconStyles}>
                                            <Image 
                                                source={buttonType.buttonIcon}
                                                style={[CompStyles.buttonIcon, buttonType.name === 'apple' && { tintColor: Theme.black }]}
                                            />
                                        </View>
                                        <View style={CompStyles.textWrapper}>
                                            <Text style={[CompStyles.buttonText, buttonType.name === 'apple' && { color: Theme.black }]}>
                                                {buttonType.title}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                </View>
                <TouchableOpacity style={{marginHorizontal: 20, marginVertical: 20, alignItems: 'center'}}
                activeOpacity={1}
                onPress={()=> navigate(ROUTES.LOGIN.routeName)}>
                    <Text
                        style={CompStyles.loginText}
                    >Already have an account? <Text style={{textDecorationLine: 'underline'}}>Login</Text></Text>
                </TouchableOpacity>
                {
                    showAgrermentModal && this._renderAgreementModal()
                }
            </ScrollView>
            </BackgroundImageWrapper>
        )
    }
}

export default AppStarter
{/* <View style={CompStyles.signInComponent}>
    <Text style={CompStyles.accountText} onPress={()=> navigate(ROUTES.LOGIN.routeName)}>Already have an account? <Text style={CompStyles.underLine}>Sign-in</Text></Text>
</View> */}
