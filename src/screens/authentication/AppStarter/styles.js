import { Platform, StyleSheet } from 'react-native'
import { AppStyles, Fonts, Metrics, Theme } from '../../../assets/theme'

export default StyleSheet.create({
    scrollView: {
        flexGrow: 1,
    },
    logoComponent: {
        // flex: 0.6,
        marginTop: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoImage: {
        resizeMode: 'contain',
        width: 150,
        height: 150,
    },
    signupHeading: {
        fontSize: Fonts.size.large,
        fontFamily: Fonts.type.SemiBold
    },
    signupSubHeading: {
        fontFamily: Fonts.type.Regular
    },
    bottomContainer: {
        flex: 1,
        marginHorizontal: 20,
        justifyContent: 'center',
    },
    buttonsComponent: {
        marginTop: 20,
    },
    button: {
        flexDirection: 'row',
        borderRadius: 40,
        marginVertical: 13,
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        height: Platform.select({
            ios: 60,
            android: 65//Metrics.screenHeight > 700 ? 80 : 60
        }),
        shadowColor: Theme.black,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.54,
        shadowRadius: 7.27,
        elevation: 10,
    },
    appleButton: {
        backgroundColor: Theme.white
    },
    googleButton: {
        backgroundColor: Theme.googleColor
    },
    facebookButton: {
        backgroundColor: Theme.facebookColor
    },
    emailButton: {
        backgroundColor: Theme.themeFontColor
    },
    iconStyles: {
        flex: 0.3,
        alignItems: 'flex-end',
        marginRight: 20
    },
    buttonIcon: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
    },
    textWrapper: {
        flex: 1,
    },
    buttonText: {
        color: Theme.white,
        // marginTop: 5,
        paddingTop: 3,
        fontFamily: Fonts.type.Bold,
        letterSpacing: 0.2,
        fontSize: Metrics.normalize(Fonts.size.small)
        // Platform.select({
        //     ios: Fonts.size.normal,
        //     android: Fonts.size.large
        // })
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.SemiBold,
        fontSize: Metrics.normalize(Fonts.size.small)
    },
    signInComponent: {
        paddingVertical: 20,
        alignItems: 'center'
    },
    accountText: {
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.xSmall,
    },
    underLine: {
        textDecorationLine: 'underline'
    },
    modalContainer: {
        borderRadius: 10,
        backgroundColor: Theme.white,
        overflow: 'hidden'
    },
    modalAgreementHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15,
        backgroundColor: Theme.themeFontColor
    },
    agreementHeading: {
        color: Theme.white,
        fontSize: Fonts.size.large,
        fontFamily: Fonts.type.SemiBold
    },
    agreementSubHeading: {
        fontSize: Fonts.size.small,
        fontFamily: Fonts.type.Regular
    },
    policyContainer: {
        marginHorizontal: 25,
        marginVertical: 30
    },
    agreementButton: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    tickBox: {
        justifyContent: 'center',
        marginRight: 20
    },
    acceptImage: {
        alignSelf: 'center',
        width: 20,
        height: 20,
        resizeMode: 'contain',
        borderRadius: 4
        // tintColor: Colors.black
    },
    termsText: {
        fontSize: Fonts.size.small,
        fontFamily: Fonts.type.Regular
    },
    buttonContainer: {
        marginTop: 10,
        marginBottom: 30,
        marginHorizontal: 25,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    modalButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 55,
    },
    acceptButton: {
        backgroundColor: Theme.themeFontColor,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30
    },
    declineButton: {
        backgroundColor: Theme.secondary,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30
    },
    modalButtonText: {
        fontSize: Fonts.size.small,
        fontFamily: Fonts.type.Medium,
        color: Theme.white
    },
    textBlack: {
        color: Theme.black
    },
    modalErrorMessages: {
        marginVertical: 5,
        alignSelf: 'center',
        color: Theme.error
    }
});