import { StyleSheet } from "react-native";
import { Fonts, Theme } from "../../../assets/theme";

export default StyleSheet.create({
    formContainer: {
        // flex: 1,
        marginTop: 40,
        marginBottom: 20,
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 15,
        marginHorizontal: 20,
        borderBottomColor: Theme.white,
        paddingHorizontal: 10,
        borderBottomWidth: 1
    },
    headerLabel: {
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.xxSmall,
        color: Theme.rgbaWhite
    },
    iconWrapper: {
        justifyContent: 'center',
    },
    eyeIcon: {
        width: 15,
        height: 15,
        resizeMode: 'contain',
        tintColor: Theme.white
    },
    input: {
        height: 40,
        paddingRight: 15,
        color: Theme.white,
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.small
    },
    buttonContainer: {
        overflow: 'hidden',
        alignItems: 'center'
    },
    button: {
        backgroundColor: Theme.themeFontColor,
        height: 50,
        width: 130,
        borderRadius: 50
    },
    updateText: {
        color: Theme.white,
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.type.Regular,
    },
    flex: {
        flex: 1
    }
})