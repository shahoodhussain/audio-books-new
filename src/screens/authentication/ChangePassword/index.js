import React, { PureComponent } from 'react'
import { View, Text, Image, ScrollView, TextInput, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { Images, Theme } from "../../../assets/theme";
import { ButtonView } from "../../../components";
import { changePasswordRequest } from "../../../redux/actions/UserActions";
import Util from "../../../util";
import CompStyles from "./styles";
import _ from 'lodash';

const PASSWORD_TYPE = {
    CURRENT_PASSWORD: 0,
    NEW_PASSWORD: 1,
    CONFIRM_PASSWORD: 2,
}

const ERROR_MSG = {
    PASSWORD_FORMAT: "Invalid password format. Example: Qwerty@123",
    CONFIRM_PASSWORD: "Password mismatch",
    EMPTY_FEILD: "This field can not be empty"
}

Object.freeze(PASSWORD_TYPE);

class ChangePassword extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            showExisitingPass: false,
            showNewPass: false,
            showConfirmPass: false,
            currentPass: null,
            newPass: null,
            confirmPass: null,
            formError: {
                cpError: true,
                npError: true,
                cnpError: true
            }
        }
        this.currentPassword = this.currentPassword.bind(this)
        this.newPassword = this.newPassword.bind(this)
        this.updatePassword = this.updatePassword.bind(this)
    }

    currentPassword(pass) {
        this.updatePasswordState('currentPass', pass)
    }

    newPassword(pass) {
        this.updatePasswordState('newPass', pass)
    }

    updatePassword(pass) {
        this.updatePasswordState('confirmPass', pass)
    }

    updatePasswordState(key, pass) {
        this.setState({ [key]: pass.replace(/\s/g, '') })
    }

    validatePassword(type) {
        const passwordFormatRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,20}$/;
        const {currentPass, newPass, confirmPass, formError} = this.state;
        switch (type) {
            case PASSWORD_TYPE.CURRENT_PASSWORD:
                if( currentPass && currentPass.length > 0 ) {
                    if( !passwordFormatRegex.test(currentPass) ) {
                        formError.cpError = true
                    } else formError.cpError = false
                }
                break;
            case PASSWORD_TYPE.NEW_PASSWORD:
                if( newPass && newPass.length > 0) {
                    if( !passwordFormatRegex.test(newPass) ) {
                        formError.npError = true
                    } else formError.npError = false
                }
                break;
            case PASSWORD_TYPE.CONFIRM_PASSWORD:
                if( newPass !== confirmPass ) {
                    formError.cnpError = true
                } else formError.cnpError = false
                break;
        }
        this.forceUpdate();
    }

    changePassword() {
        const {currentPass, newPass, confirmPass} = this.state;
        const payload = {
            old_password: currentPass,
            new_password: newPass
        };
        if( _.isEmpty(currentPass) ) {
            Util.DialogAlert(ERROR_MSG.EMPTY_FEILD, "Current Password")
        } else if( !Util.isValidPasswordFormat(currentPass) ) {
            Util.DialogAlert(ERROR_MSG.PASSWORD_FORMAT)
        } else if( _.isEmpty(newPass) ) {
            Util.DialogAlert(ERROR_MSG.EMPTY_FEILD, "New Password")
        } else if( !Util.isValidPasswordFormat(newPass) ) {
            Util.DialogAlert(ERROR_MSG.PASSWORD_FORMAT, "New Password")
        } else if( newPass !== confirmPass ) {
            Util.DialogAlert(ERROR_MSG.CONFIRM_PASSWORD)
        } else {
            this.props.changePasswordRequest(payload);
        }
    }

    _renderEyeIcon(fieldName) {
        return(
            <TouchableOpacity onPress={()=> this.setState({ [fieldName]: !this.state[fieldName] })} style={CompStyles.iconWrapper}>
                <Image source={this.state[fieldName] ? Images.openEye : Images.closeEye} style={CompStyles.eyeIcon}/>
            </TouchableOpacity>
        )
    }

    render() {
        const {showExisitingPass, showNewPass, showConfirmPass, currentPass, newPass, confirmPass } = this.state;
        return (
            <ScrollView 
            keyboardShouldPersistTaps="handled">
                <View style={CompStyles.formContainer}>
                    <View style={CompStyles.inputContainer}>
                        <View style={CompStyles.flex}>
                            <Text style={CompStyles.headerLabel}>Exisiting Password</Text>
                            <TextInput
                                maxLength={30}
                                placeholderTextColor={Theme.white}
                                placeholder="Old Password"
                                secureTextEntry={showExisitingPass ? false : true}
                                style={CompStyles.input}
                                keyboardType="default"
                                returnKeyType="next"
                                onSubmitEditing={()=> this.newPassRef.focus()}
                                // onBlur={()=> this.validatePassword(PASSWORD_TYPE.CURRENT_PASSWORD)}
                                onChangeText={this.currentPassword}
                                value={currentPass}
                                textContentType="password"
                            />
                        </View>
                        {this._renderEyeIcon('showExisitingPass')}
                    </View>
                    <View style={CompStyles.inputContainer}>
                        <View style={CompStyles.flex}>
                            <Text style={CompStyles.headerLabel}>New Password</Text>
                            <TextInput
                                maxLength={30}
                                placeholderTextColor={Theme.white}
                                placeholder="New Password"
                                secureTextEntry={showNewPass ? false : true}
                                style={CompStyles.input}
                                keyboardType="default"
                                returnKeyType="next"
                                ref={(ref)=> this.newPassRef = ref}
                                onSubmitEditing={()=> this.confirmPass.focus()}
                                // onBlur={()=> this.validatePassword(PASSWORD_TYPE.NEW_PASSWORD)}
                                onChangeText={this.newPassword}
                                value={newPass}
                                textContentType="newPassword"
                            />
                        </View>
                        {this._renderEyeIcon('showNewPass')}
                    </View>
                    <View style={CompStyles.inputContainer}>
                        <View style={CompStyles.flex}>
                            <Text style={CompStyles.headerLabel}>Confirm Password</Text>
                            <TextInput
                                maxLength={30}
                                placeholderTextColor={Theme.white}
                                placeholder="Confirm Password"
                                secureTextEntry={showConfirmPass ? false : true}
                                style={CompStyles.input}
                                keyboardType="default"
                                returnKeyType="done"
                                ref={(ref)=> this.confirmPass = ref}
                                // onBlur={()=> this.validatePassword(PASSWORD_TYPE.CONFIRM_PASSWORD)}
                                onChangeText={this.updatePassword}
                                value={confirmPass}
                                textContentType="newPassword"
                            />
                        </View>
                        {this._renderEyeIcon('showConfirmPass')}
                    </View>
                </View>
                <View style={CompStyles.buttonContainer}>
                    <ButtonView
                        shouldAnimate
                        disabled={this.props.loading}
                        style={CompStyles.button}
                        onPress={()=> this.changePassword()}>
                            <Text style={CompStyles.updateText}>Update</Text>
                    </ButtonView>
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = ({ user }) => ({
    ...user
});

const actions = {changePasswordRequest};

export default connect(mapStateToProps, actions)(ChangePassword);