import { StyleSheet } from "react-native"
import { AppStyles, Fonts, Metrics, Theme } from "../../../assets/theme"

export default StyleSheet.create({
    logoComponent: {
        // height: '42%',
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50
    },
    inactiveTextInput: {
        borderWidth: 2,
        borderColor: Theme.white,
        height: 55,
        justifyContent: 'center',
        alignItems: 'center',
        color: Theme.white,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Medium,
        borderRadius: 10,
    },
    activeTextInput: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Medium,
        color: Theme.white,
    },
    buttonContainer: {
        // alignItems: 'center',
        alignSelf: 'center',
        width: '40%',
        marginVertical: 20,
        marginHorizontal: 30,
    },
    button: {
        overflow: 'hidden',
        backgroundColor: Theme.themeFontColor,
        paddingVertical: 13,
        borderRadius: 30,
        ...AppStyles.shadow7
    },
    verifyText: {
        color: Theme.white,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Regular
    },
    container: {
        flexGrow: 1
    },
    textContainer: {
        marginTop: 20,
        marginHorizontal: 30,
        alignItems: 'center',
    },
    verificationText: {
        textAlign: 'center',
        color: Theme.white,
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.type.Regular
    },
    verificationCode: {
        color: Theme.themeFontColor
    },
    otpContainer: {
        marginVertical: 20,
    },
    otpView: {
        flex: 1,
        width: '80%',
        alignSelf: 'center'
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.CiceroMedium,
        fontSize: Metrics.normalize(Fonts.size.medium),
        marginBottom: 30
    }
})
