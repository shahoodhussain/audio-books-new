import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ActivityIndicator, StyleSheet, Animated, LayoutAnimation } from 'react-native'
import { Fonts, Theme } from '../../../assets/theme';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';

class ResendVerificationCode extends Component {

    state = {
        countdownTimer: this.props.startOnMount ? 59 : 0,
        animatedHeight: new Animated.Value(100),
        showResend: false
    }

    viewHeight = 0;

    componentDidMount() {
        // if( this.props.startOnMount ) {
        //     this.clockCall = setInterval(() => {
        //         this.decrementClock();
        //     }, 1000);
        // };
    }

    animateTimerContainer() {
        Animated.timing(                    // Animate over time
            this.state.animatedHeight,             // The animated value to drive, this would be a new Animated.Value(0) object.
            {
              toValue: this.viewHeight - 1.55,                   // Animate the value
              duration: 500,                 // Make it take a while
              useNativeDriver: false,
            }
        ).start();
    }

    componentWillUnmount() {
        clearInterval(this.clockCall);
    }

    decrementClock = () => {
        if(this.state.countdownTimer === 0) {
            clearInterval(this.clockCall);
            // LayoutAnimation.create(2000, LayoutAnimation.spring, LayoutAnimation.configChecker())
            LayoutAnimation.easeInEaseOut();
            this.setState({ showResend: true })
            return;
        }
        this.setState((prevstate) => ({ countdownTimer: prevstate.countdownTimer - 1 }));
        this.animateTimerContainer();
    };

    startTimer = () => {
        LayoutAnimation.easeInEaseOut();
        this.setState({ countdownTimer : 59, showResend: false })
        this.clockCall = setInterval(() => {
            this.decrementClock();
        }, 1000);
        this.props.resendVerificationCodeRequest()
    }

    render() {
        return (
            <View style={CompStyles.container}>
                {
                    this.state.showResend ? (
                        <TouchableOpacity 
                        // disabled={this.state.countdownTimer != 0}
                        disabled={!this.state.showResend}
                        onPress={this.startTimer}>
                            {
                                this.props.userData.loading && !this.props.userData.shouldAnimate ? <ActivityIndicator size="small" color={Theme.primaryDark}/> : <Text style={CompStyles.resendText}>Resend Code</Text>
                            }
                        </TouchableOpacity>
                    ) : (
                        <CountdownCircleTimer
                            key={1}
                            isPlaying
                            rotation={'clockwise'}
                            duration={this.state.countdownTimer}
                            colors={[
                                [Theme.white, 1],
                            ]}
                            trailColor={Theme.transparent}
                            size = {110}
                            strokeWidth={4}
                            onComplete={()=>this.setState({ showResend: true })}>
                            {({ remainingTime, animatedColor }) => (
                                <View style={{height:'85%', borderColor: Theme.white, borderWidth: 3, justifyContent:'center',alignItems:'center',width:'85%',borderRadius:100}}>
                                    <Animated.Text style={{color: Theme.white, fontFamily: Fonts.type.SemiBold}}>
                                        00:{remainingTime}
                                    </Animated.Text>
                                </View>
                            )}
                        </CountdownCircleTimer>
                    )
                }
            </View>
        )

        return (
            <View style={CompStyles.container}>
                {
                    this.state.showResend ? (
                        <TouchableOpacity disabled={this.state.countdownTimer != 0}
                        onPress={this.startTimer}>
                            {
                                this.props.userData.loading && !this.props.userData.shouldAnimate ? <ActivityIndicator size="small" color={Theme.primaryDark}/> : <Text style={CompStyles.resendText}>Resend Code</Text>
                            }
                        </TouchableOpacity>
                    ) : (
                        <View style={CompStyles.timerView}>
                            <Text style={CompStyles.timerText}>00 : {this.state.countdownTimer < 10 ? '0'+this.state.countdownTimer : this.state.countdownTimer}</Text>
                            <Animated.View style={{backgroundColor: 'pink', opacity: 0.5, flex: 1, position: 'absolute', bottom: 0, width: '100%',
                                height: this.state.animatedHeight }} 
                                onLayout={(event) => {
                                    var {x, y, width, height} = event.nativeEvent.layout;
                                    this.viewHeight = height;
                                }}
                            />
                        </View>
                    )
                }
            </View>
        )
    }
}

const CompStyles = StyleSheet.create({
    container: {
        marginVertical: 20,
        alignItems: 'center'
    },
    timerView: {
        overflow: 'hidden',
        marginTop: 40,
        marginBottom: 10,
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 55,
        borderWidth: 2,
        borderColor: Theme.white
    },
    timerText: {
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.Medium,
        color: Theme.primaryBackground
    },
    resendText: {
        color: Theme.white,
        fontSize: Fonts.size.xSmall,
        fontFamily: Fonts.type.Regular,
        textDecorationLine: 'underline'
    }
})

export default ResendVerificationCode
