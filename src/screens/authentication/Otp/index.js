import React, { Component } from 'react';
import { View, Text, ScrollView, KeyboardAvoidingView } from 'react-native';
import { verifyUserRequest, showLoaderRequest, resendVerificationCodeRequest, clearTempUserData } from "../../../redux/actions/UserActions";
import Util from "../../../util";
import _ from "lodash";
import { connect } from "react-redux";
import { ButtonView, AppLogo, BackgroundImageWrapper } from "../../../components";
import OTPInputView from '@twotalltotems/react-native-otp-input';
import CompStyles from "./styles";
import ResendVerificationCode from "./resend_code_counter";
import { Theme } from '../../../assets/theme';

class OTPComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: "",
        };
    }

    verifyOtp() {
        if( _.isEmpty(this.state.code) ) {
            Util.DialogAlert('Enter OTP code');
        } else if( this.state.code.length != 6 ) {
            Util.DialogAlert('Invalid Code');
        } else {
            const params = this.props.route.params;
            const payload = {
                otp: this.state.code,
                reference_code: params?.otp_refence,
                type: params?.resetPassword ? 'PASSWORD_RESET' : 'ACCOUNT_VERIFICATION',
                email: params?.email,
            }
            const isResetPassword = params?.resetPassword//this.props.userData && this.props.userData.isForgotPassword;
            this.props.verifyUserRequest(payload, isResetPassword);
        }
    }

    render() {
        const { userData, resendVerificationCodeRequest, route } = this.props;
        return (
            <BackgroundImageWrapper>
                <KeyboardAvoidingView
                behavior={'padding'}
                contentContainerStyle={CompStyles.container}
                style={{flex: 1}}
                keyboardShouldPersistTaps="handled"
                enableOnAndroid={true}>
                    <ScrollView contentContainerStyle={CompStyles.container}>
                        <View style={CompStyles.logoComponent}>
                            <Text style={CompStyles.loginText}>ACCOUNT VERIFICATION</Text>
                            <AppLogo />
                        </View>
                        <View style={CompStyles.textContainer}>
                            <Text style={CompStyles.verificationText}>We have sent you an email containing <Text style={CompStyles.verificationCode}>VERIFICATION CODE</Text> and instructions. Please follow the instructions to verify your email address.</Text>
                        </View>
                        <View style={CompStyles.otpContainer}>
                            <OTPInputView
                                style={CompStyles.otpView}
                                pinCount={6}
                                keyboardType="number-pad"
                                selectionColor="#000"
                                code={this.state.code}
                                onCodeChanged = {code => this.setState({code})}
                                autoFocusOnLoad={false}
                                codeInputFieldStyle={CompStyles.inactiveTextInput}
                                codeInputHighlightStyle={CompStyles.activeTextInput}
                                selectionColor={Theme.white}
                                // onCodeFilled = {(code => {})}
                            />
                            <ResendVerificationCode 
                                resendVerificationCodeRequest={()=>resendVerificationCodeRequest(route.params?.email)}
                                userData={userData}
                                // startOnMount={userData.isForgotPassword ? true : this.props.route && this.props.route.params && this.props.route.params.startOnMount}
                                startOnMount={true}
                            />
                            <View style={CompStyles.buttonContainer}>
                                <ButtonView
                                shouldAnimate
                                disabled={userData.loading}
                                style={CompStyles.button}
                                onPress={()=> this.verifyOtp()}>
                                    <Text style={CompStyles.verifyText}>Verify</Text>
                                </ButtonView>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </BackgroundImageWrapper>
        );
    }
}

const mapStateToProps = ({ user }) => ({
    userData: user
});
const actions = { verifyUserRequest, showLoaderRequest, resendVerificationCodeRequest, clearTempUserData };

export default connect(
    mapStateToProps,
    actions
)(OTPComponent);
