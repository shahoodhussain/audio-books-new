import React, { Component } from 'react'
import { Image, KeyboardAvoidingView, ScrollView, Text, TouchableOpacity, View, PermissionsAndroid } from 'react-native'
import { Images } from '../../../assets/theme';
import { BackgroundImageWrapper, ButtonView } from '../../../components';
import { Fumi } from 'react-native-textinput-effects';
import CompStyles from './styles';
import MediaPicker from "../../../services/MediaPicker";
import { connect } from "react-redux";
import { completeProfileRequest } from "../../../redux/actions/UserActions";
import { generateFormData } from '../../../services/classesHelper'
import _ from 'lodash'
import util from '../../../util';

const INPUT_TYPES = [
    {
        name: 'name',
        iconBackgroundColor: '#161415',
        textBackgroundColor: '#231f20',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Full Name',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: false,
        buttonIcon: Images.User,
        eyeIcon: false,
        reference: null,
        keyboardType: 'default'
    },
    {
        name: 'number',
        iconBackgroundColor: '#314c8c',
        textBackgroundColor: '#3e5896',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Phone Number',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: true,
        buttonIcon: Images.Phone,
        eyeIcon: true,
        reference: null,
        keyboardType: 'phone-pad'
    },
    {
        name: 'address',
        iconBackgroundColor: '#dc1c2f',
        textBackgroundColor: '#f3343e',
        lableStyle: CompStyles.inputLabelStyle,
        inputLabel: 'Street Address',
        inputStyle: CompStyles.inputStyle,
        secureTextEntry: true,
        buttonIcon: Images.Email,
        eyeIcon: true,
        reference: null,
        keyboardType: 'default'
    },
];

export class ProfileCreation extends Component {

    state = {
        imageUrl: null,
        name: '',
        number: '',
        address: ''
    }

    async selectImage() {
        try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                title: "Cool Photo App Camera Permission",
                message:
                  "Cool Photo App needs access to your camera " +
                  "so you can take awesome pictures.",
                buttonNeutral: "Ask Me Later",
                buttonNegative: "Cancel",
                buttonPositive: "OK"
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                MediaPicker.openShowImagePicker((imageUrl, response)=> {
                    if(imageUrl) {
                        this.setState({ imageUrl: imageUrl })
                    }
                })
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }

    onChangeText(text, type) {
        this.setState({ [type]: text })
    }

    profileCompleted() {
        const { name, number, address, imageUrl } = this.state;
        if( _.isEmpty(name) ) {
            util.DialogAlert('Enter your full name', 'Error', 'error')
        } else if( !util.isNumber(number) ) {
            util.DialogAlert('Enter your phone number', 'Error', 'error')
        } else if( _.isEmpty(address) ) {
            util.DialogAlert('Enter your address', 'Error', 'error')
        } else if( _.isEmpty(imageUrl) ) {
            util.DialogAlert('Please select your image', 'Error', 'error')
        } else {
            const payload = {
                name,
                phone_no: number,
                address,
                image: imageUrl,
            }
            this.props.completeProfileRequest(generateFormData(payload), this.props.userData.tempUserData?.bearer_token);
        }
    }

    render() {
        const { imageUrl } = this.state;
        return (
            <BackgroundImageWrapper>
                <KeyboardAvoidingView
                behavior='padding'
                contentContainerStyle={CompStyles.container}
                style={{flex: 1}}
                keyboardShouldPersistTaps="handled"
                enableOnAndroid={true}>
                    <ScrollView
                    contentContainerStyle={{flexGrow: 1}}>
                        <View style={CompStyles.logoComponent}>
                            <Text style={CompStyles.loginText}>PROFILE</Text>
                            <View style={CompStyles.cameraBorder}>
                                <TouchableOpacity style={CompStyles.imageContainer}
                                activeOpacity={1}
                                onPress={()=> this.selectImage()}>
                                    <Image 
                                    style={imageUrl ? CompStyles.avatar : CompStyles.cameraImage}
                                    source={imageUrl ? {uri: imageUrl} : Images.Camera}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={CompStyles.bottomContainer}>
                            {
                                INPUT_TYPES.map((inputType, index) => {
                                    return(
                                        <View
                                            style={CompStyles.button}
                                            key={index}>
                                            <View style={CompStyles.textWrapper}>
                                                <Fumi
                                                    iconName={'university'}
                                                    iconColor={'transparent'}
                                                    passiveIconColor={'transparent'}
                                                    iconSize={0}
                                                    iconWidth={0}
                                                    inputPadding={16}
                                                    selectionColor='#fff'
                                                    keyboardType={inputType.keyboardType}
                                                    ref={ref => inputType.reference = ref}
                                                    onSubmitEditing={()=> inputType.name !== 'address' && INPUT_TYPES[index+1].reference.focus()}
                                                    returnKeyType={inputType.name !== 'address' ? 'next' : 'done'}
                                                    label={inputType.inputLabel}
                                                    labelStyle={inputType.lableStyle}
                                                    inputStyle={inputType.inputStyle}
                                                    style={{
                                                        backgroundColor: 'transparent',
                                                    }}
                                                    onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                                />
                                            </View>
                                        </View>
                                    )
                                })
                            }
                            <View style={CompStyles.signupContainer}>
                                <ButtonView
                                shouldAnimate
                                disabled={this.props.userData.loading}
                                style={CompStyles.signupButton}
                                onPress={()=> this.profileCompleted()}>
                                    <Text style={CompStyles.signupText}>Done</Text>
                                </ButtonView>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </BackgroundImageWrapper>
        )
    }
}

const mapStateToProps = ({ user }) => ({
    userData: user
});

const actions = { completeProfileRequest };

export default connect(
    mapStateToProps,
    actions
)(ProfileCreation);
