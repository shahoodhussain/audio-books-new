import { StyleSheet } from "react-native";
import { AppStyles, Fonts, Metrics, Theme } from "../../../assets/theme";

export default StyleSheet.create({
    logoComponent: {
        height: '42%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    resetContainer: {
        height: 55,
        marginHorizontal: 20,
        marginVertical: 20,
        alignItems: 'center'
    },
    signupButton: {
        // marginTop: 25,
        backgroundColor: Theme.themeFontColor,
        borderRadius: 30,
        width: '35%',
        overflow: 'hidden',
        ...AppStyles.shadow7
    },
    signupText: {
        color: Theme.white,
        fontSize: Fonts.size.normal,
        fontFamily: Fonts.type.Regular
    },
    textWrapper: {
        flex: 1,
    },
    inputStyle: {
        fontSize: Fonts.size.small,
        fontFamily: Fonts.type.Regular,
        fontWeight: '500',
        color: Theme.white,
        paddingRight: 20,
    },
    InputWrapper: {
        marginHorizontal: 20,
        flexDirection: 'row',
        // borderRadius: 30,
        height: 60,
        overflow: 'hidden',
        borderBottomWidth: 2,
        borderColor: Theme.white,
        alignItems: 'center',
    },
    iconStyles: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonIcon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginHorizontal: 20,
    },
    loginText: {
        color: Theme.white,
        fontFamily: Fonts.type.CiceroMedium,
        fontSize: Metrics.normalize(Fonts.size.medium),
        marginBottom: 30
    }
})