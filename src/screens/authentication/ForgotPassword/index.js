import React, { Component } from "react";
import { View, Text, TextInput, Image } from 'react-native';
import _ from "lodash";
import { forgotPasswordRequest } from "../../../redux/actions/UserActions";
import { connect } from "react-redux";
import Util from "../../../util";
import { Images, Theme} from "../../../assets/theme";
import {ButtonView, AppLogo, BackgroundImageWrapper} from "../../../components";
import CompStyles from "./styles";

class ForgetPassword extends Component {
	constructor(props) {
		super(props);
		this.state = {
            email: ''
		};
	}

    resetPassword() {
        const {email} = this.state;
        if( _.isEmpty(email) ) {
            Util.DialogAlert('Enter email address', 'Error');
        } else if( !Util.isEmailValid(email) ) {
            Util.DialogAlert('Invalid email', 'Error');
        } else {
            const payload = { 
                email: email,
                type: 'PASSWORD_RESET'
            };
            this.props.forgotPasswordRequest(payload);
        }
    }

	render() {
		return (
            <BackgroundImageWrapper>
                <View style={CompStyles.logoComponent}>
                    <Text style={CompStyles.loginText}>FORGOT PASSWORD</Text>
                    <AppLogo />
                </View>
                <View>
                    <View
                        style={CompStyles.InputWrapper}>
                        <View style={CompStyles.iconStyles}>
                            <Image 
                                source={Images.Email}
                                style={CompStyles.buttonIcon}
                            />
                        </View>
                        <View style={CompStyles.textWrapper}>
                            <TextInput 
                                ref={ref => this.inputRef = ref}
                                returnKeyType={'done'}
                                placeholder='Enter email address'
                                placeholderTextColor={Theme.white}
                                selectionColor='#fff'
                                style={CompStyles.inputStyle}
                                keyboardType="email-address"
                                onChangeText={(text) => this.setState({ email: text })}
                            />
                        </View>
                    </View>
                    <View style={CompStyles.resetContainer}>
                        <ButtonView
                        shouldAnimate
                        disabled={this.props.userData.loading}
                        style={CompStyles.signupButton}
                        onPress={()=> this.resetPassword()}>
                            <Text style={CompStyles.signupText}>Submit</Text>
                        </ButtonView>
                    </View>
                </View>
            </BackgroundImageWrapper>
		);
	}
}

const mapStateToProps = ({user}) => ({
    userData: user
})

const actions = { forgotPasswordRequest };

export default connect(
  mapStateToProps,
  actions
)(ForgetPassword);