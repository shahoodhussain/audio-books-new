import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Keyboard, ActivityIndicator } from 'react-native'
import { Fonts, Theme } from '../../assets/theme';
import { useDispatch } from "react-redux";
import { addCard } from "../../redux/actions/card.actions";
import { goBack } from '../../services/NavigationService';
import { CardField, createToken } from '@stripe/stripe-react-native';

const AddCard = () => {

    const [loading, setLoading] = useState(false);
    const [cardData, setCardData] = useState(null)

    const dispatch = useDispatch();

    const handlePayment = async () => {
        if ( cardData?.complete ) {
            Keyboard.dismiss();
            setLoading(true)
            const token = await createToken(cardData)//stripe.createTokenWithCard(params)
            dispatch(
                addCard(token?.token?.id, responseData=> {
                    setLoading(false)
                    goBack();
                })
            )
        }
    }

    return (
        <>
            <CardField
                postalCodeEnabled={false}
                placeholder={{
                    number: '4242 4242 4242 4242',
                }}
                cardStyle={{
                    backgroundColor: '#FFFFFF',
                    textColor: '#000000',
                    placeholderColor: Theme.lightTextColor
                }}
                style={{
                    height: 70,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginVertical: 30,
                    marginHorizontal: 5
                }}
                onCardChange={(cardDetails) => {
                    console.log('cardDetails-----------', cardDetails);
                    if(cardDetails?.complete) Keyboard.dismiss()
                    setCardData(cardDetails)
                }}
                // onFocus={(focusedField) => {
                //     console.log('focusField', focusedField);
                // }}
            />
            <View style={{position: 'absolute', bottom: 0, left: 0, right: 0, marginVertical: 20, marginHorizontal: 20}}>
                {
                    cardData?.complete && (
                        <TouchableOpacity
                        disabled={loading}
                        onPress={handlePayment}
                        style={{
                            height: 55,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: Theme.themeFontColor,
                            borderRadius: 30
                        }}>
                            {
                                loading ? <ActivityIndicator size={'small'} color={Theme.white}/> : (
                                    <Text style={{
                                        color: Theme.white,
                                        fontSize: Fonts.size.normal,
                                        fontFamily: Fonts.type.Regular
                                    }}>DONE</Text>
                                )
                            }
                        </TouchableOpacity>
                    )
                }
            </View>
        </>
    );
}

export default AddCard
