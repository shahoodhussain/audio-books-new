import React, { Component } from 'react'
import { ImageBackground, View, Text, TouchableOpacity, ActivityIndicator, FlatList } from 'react-native'
import CompStyles from './styles'
import { navigate } from '../../services/NavigationService'
import { APP_ROUTES } from '../../router/RoutesConfig'
import { Fonts, Theme } from '../../assets/theme'
import { connect } from "react-redux";
import { fetchCategories } from "../../redux/actions/category.actions";

export class CategoryHome extends Component {

    state = {
        currentOffset: 0,
        allCategories: [],
        loading: false
    }

    componentDidMount() {
        this.getCategories();
    }

    getCategories() {
        const { allCategories, currentOffset } = this.state;
        this.setState({ loading: true })
        this.props.fetchCategories(
            currentOffset,
            (categories, offset) => {
                this.setState({
                    currentOffset: categories.length > 0 ? currentOffset + categories.length + 1 : '',
                    loading: false,
                    allCategories: [...allCategories, ...categories],
                })
            }
        )
    }

    renderCustomItem = ({item: {id, name, thumbnail}}) => {
        return(
            <TouchableOpacity
            activeOpacity={0.8}
            style={CompStyles.card}
            key={id}
            onPress={()=> navigate(APP_ROUTES.CATEGORY_BOOKS.routeName, {title: name})}>
                <ImageBackground
                source={{uri: thumbnail}}
                resizeMode="cover"
                style={CompStyles.backgroundImage}>
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                        padding: 10,
                        backgroundColor: Theme.themeFontColor.replace('1)', '0.2)')
                    }}>
                        <Text
                            style={{
                                color: Theme.white,
                                fontFamily: Fonts.type.CiceroMedium,
                                fontSize: Fonts.size.xLarge
                            }}
                        >{name}</Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        )
    }

    loadMoreCategories = () => {
        setTimeout(() => {
            this.getCategories();
        }, 1000);
    }

    _renderEmptyComponent() {
        return <Text style={{alignSelf: 'center', marginVertical: 50, color: Theme.white, fontSize: Fonts.size.large, fontFamily: Fonts.type.CiceroMedium}}>
                No Categories found
            </Text>
    }

    render() {
        const { allCategories, loading } = this.state;
        if (loading && allCategories.length === 0) return <ActivityIndicator color={Theme.white} size={'large'} style={{flex: 1}}/>
        return (
            <FlatList 
                data={allCategories}
                renderItem={this.renderCustomItem}
                showsVerticalScrollIndicator={false}
                pagingEnabled={true}
                contentContainerStyle={{flexGrow: 1}}
                keyExtractor={({id}) => id.toString()}
                onEndReachedThreshold={0}
                ListEmptyComponent={()=> this._renderEmptyComponent()}
                ListFooterComponent={()=> loading && <ActivityIndicator color={Theme.white} size={'large'} />}
                ListFooterComponentStyle={{marginBottom: 45}}
                onEndReached={!loading && this.loadMoreCategories}
            />
        )
    }
}

const actions = { fetchCategories };

export default connect(null, actions)(CategoryHome);