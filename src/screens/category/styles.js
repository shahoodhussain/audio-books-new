import { StyleSheet } from "react-native";
import { AppStyles, Theme } from "../../assets/theme";

export default StyleSheet.create({
    container: {
        flexGrow: 1,
        // marginHorizontal: 20,
    },
    card: {
        marginVertical: 10,
        marginHorizontal: 20,
        backgroundColor: Theme.primaryBackground,
        ...AppStyles.shadow5
    },
    backgroundImage: {
        height: 170,
        borderRadius: 10,
        overflow: 'hidden',
    }
})