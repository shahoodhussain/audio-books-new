import React, { Component } from 'react'
import { View, ScrollView, FlatList, Text, RefreshControl } from 'react-native'
import { Fonts, Theme } from '../../assets/theme'
import { BookItem } from '../../components'
import { connect } from "react-redux";
import { fetchHomeBooks } from "../../redux/actions/books.actions";
import { APP_ROUTES } from '../../router/RoutesConfig';
import { navigate } from '../../services/NavigationService';

export class UserHome extends Component {

    state = {
        popularData: [],
        fictionData: [],
        categoriesData: [],
        loadingPopular: true,
        loadingFiction: true,
        loadingCategories: true
    }

    componentDidMount() {
        this.props.fetchHomeBooks();
    }

    _renderListHeader(headerText, routeName) {
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginVertical: 10, alignItems: 'center'}}>
                <Text style={{color: Theme.white, fontSize: Fonts.size.large, fontFamily: Fonts.type.CiceroMedium}}>{headerText}</Text>
                <Text 
                onPress={()=> navigate(routeName, { searchCategory: headerText } )}
                style={{color: Theme.themeFontColor, fontSize: Fonts.size.small, fontFamily: Fonts.type.Regular, textDecorationLine: 'underline'}}>
                    See All
                </Text>
            </View>
        )
    }

    _renderBookItem(item) {
        return(
            <BookItem 
                item={item}
            />
        )
    }

    render() {
        const { loading, suggestedCategories } = this.props;
        return (
            <ScrollView 
            style={{flex: 1}}
            contentContainerStyle={{flexGrow: 1}}
            contentContainerStyle={{paddingBottom: 30}}
            showsVerticalScrollIndicator={false}
            refreshControl={<RefreshControl
                    refreshing={loading}
                    tintColor={Theme.white} colors={[Theme.black]}
                    onRefresh={()=> this.props.fetchHomeBooks()}
                />}
            >
                {this._renderListHeader("Popular", APP_ROUTES.TABBOOKS.routeName)}
                <FlatList
                    horizontal
                    data={suggestedCategories?.popular}
                    keyExtractor={(item)=> item?.id.toString()}
                    renderItem={({item})=> this._renderBookItem(item)}
                    style={{ paddingLeft: 20, marginTop: 10}}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={()=> <View style={{width: 30}}/>}
                    ref={(ref) => this.popularRef = ref}
                    // refreshControl={<RefreshControl refreshing={loadingPopular} tintColor={Theme.white} colors={[Theme.black]}/>}
                />
                {this._renderListHeader("Recommendation", APP_ROUTES.TABBOOKS.routeName)}
                <FlatList
                    horizontal
                    data={suggestedCategories?.recommended}
                    keyExtractor={(item)=> item?.id.toString()}
                    renderItem={({item})=> this._renderBookItem(item)}
                    style={{ paddingLeft: 20}}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={()=> <View style={{width: 30}}/>}
                    // refreshControl={<RefreshControl refreshing={loadingFiction} tintColor={Theme.white} colors={[Theme.black]}/>}
                />
                {this._renderListHeader("Categories", APP_ROUTES.TABCATEGORY.routeName)}
                <FlatList
                    horizontal
                    data={suggestedCategories?.categories}
                    keyExtractor={(item)=> item?.id.toString()}
                    renderItem={({item})=> this._renderBookItem(item)}
                    style={{ paddingLeft: 20, marginTop: 10}}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={()=> <View style={{width: 30}}/>}
                    ref={(ref) => this.popularRef = ref}
                    // refreshControl={<RefreshControl refreshing={loadingPopular} tintColor={Theme.white} colors={[Theme.black]}/>}
                />
            </ScrollView>
        )
    }
}

const mapStateToProps = ({ books }) => ({
    suggestedCategories: books.suggestedCategoryBooks,
    loading: books.fetching
});

const actions = { fetchHomeBooks };

export default connect(mapStateToProps, actions)(UserHome);