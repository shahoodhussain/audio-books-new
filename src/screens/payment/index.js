import React, { Component } from 'react'
import { Text, View, FlatList, RefreshControl, TouchableOpacity, ActivityIndicator, Image } from 'react-native'
import { connect } from "react-redux";
import { Fonts, Images, Theme } from '../../assets/theme';
import { getPaymentCards, deleteCard } from "../../redux/actions/card.actions";
import { navigate } from '../../services/NavigationService';
import { APP_ROUTES } from '../../router/RoutesConfig';
import util from '../../util';

export class CardPayment extends Component {
    state = {
        allCards: [],
        loading: false,
        isCardSelected: false,
        deleteLoading: false
    }

    componentDidMount() {
        this.getCard();
    }
    
    getCard() {
        this.setState({ loading: true })
        this.props.getPaymentCards(responseData=> {
            responseData.length >= 0 ? this.setState({ allCards: responseData, loading: false }) : this.setState({ loading: false })
        });
    }

    deleteCard(cardId) {
        this.setState({ deleteLoading: true })
        this.props.deleteCard(cardId, responseData=> {
            this.getCard();
            this.setState({ deleteLoading: false })
        })
    }

    _renderCard(item) {
        return(
            <View style={{
                backgroundColor: Theme.white,
                flexDirection: 'row',
                marginHorizontal: 20,
                height: 50,
                alignItems: 'center',
                borderRadius: 10
            }}>
                <View style={{
                    marginHorizontal: 10
                }}>
                    <Image 
                        source={item?.brand === 'Visa' ? Images.Visa : Images.MasterCard}
                        style={{
                            width: 50,
                            height: 50,
                            resizeMode: 'contain'
                        }}
                    />
                </View>
                <View style={{
                    flex: 1,
                    marginHorizontal: 10
                }}>
                    <Text style={{
                        color: Theme.themeFontColor,
                        fontFamily: Fonts.type.CiceroMedium,
                        fontSize: Fonts.size.medium
                    }}>{item?.last4}</Text>
                </View>
                <TouchableOpacity
                activeOpacity={0.7}
                onPress={()=> this.deleteCard(item?.id)}
                style={{
                    marginHorizontal: 20
                }}>
                    {
                        this.state.deleteLoading ? <ActivityIndicator size={'large'} color={Theme.black}/> : (
                            <Image 
                                source={Images.Trash}
                                style={{
                                    width: 25,
                                    height: 25,
                                    resizeMode: 'contain',
                                    tintColor: Theme.themeFontColor
                                }}
                            />
                        )
                    }
                </TouchableOpacity>
            </View>
        )
    }

    _renderFooterComponent() {
        return !this.state.loading && (
            <TouchableOpacity
            activeOpacity={0.9}
            onPress={()=> navigate(APP_ROUTES.ADD_CARD.routeName)}
            style={{
                alignSelf: 'center',
                paddingVertical: 10,
                marginVertical: 20,
                paddingHorizontal: 30
            }}>
                <Text style={{
                    color: Theme.white,
                    fontSize: Fonts.size.medium,
                    fontFamily: Fonts.type.CiceroMedium
                }}>+ {this.state.allCards.length > 0 ? 'Add Another Card' : 'Add Card'}</Text>
            </TouchableOpacity>
        )
    }

    _renderPaynowButton() {
        return(
            <TouchableOpacity
            activeOpacity={0.9}
            onPress={()=> util.DialogAlert('Unable to processed at the moment', 'Error')}
            style={{
                alignSelf: 'center',
                paddingVertical: 10,
                paddingHorizontal: 30
            }}>
                <Text style={{
                    color: Theme.white,
                    fontSize: Fonts.size.medium,
                    fontFamily: Fonts.type.CiceroMedium
                }}>Pay Now</Text>
            </TouchableOpacity>
        )
    }

    _renderEmptyComponent() {
        return (
            <View style={{
                alignSelf: 'center',
                marginVertical: 10
            }}>
                <Text style={{
                    color: Theme.white,
                    fontSize: Fonts.size.medium,
                    fontFamily: Fonts.type.CiceroMedium,
                    textAlign: 'center'
                }}>No cards found{'\n'}Add payment card for shooping</Text>
            </View>
        )
    }

    render() {
        const { allCards, loading, isCardSelected } = this.state;
        if(loading) return <ActivityIndicator size={'large'} color={Theme.white}/>
        return (
            <View style={{flex: 1}}>
                <FlatList
                    data={allCards}
                    keyExtractor={(item)=> item?.id.toString()}
                    renderItem={({item})=> this._renderCard(item)}
                    style={{ paddingTop: 20 }}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={()=> this._renderEmptyComponent()}
                    ListFooterComponent={()=> this._renderFooterComponent()}
                    refreshControl={<RefreshControl onRefresh={()=> this.getCard()} refreshing={loading} tintColor={Theme.white} colors={[Theme.black]}/>}
                />
                {isCardSelected && this._renderPaynowButton()}
            </View>
        )
    }
}

const actions = { getPaymentCards, deleteCard };

export default connect(null, actions)(CardPayment);