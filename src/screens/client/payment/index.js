import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { ButtonView } from '../../../components';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Fumi } from 'react-native-textinput-effects';
import { Fonts, Theme, Images } from '../../../assets/theme'
import { goBack } from '../../../services/NavigationService';

export class PaymentCards extends Component {
    state={
        card: 1,
    }
    render() {
        const inputStyle = {
            height: 50,
            fontSize: Fonts.size.medium,
            fontFamily: Fonts.type.Regular,
            fontWeight: '500',
            color: Theme.white,
            paddingHorizontal: 20,
        };
        const lableStyle = {
            color: Theme.rgbaWhite,
            fontSize: Fonts.size.xSmall,
            paddingHorizontal: 10,
        }
        const inputContainer = {
            flexDirection: 'row',
            borderRadius: 40,
            height: 65,
            overflow: 'hidden',
            marginVertical: 10,
            borderWidth: 2,
            borderColor: Theme.white,
            flexDirection: 'row',
            alignItems: 'center'
            // justifyContent: 'center',
            // alignItems: 'center'
        }

        return (
            <View style={{flex: 1, marginTop: 20, paddingBottom: 20}}>
                <TouchableOpacity style={{margin: 20, marginTop: 0, backgroundColor: Theme.black + '90', paddingHorizontal: 20, paddingVertical: 10, alignItems: 'center', borderRadius: 20, flexDirection: 'row'}}
                onPress={()=> this.setState({ card: 1 })}
                activeOpacity={1}>
                    <Image 
                        source={Images.VisaCard}
                        style={{width: 45, height: 45, resizeMode: 'contain', marginRight: 20}}
                    />
                    <Text style={{color: Theme.white, fontFamily: Fonts.type.Regular, fontSize: Fonts.size.normal, flex: 1,}}>**** ***** **** 1234</Text>
                    { this.state.card === 1 && <Image 
                        source={Images.Check}
                        style={{width: 20, height: 20, resizeMode: 'contain'}}
                    />}
                </TouchableOpacity>
                <TouchableOpacity style={{margin: 20, marginTop: 0, backgroundColor: Theme.black + '90', paddingHorizontal: 20, paddingVertical: 10, alignItems: 'center', borderRadius: 20, flexDirection: 'row'}}
                onPress={()=> this.setState({ card: 2 })}
                activeOpacity={1}>
                    <Image 
                        source={Images.MasterCard}
                        style={{width: 45, height: 45, resizeMode: 'contain', marginRight: 20}}
                    />
                    <Text style={{color: Theme.white, fontFamily: Fonts.type.Regular, fontSize: Fonts.size.normal, flex: 1,}}>**** ***** **** 1234</Text>
                    { this.state.card === 2 && <Image 
                        source={Images.Check}
                        style={{width: 20, height: 20, resizeMode: 'contain'}}
                    />}
                </TouchableOpacity>
                <View style={{marginHorizontal: 20, flex: 1,}}>
                    <Text style={{color: Theme.white, fontSize: Fonts.size.medium, fontFamily: Fonts.type.SemiBold}}>Add Card</Text>
                    <View>
                        <View style={inputContainer}>
                            <Image 
                                source={Images.Card}
                                style={{width: 20, height: 20, resizeMode: 'contain', marginLeft: 10}}
                            />
                            <Fumi
                                iconClass={FontAwesome5}
                                iconName={'university'}
                                iconColor={'transparent'}
                                passiveIconColor={'transparent'}
                                iconSize={0}
                                iconWidth={0}
                                inputPadding={16}autoCapitalize="none"
                                label='Card Number'
                                labelStyle={lableStyle}
                                inputStyle={inputStyle}
                                style={{
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    backgroundColor: 'transparent',
                                }}
                                selectionColor='#fff'
                                // onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                // ref={ref => inputType.reference = ref}
                                // onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                                // returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                                // secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                            />
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                        <View style={{...inputContainer, flex: 1,}}>
                            <Image 
                                source={Images.Clander}
                                style={{width: 20, height: 20, resizeMode: 'contain', marginLeft: 10}}
                            />
                            <Fumi
                                iconClass={FontAwesome5}
                                iconName={'university'}
                                iconColor={'transparent'}
                                passiveIconColor={'transparent'}
                                iconSize={0}
                                iconWidth={0}
                                inputPadding={16}autoCapitalize="none"
                                label='Exp. Date'
                                labelStyle={lableStyle}
                                inputStyle={inputStyle}
                                style={{
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    backgroundColor: 'transparent',
                                }}
                                selectionColor='#fff'
                                // onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                // ref={ref => inputType.reference = ref}
                                // onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                                // returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                                // secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                            />
                        </View>
                        <View style={{width: 10}}/>
                        <View style={{...inputContainer, flex: 1,}}>
                            <Image 
                                source={Images.Cvv}
                                style={{width: 20, height: 20, resizeMode: 'contain', marginLeft: 10}}
                            />
                            <Fumi
                                iconClass={FontAwesome5}
                                iconName={'university'}
                                iconColor={'transparent'}
                                passiveIconColor={'transparent'}
                                iconSize={0}
                                iconWidth={0}
                                inputPadding={16}autoCapitalize="none"
                                label='CVV'
                                labelStyle={lableStyle}
                                inputStyle={inputStyle}
                                style={{
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    backgroundColor: 'transparent',
                                }}
                                secureTextEntry={true}
                                selectionColor='#fff'
                                // onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                // ref={ref => inputType.reference = ref}
                                // onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                                // returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                                // secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                            />
                        </View>
                    </View>
                </View>
                <View style={{marginVertical: 20, alignItems: 'center'}}>
                    <View style={{height: 55, width: '90%'}}>
                        <ButtonView
                        // shouldAnimate
                        style={{borderRadius: 30}}
                        onPress={()=> goBack()}>
                            <Text style={{fontFamily: Fonts.type.Regular, fontSize: Fonts.size.small}}>Add</Text>
                        </ButtonView>
                    </View>
                </View>
            </View>
        )
    }
}

export default PaymentCards
