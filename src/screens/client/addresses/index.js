import React, { Component } from 'react'
import { Text, View, Image, ScrollView, KeyboardAvoidingView } from 'react-native'
import { Fonts, Images, Theme } from '../../../assets/theme'
import { Fumi } from 'react-native-textinput-effects';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { ButtonView } from '../../../components';
import { goBack } from '../../../services/NavigationService';

export class Addresses extends Component {
    render() {
        const inputStyle = {
            height: 50,
            fontSize: Fonts.size.medium,
            fontFamily: Fonts.type.Regular,
            fontWeight: '500',
            color: Theme.white,
            paddingHorizontal: 20,
        };
        const lableStyle = {
            color: Theme.rgbaWhite,
            fontSize: Fonts.size.xSmall,
            paddingHorizontal: 10,
        }
        const inputContainer = {
            flexDirection: 'row',
            borderRadius: 40,
            height: 65,
            overflow: 'hidden',
            marginVertical: 10,
            borderWidth: 2,
            borderColor: Theme.white,
            // justifyContent: 'center',
            // alignItems: 'center'
        }
        
        return (
            <KeyboardAvoidingView style={{flex: 1,}}
            contentContainerStyle={{flexGrow: 1, paddingBottom: 20}}
            behavior="position">
                <View style={{margin: 20, backgroundColor: Theme.black + '90', padding: 20, borderRadius: 20}}>
                    <Text style={{color: Theme.primaryBackground, fontFamily: Fonts.type.Regular }}>Address</Text>
                    <Text style={{color: Theme.white, fontFamily: Fonts.type.Regular, fontSize: Fonts.size.normal}}>141 Washington Ave Extension,{'\n'}Albany, NY. 12205</Text>
                    <View style={{position: 'absolute', right: 20, top: 20}}>
                        <Image 
                            source={Images.Edit}
                            style={{width: 20, height: 20, resizeMode: 'contain'}}
                        />
                    </View>
                </View>
                <View style={{marginHorizontal: 20, flex: 1,}}>
                    <Text style={{color: Theme.white, fontSize: Fonts.size.medium, fontFamily: Fonts.type.SemiBold}}>Add Address</Text>
                    <View style={inputContainer}>
                        <Fumi
                            iconClass={FontAwesome5}
                            iconName={'university'}
                            iconColor={'transparent'}
                            passiveIconColor={'transparent'}
                            iconSize={0}
                            iconWidth={0}
                            inputPadding={16}autoCapitalize="none"
                            label='House No'
                            labelStyle={lableStyle}
                            inputStyle={inputStyle}
                            style={{
                                flex: 1,
                                paddingHorizontal: 10,
                                backgroundColor: 'transparent',
                            }}
                            selectionColor='#fff'
                            // onChangeText={(text) => this.onChangeText(text, inputType.name)}
                            // ref={ref => inputType.reference = ref}
                            // onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                            // returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                            // secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                        />
                    </View>
                    <View style={inputContainer}>
                        <Fumi
                            iconClass={FontAwesome5}
                            iconName={'university'}
                            iconColor={'transparent'}
                            passiveIconColor={'transparent'}
                            iconSize={0}
                            iconWidth={0}
                            inputPadding={16}autoCapitalize="none"
                            label='Street Name'
                            labelStyle={lableStyle}
                            inputStyle={inputStyle}
                            style={{
                                flex: 1,
                                paddingHorizontal: 10,
                                backgroundColor: 'transparent',
                            }}
                            selectionColor='#fff'
                            // onChangeText={(text) => this.onChangeText(text, inputType.name)}
                            // ref={ref => inputType.reference = ref}
                            // onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                            // returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                            // secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                        />
                    </View>
                    <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                        <View style={{...inputContainer, flex: 1,}}>
                            <Fumi
                                iconClass={FontAwesome5}
                                iconName={'university'}
                                iconColor={'transparent'}
                                passiveIconColor={'transparent'}
                                iconSize={0}
                                iconWidth={0}
                                inputPadding={16}autoCapitalize="none"
                                label='Town'
                                labelStyle={lableStyle}
                                inputStyle={inputStyle}
                                style={{
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    backgroundColor: 'transparent',
                                }}
                                selectionColor='#fff'
                                // onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                // ref={ref => inputType.reference = ref}
                                // onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                                // returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                                // secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                            />
                        </View>
                        <View style={{width: 10}}/>
                        <View style={{...inputContainer, flex: 1,}}>
                            <Fumi
                                iconClass={FontAwesome5}
                                iconName={'university'}
                                iconColor={'transparent'}
                                passiveIconColor={'transparent'}
                                iconSize={0}
                                iconWidth={0}
                                inputPadding={16}autoCapitalize="none"
                                label='City'
                                labelStyle={lableStyle}
                                inputStyle={inputStyle}
                                style={{
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    backgroundColor: 'transparent',
                                }}
                                selectionColor='#fff'
                                // onChangeText={(text) => this.onChangeText(text, inputType.name)}
                                // ref={ref => inputType.reference = ref}
                                // onSubmitEditing={()=> inputType.name !== 'password' && INPUT_TYPES[index+1].reference.focus()}
                                // returnKeyType={inputType.name !== 'password' ? 'next' : 'done'}
                                // secureTextEntry={inputType.secureTextEntry !== false ? (inputType.secureTextEntry && inputType.name === 'password') ? !showPassword : !showConfirmPassword : false}
                            />
                        </View>
                    </View>
                </View>
                <View style={{marginVertical: 20, alignItems: 'center'}}>
                    <View style={{height: 55, width: '90%'}}>
                        <ButtonView
                        // shouldAnimate
                        style={{borderRadius: 30}}
                        onPress={()=> goBack()}>
                            <Text style={{fontFamily: Fonts.type.Regular, fontSize: Fonts.size.small}}>Apply</Text>
                        </ButtonView>
                    </View>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Addresses
