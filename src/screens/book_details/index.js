import React, { Component } from 'react'
import { FlatList, Image, LayoutAnimation, Platform, Text, UIManager, TouchableOpacity, View, ActivityIndicator } from 'react-native'
import { Fonts, Images, Theme } from '../../assets/theme'
import { navigate } from '../../services/NavigationService';
import { APP_ROUTES } from '../../router/RoutesConfig';
import { connect } from "react-redux";
import { addBookToCart } from "../../redux/actions/cart.actions";
import { saveDownloadedChapter } from "../../redux/actions/track.actions";
import { DemoPlayer } from '../../components';
import util from '../../util';
import RNFetchBlob from 'rn-fetch-blob'
import TrackPlayer, { State } from 'react-native-track-player';

if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}

export class BookDetails extends Component {

    state = {
        addCartState: false,
        showDemoModal: false,
        loadingId: null
    }

    addToCart() {
        const item = this.props.route.params?.item;
        LayoutAnimation.spring()
        this.setState({ addCartState: !this.state.addCartState })
        setTimeout(() => {
            this.setState({ addCartState: !this.state.addCartState })
            LayoutAnimation.spring()
        }, 1500);
        this.props.addBookToCart(item)
        util.DialogAlert('Book added to card', 'Cart updated', 'success')
    }

    async navigatePlay(shouldStart = false, chapterNumber = null) {
        const bookData = this.props.route.params?.item;
        if(bookData?.is_purchased) {
            navigate(APP_ROUTES.PLAYER_SCREEN.routeName, { book: bookData, newStart: shouldStart, chapterNumber });
            return;
        }
        if(await TrackPlayer.getState() === State.Playing) {
            util.DialogAlert('Please stop the player and try again');
            return;
        } else this.setState({ showDemoModal: true })
    }

    _downloadChapter(chapter, bookData) {
        console.log('bookData',chapter)
        this.setState({ loadingId: chapter?.id })
        RNFetchBlob.config({
            // add this option that makes response data to be stored as a file,
            fileCache : true,
            appendExt : 'mp3'
        })
        .fetch('GET', chapter?.url, {})
        .then((res) => {
            chapter.chapterFilePath = res.path();
            this.props.saveDownloadedChapter(chapter)
            this.setState({ loadingId: null })
        }).catch(error=> {
            console.log('error', error)
            this.setState({ loadingId: null })
        })
    }

    _renderBookChapters = ({ item }) => {
        const bookData = this.props.route.params?.item;
        const { id, chapter_no, title, media, url } = item;
        const downloadedChapter = this.props.trackState.chapters.find(chp => chp.id === id);
        return(
            <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 15, paddingHorizontal: 5}}>
                <TouchableOpacity style={{width: 55, height: 55, backgroundColor: Theme.themeFontColor, borderRadius: 30, justifyContent: 'center', alignItems: 'center'}}
                activeOpacity={1}
                disabled={bookData?.is_purchased === 0}
                onPress={()=> this.navigatePlay(false, chapter_no)}>
                    <Image 
                        source={bookData?.is_purchased === 0 ? Images.PasswordLock : Images.Play}
                        style={{width: 20, height: 20, resizeMode: 'contain'}}
                    />
                </TouchableOpacity>
                <View style={{flex: 1, marginLeft: 10, marginRight: 5}}>
                    <Text style={{
                        color: Theme.rgbaWhite,
                        fontSize: Fonts.size.xxSmall,
                        fontFamily: Fonts.type.Regular
                    }}>Chapter {chapter_no}</Text>
                    <Text 
                    numberOfLines={2}
                    style={{
                        color: Theme.white,
                        fontSize: Fonts.size.normal,
                        fontFamily: Fonts.type.SemiBold
                    }}>{title}</Text>
                </View>
                {
                    downloadedChapter?.chapterFilePath ? null : (
                        <TouchableOpacity
                        onPress={()=> this._downloadChapter(item, bookData)}>
                            {
                                id === this.state.loadingId ? (
                                    <ActivityIndicator size={'small'} color={Theme.white}/>
                                ) : bookData?.is_purchased ? <Image
                                    source={Images.Download}
                                    style={{width: 25, height: 25, resizeMode: 'contain'}}
                                /> : null
                            }
                        </TouchableOpacity>
                    )
                }
            </View>
        )
    }

    _renderSeperator() {
        return(
            <View 
                style={{height: 0.7, backgroundColor: Theme.rgbaWhite, marginHorizontal: 8}}
            />
        )
    }

    _renderChapterHeading() {
        return(
            <View style={{borderBottomColor: Theme.themeFontColor, borderBottomWidth: 3, marginBottom: 10}}>
                <Text
                    style={{
                        color: Theme.white,
                        fontFamily: Fonts.type.CiceroMedium,
                        fontSize: Fonts.size.xLarge,
                    }}
                >
                    Chapters
                </Text>
            </View>
        )
    }

    render() {
        const { item } = this.props.route.params;
        const animatedStyle = this.state.addCartState ? {
            width: 30,
            height: 30
        } : {
            width: 22,
            height: 22
        }
        const { trackState } = this.props.trackState
        // console.log('track state', this.props.trackState)
        let isVerifiedState = trackState?.bookId === item?.id
        return (
            <View style={{flex: 1}}>
                <View style={{backgroundColor: Theme.themeFontColor, borderBottomLeftRadius: 30, borderBottomRightRadius: 30, paddingHorizontal: 20, paddingVertical: 10, paddingBottom: 20}}>
                    <Text style={{color: Theme.rgbaWhite, fontSize: Fonts.size.small, fontFamily: Fonts.type.Regular}}>{item.author?.name}</Text>
                    <Text style={{color: Theme.rgbaWhite, fontSize: Fonts.size.medium, fontFamily: Fonts.type.Bold, width: '60%'}} numberOfLines={2}>{item.title}</Text>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        {item?.is_purchased === 0 ? (
                            <TouchableOpacity
                            style={{
                                height: 45,
                                backgroundColor: Theme.white,
                                // width: '35%',
                                paddingHorizontal: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 40
                            }}
                            activeOpacity={0.7}
                            onPress={()=> this.navigatePlay()}>
                                <Text style={{color: Theme.themeFontColor, fontFamily: Fonts.type.Regular, fontSize: Fonts.size.small}}>
                                    {/* {item?.is_purchased === 0 ? 'Play Demo' : isVerifiedState ? 'Continue' : ' Start '} */}
                                    Play Demo
                                </Text>
                            </TouchableOpacity>
                            ) : <View style={{height: 35}}/>
                        }
                        {
                            item?.is_purchased === 0 && (
                                <TouchableOpacity
                                style={{
                                    backgroundColor: Theme.white,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 40,
                                    marginHorizontal: 5,
                                    height: 45,
                                    width: 45
                                }}
                                activeOpacity={1}
                                onPress={()=> this.addToCart()}>
                                    <Image 
                                        source={this.state.addCartState ? Images.Check : Images.Cart}
                                        style={{...animatedStyle, resizeMode: 'contain', tintColor: Theme.themeFontColor}}
                                    />
                                </TouchableOpacity>
                            )
                        }
                    </View>
                </View>
                <View style={{position: 'absolute', right: 20, top: 20, borderRadius: 20, overflow: 'hidden', zIndex: 99}}>
                    <Image 
                        source={{uri: item.image}}
                        style={{width: 150, height: 190, resizeMode: 'cover'}}
                    />
                </View>
                <View style={{height: Platform.OS === 'android' ? 40 : 55, backgroundColor: Theme.primaryBackground}}></View>
                <FlatList
                    data={item?.chapter}
                    renderItem={this._renderBookChapters}
                    keyExtractor={({ id }) => id.toString()}
                    style={{flex: 1, marginTop: 40, paddingHorizontal: 20}}
                    ListHeaderComponent={this._renderChapterHeading()}
                    ListFooterComponent={()=><View style={{height: 20}}/>}
                    ItemSeparatorComponent={()=> this._renderSeperator()}
                />
                {
                    this.state.showDemoModal && <DemoPlayer trackDetails={item} onClose={()=> this.setState({ showDemoModal: false })}/>
                }
            </View>
        )
    }
}

const mapStateToProps = ({ trackState }) => ({
    trackState
})

const actions = { addBookToCart, saveDownloadedChapter };

export default connect(mapStateToProps, actions)(BookDetails);