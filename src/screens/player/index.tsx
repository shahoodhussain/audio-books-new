import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
import TrackPlayer, { State, Capability } from 'react-native-track-player';
import { AudioPlayer } from '../../components';
import util from '../../util';
import { connect } from "react-redux";

export class NowPlaying extends Component {

    state = {
        tracks: [],
        bookId: null,
        playerReady: false,
        chapterPlay: null
    }

    async componentDidMount() {
        const { chapter, id } = this.props.route.params?.book
        const chap = chapter.find(chp => chp.chapter_no === this.props.route.params?.chapterNumber);
        chapter.map(chp => {
            chp.artist = '';
            chp.id = chp.id.toString()
        })
        await this.initPlayer();
        this.setState({
            tracks: chapter,
            bookId: id,
            playerReady: true,
            chapterPlay: chap
        })
    }

    async initPlayer() {
        try {
            await TrackPlayer.destroy();
            TrackPlayer.updateOptions({
                stopWithApp: false, // false=> music continues in background even when app is closed
                // Media controls capabilities
                capabilities: [
                    Capability.Play,
                    Capability.Pause,
                    Capability.Stop,
                    Capability.SeekTo,
                ],
                // Capabilities that will show up when the notification is in the compact form on Android
                compactCapabilities: [
                    Capability.Play,
                    Capability.Pause,
                    Capability.Stop,
                    Capability.SeekTo,
                ],
            });
            await TrackPlayer.setupPlayer();
        } catch (e) {
            console.log('init player---', e);
            util.DialogAlert('Unable to play book');
        }
    }

    render() {
        const { book, newStart = true } = this.props.route.params;
        const { bookId, chapterPlay } = this.state;
        console.log('track state', this.props.trackState.chapters)
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <View
                    style={{
                        width: 230,
                        height: 350,
                        // overflow: 'hidden',
                    }}>
                        <Image 
                            source={{uri: book?.image}}
                            style={{width: 270, height: 350, right: 20, resizeMode: 'contain'}}
                        />
                    </View>
                    {/* <Text>title</Text> */}
                </View>
                <AudioPlayer 
                    bookId={bookId}
                    allTracks={this.state.tracks}
                    // trackState={newStart ? null : this.props.trackState?.trackState}
                    trackState={this.props.trackState.chapters}
                    chapterPlay={chapterPlay}
                />
            </View>
        )
    }
}

const mapStateToProps = ({ trackState }) => ({
    trackState
})

export default connect(mapStateToProps, null)(NowPlaying);