import React, { useState, useEffect } from 'react'
import { View, Text, Image, FlatList, TouchableOpacity, ActivityIndicator, RefreshControl } from 'react-native'
import { useDispatch, useSelector } from "react-redux";
import { Fonts, Images, Theme } from '../../assets/theme';
import { getPaymentCards, checkout } from "../../redux/actions/card.actions";
import { APP_ROUTES } from '../../router/RoutesConfig';
import { navigate } from '../../services/NavigationService';

const ConfirmPayment = () => {
    const dispatch = useDispatch()
    const [loading, setloading] = useState(false)
    const [isPaying, payNowLoading] = useState(false)
    const [isCardSelected, setSelectedCard] = useState(null)
    const [allCards, setAllCards] = useState([])
    const cart = useSelector(store => store.cart);

    const getCard = () => {
        setloading(true)
        dispatch(getPaymentCards(responseData=> {
            if(responseData.length > 0) {
                setloading(false);
                setAllCards(responseData);
            } else {
                setloading(false)
            }
        }));
    }

    useEffect(() => {
        getCard();
    }, [])

    const payNow = () => {
        payNowLoading(true)
        dispatch(checkout(
            isCardSelected,
            cart?.cartItems,
            responseData => {
                payNowLoading(false)
            }
        ))
    }

    const _renderCard = ({item, index}) => {
        return(
            <TouchableOpacity
            activeOpacity={0.7}
            onPress={()=> setSelectedCard(item.id)}
            style={{
                backgroundColor: Theme.white,
                flexDirection: 'row',
                marginHorizontal: 20,
                height: 50,
                alignItems: 'center',
                borderRadius: 10
            }}>
                <View style={{
                    marginHorizontal: 10
                }}>
                    <Image 
                        source={item?.brand === 'Visa' ? Images.Visa : Images.MasterCard}
                        style={{
                            width: 50,
                            height: 50,
                            resizeMode: 'contain'
                        }}
                    />
                </View>
                <View style={{
                    flex: 1,
                    marginHorizontal: 10
                }}>
                    <Text style={{
                        color: Theme.themeFontColor,
                        fontFamily: Fonts.type.CiceroMedium,
                        fontSize: Fonts.size.medium
                    }}>{item?.last4}</Text>
                </View>
                <View
                style={{
                    marginHorizontal: 20
                }}>
                    <Image 
                        source={isCardSelected === item?.id ? Images.Check : Images.CheckMark}
                        style={{
                            width: 25,
                            height: 25,
                            borderRadius: 20,
                            resizeMode: 'contain',
                            tintColor: isCardSelected === item?.id ?Theme.themeFontColor : Theme.lightText
                        }}
                    />
                </View>
            </TouchableOpacity>
        )
    }

    const _renderPaynowButton = () => {
        return(
            <TouchableOpacity
            activeOpacity={0.9}
            onPress={payNow}
            disabled={isPaying}
            style={{
                alignSelf: 'center',
                height: 50,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 25,
                width: 150,
                backgroundColor: Theme.themeFontColor,
                marginVertical: 20,
            }}>
                {
                    isPaying ? <ActivityIndicator size={'large'} color={Theme.white}/> : (
                        <Text style={{
                            color: Theme.white,
                            fontSize: Fonts.size.medium,
                            fontFamily: Fonts.type.CiceroMedium
                        }}>Pay Now</Text>
                    )
                }
            </TouchableOpacity>
        )
    }

    const _renderFooterComponent = () => {
        return !loading && (
            <TouchableOpacity
            activeOpacity={0.9}
            onPress={()=> navigate(APP_ROUTES.ADD_CARD.routeName)}
            style={{
                alignSelf: 'center',
                paddingVertical: 10,
                marginVertical: 20,
                paddingHorizontal: 30
            }}>
                <Text style={{
                    color: Theme.white,
                    fontSize: Fonts.size.medium,
                    fontFamily: Fonts.type.CiceroMedium
                }}>+ {allCards.length > 0 ? 'Add Another Card' : 'Add Card'}</Text>
            </TouchableOpacity>
        )
    }

    if(loading) return <ActivityIndicator size={'large'} color={Theme.white}/>

    return (
        <View style={{flex: 1}}>
            <FlatList
                data={allCards}
                keyExtractor={(item)=> item?.id.toString()}
                renderItem={_renderCard}
                style={{ paddingTop: 20 }}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={_renderFooterComponent}
                refreshControl={<RefreshControl onRefresh={getCard} refreshing={loading} tintColor={Theme.white} colors={[Theme.black]}/>}
            />
            {isCardSelected && _renderPaynowButton()}
        </View>
    )
}

export default ConfirmPayment
