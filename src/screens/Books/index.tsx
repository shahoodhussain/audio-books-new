import React, { Component } from 'react'
import { Text, View, RefreshControl, FlatList, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import { Theme } from '../../assets/theme';
import { APP_ROUTES } from '../../router/RoutesConfig';
import { navigate } from '../../services/NavigationService';
import { connect } from "react-redux";
import { fetchBooksByCategory } from "../../redux/actions/books.actions";
import styles from "./styles";

export class AllBooks extends Component {
    state = {
        allBooks: [],
        loadingBooks: false,
        currentOffset: 0,
        lastOffset: 0,
    }

    componentDidMount() {
        this.getBooksCategories()
    }

    getBooksCategories() {
        const { currentOffset, allBooks, lastOffset } = this.state;
        this.setState({ loadingBooks: true })
        const item = this.props.route.params?.item;
        this.props.fetchBooksByCategory(
            currentOffset,
            item?.id,
            undefined,
            (books) => {
                this.setState({
                    allBooks: [...allBooks, ...books],
                    loadingBooks: false,
                    currentOffset: books.length > 0 ? currentOffset + books.length + 1 : lastOffset,
                    lastOffset: currentOffset + books.length
                })
            }
        );
    }

    loadMoreCategories = () => {
        setTimeout(() => {
            this.getBooksCategories();
        }, 1000);
    }

    _renderBookItem(item) {
        const { id, title, image, author } = item;
        return(
            <TouchableOpacity style={styles.bookItem} activeOpacity={1}
            onPress={()=> navigate(APP_ROUTES.BOOK_DETAILS.routeName, { item })}>
                <View style={styles.imageContainer}>
                    <Image 
                        source={{uri: image}}
                        style={styles.image}
                    />
                </View>
                <View style={styles.textContainer}>
                    <Text numberOfLines={2} style={styles.title}>{title}</Text>
                    <Text numberOfLines={2} style={styles.author}>{author?.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const { allBooks, loadingBooks } = this.state;
        if (loadingBooks && allBooks.length === 0) return <ActivityIndicator color={Theme.white} size={'large'} style={styles.flex}/>
        return (
            <View style={styles.flex}>
                <FlatList
                    numColumns={2}
                    data={allBooks}
                    renderItem={({item})=> this._renderBookItem(item)}
                    keyExtractor={({id})=> id.toString()}
                    style={styles.flatListStyle}
                    showsVerticalScrollIndicator={false}
                    onEndReachedThreshold={0}
                    ListEmptyComponent={()=> <Text style={styles.emptyList}>No Categories found</Text>}
                    ListFooterComponent={()=> loadingBooks && <ActivityIndicator color={Theme.white} size={'large'} />}
                    ListFooterComponentStyle={{marginBottom: 45}}
                    onEndReached={!loadingBooks && this.loadMoreCategories}
                    refreshControl={<RefreshControl refreshing={loadingBooks} onRefresh={()=>this.getBooksCategories()} tintColor={Theme.white} colors={[Theme.black]}/>}
                />
            </View>
        )
    }
}

const actions = { fetchBooksByCategory };

export default connect(null, actions)(AllBooks);