import { StyleSheet } from "react-native";
import { Fonts, Theme } from "../../assets/theme";

export default StyleSheet.create({
    flex: {
        flex: 1
    },
    flatListStyle: {
        paddingTop: 10,
        paddingHorizontal: 20
    },
    bookItem: {
        flex: 1,
        marginHorizontal: 10,
        marginBottom: 30
    },
    imageContainer: {
        borderRadius: 15,
        overflow: 'hidden',
        height: 220,
        width: '100%'
    },
    image: {
        flex: 1,
        resizeMode: 'cover'
    },
    textContainer: {
        width: 130, marginTop: 5
    },
    title: {
        color: Theme.white,
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.small
    },
    author: {
        color: '#ffffff70',
        fontFamily: Fonts.type.Regular,
        fontSize: Fonts.size.xxSmall
    },
    emptyList: {
        alignSelf: 'center',
        marginVertical: 50,
        color: Theme.white,
        fontSize: Fonts.size.large,
        fontFamily: Fonts.type.CiceroMedium
    }
})