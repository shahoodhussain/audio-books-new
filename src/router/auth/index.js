import React from 'react'

// @Authentication component imports
import AppStarter from "../../screens/authentication/AppStarter";
import SignIn from "../../screens/authentication/Login";
import SignUp from "../../screens/authentication/Registeration";
import OTPSignUp from "../../screens/authentication/Otp";
// // import ChangePassword from "../../screens/authentication/ChangePassword";
import ForgotPassword from "../../screens/authentication/ForgotPassword";
import ResetPassword from "../../screens/authentication/ResetPassword";
import CreateProfile from "../../screens/authentication/ProfileCreation";
import Policy from '../../screens/authentication/Agreements/Policy';
import TermCondition from '../../screens/authentication/Agreements/TermCondition';

import { ROUTES } from "../RoutesConfig";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { BackgroundImageWrapper, NavigationComponents } from '../../components';

const Stack = createNativeStackNavigator();

function AuthRoutes() {
    return(
        <BackgroundImageWrapper
        isNavigation={true}>
            <Stack.Navigator initialRouteName={ROUTES.APP_START.routeName} screenOptions={{animation: 'simple_push', headerShadowVisible: false}}>
                <Stack.Group>
                    <Stack.Screen
                        name={ROUTES.APP_START.routeName}
                        component={AppStarter}
                        options={ROUTES.APP_START.options}
                    />
                    <Stack.Screen
                        name={ROUTES.LOGIN.routeName}
                        component={SignIn}
                        options={ROUTES.LOGIN.options}
                    />
                    <Stack.Screen
                        name={ROUTES.FORGOT_PASSWORD.routeName}
                        component={ForgotPassword}
                        options={ROUTES.FORGOT_PASSWORD.options}
                    />
                    <Stack.Screen
                        name={ROUTES.ACCOUNT_VERIFICATION.routeName}
                        component={OTPSignUp}
                        options={ROUTES.ACCOUNT_VERIFICATION.options}
                    />
                    <Stack.Screen
                        name={ROUTES.RESET_PASSWORD.routeName}
                        component={ResetPassword}
                        options={ROUTES.RESET_PASSWORD.options}
                    />
                    <Stack.Screen
                        name={ROUTES.SIGNUP_ROUTE.routeName}
                        component={SignUp}
                        options={ROUTES.SIGNUP_ROUTE.options}
                    />
                    <Stack.Screen
                        name={ROUTES.COMPLETE_PROFILE.routeName}
                        component={CreateProfile}
                        options={ROUTES.COMPLETE_PROFILE.options}
                    />
                </Stack.Group>
                <Stack.Group screenOptions={{ presentation: 'modal', animation: 'slide_from_bottom', contentStyle: {backgroundColor: 'transparent'}}}>
                    <Stack.Screen
                        name={ROUTES.PRIVACY_POLICY.routeName}
                        component={Policy}
                        options={({navigation})=>({
                            ...ROUTES.PRIVACY_POLICY.options,
                            headerLeft: ()=> <NavigationComponents.HeaderBack onPress={()=>navigation.goBack()}/>,
                        })}
                    />
                    <Stack.Screen
                        name={ROUTES.TERMS_AND_CONDITIONS.routeName}
                        component={TermCondition}
                        options={({navigation})=>({
                            ...ROUTES.TERMS_AND_CONDITIONS.options,
                            headerLeft: ()=> <NavigationComponents.HeaderBack onPress={()=>navigation.goBack()}/>,
                        })}
                    />
                </Stack.Group>
                {/* <Stack.Screen
                    name={ROUTES.CREATE_PROFILE.routeName}
                    component={CreateProfile}
                    options={({navigation})=>({
                        ...ROUTES.CREATE_PROFILE.options,
                        headerLeft: ()=> <HeaderLeft onPress={()=> navigation.goBack()} blackHeaderIcon={true}/>,
                    })}
                />
                <Stack.Screen
                    name={ROUTES.CHANGE_PASSWORD.routeName}
                    component={ChangePassword}
                    options={({navigation})=>({
                        ...ROUTES.CHANGE_PASSWORD.options,
                        headerLeft: ()=> <HeaderLeft onPress={()=> navigation.goBack()} blackHeaderIcon={true}/>,
                    })}
                />
                <Stack.Screen
                    name={ROUTES.COMPLETE_PROFILE.routeName}
                    component={CompleteProfile}
                    options={({navigation})=>({
                        ...ROUTES.ACCOUNT_VERIFICATION.options,
                        headerTitle: ()=> <HeaderTitle title="Complete Profile" fontColor={Colors.black}/>,
                        headerLeft: ()=> <HeaderLeft onPress={()=> navigation.goBack()} blackHeaderIcon={true}/>,
                    })}
                /> */}
            </Stack.Navigator>
        </BackgroundImageWrapper>
    )
}

export default AuthRoutes
