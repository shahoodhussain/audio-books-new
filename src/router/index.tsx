/**
 * Created by Shahood Hussain.
 */

import React, { PureComponent } from 'react';
import { StatusBar } from 'react-native';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { setNavigator } from '../services/NavigationService';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// @navigation imports
import { NavigationContainer } from '@react-navigation/native';

// @App logic component imports
import AppRoutes from './user';

// @Authentication component imports
import AuthRoutes from './auth';
import { APP_ROUTES } from './RoutesConfig';
import EditProfile from '../screens/edit_profile';
import { Theme } from '../assets/theme';
import { NavigationComponents } from '../components';

const Stack = createNativeStackNavigator();

class Routes extends PureComponent {
  state = {
    isLoaded: false,
  };

  componentDidMount() {
    setTimeout(() => {
      SplashScreen.hide();
      this.setState({ isLoaded: true });
    }, 1000);
  }

  render() {
    const { userData } = this.props;
    const headerBack = <NavigationComponents.HeaderBack title={APP_ROUTES.COMPLETE_PROFILE.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>;
    return (
      <NavigationContainer ref={nav => setNavigator(nav)}>
        <StatusBar barStyle="light-content" translucent backgroundColor='transparent'/>
        <Stack.Navigator screenOptions={{ headerShown: false, animationTypeForReplace: 'push', headerShadowVisible: false }}>
          {
            userData?.access_token ?
              <Stack.Screen name="App" component={AppRoutes} /> : <Stack.Screen name="Auth" component={AuthRoutes} />
          }
          <Stack.Screen 
          name={APP_ROUTES.COMPLETE_PROFILE.routeName} 
          component={EditProfile}
          options={{
            ...APP_ROUTES.COMPLETE_PROFILE.options,
            headerLeft: ()=> headerBack
          }}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  userData: user,
});

export default connect(mapStateToProps, null)(Routes);
