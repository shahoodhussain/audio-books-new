import {Fonts, Theme} from '../assets/theme';

export const ROUTES = Object.freeze({
    APP_START: {
        routeName: '/StartScreen',
        options: {
            headerShown: false,
        }
    },
    TERMS_AND_CONDITIONS: {
        routeName: '/TermsAndConditions',
        options: {
            title: 'TERMS & CONDITIONS',
            headerTitleAlign: "center",
            headerShown: true,
            headerStyle: {elevation: 0, shadowColor: 0, backgroundColor: Theme.black},
            headerTintColor: Theme.white,
        }
    },
    PRIVACY_POLICY: {
        routeName: '/PrivacyPolicy',
        options: {
            title: 'PRIVACY POLICY',
            headerTitleAlign: "center",
            headerShown: true,
            headerStyle: {elevation: 0, shadowColor: 0, backgroundColor: Theme.black},
            headerTintColor: Theme.white,
        }
    },
    LOGIN: {
        routeName: '/Login',
        options: {
            title: 'LOGIN',
            headerTitleAlign: 'center',
            headerShown: false,
            headerStyle: {backgroundColor: Theme.transparent},
            headerTintColor: Theme.white
        }
    },
    SIGNUP_ROUTE: {
        routeName: '/SignUp',
        options: {
            title: 'SIGNUP',
            headerTitleAlign: 'center',
            headerShown: false,
            headerStyle: {backgroundColor: Theme.transparent},
            headerTintColor: Theme.white
        }
    },
    FORGOT_PASSWORD: {
        routeName: '/ForgotPassword',
        options: {
            title: 'FORGOT PASSWORD',
            headerTitleAlign: 'center',
            headerShown: false,
            headerStyle: {backgroundColor: Theme.transparent},
            headerTintColor: Theme.white
        }
    },
    ACCOUNT_VERIFICATION: {
        routeName: '/AccountVerification',
        options: {
            title: 'VERIFICATION',
            headerTitleAlign: 'center',
            headerShown: false,
            headerStyle: {backgroundColor: Theme.transparent},
            headerTintColor: Theme.white
        }
    },
    RESET_PASSWORD: {
        routeName: '/ResetPassword',
        options: {
            title: 'RESET PASSWORD',
            headerStyle: { elevation: 0, shadowColor: 0, backgroundColor: Theme.transparent},
            headerTitleAlign: "center",
            headerShown: false,
            headerTintColor: Theme.white
        }
    },
    EDIT_PROFILE: {
        routeName: '/EditProfile',
        options: {
            title: 'PROFILE',
            headerTitleAlign: "center",
            headerStyle: {
                backgroundColor: Theme.primaryBackground
            }
        }
    },
    CREATE_PROFILE: {
        routeName: '/ProfileCreation',
        options: {
            title: 'PROFILE CREATION',
            headerTitleAlign: "center",
            headerStyle: {
                backgroundColor: Theme.primaryBackground
            }
        }
    },
    CHANGE_PASSWORD: {
        routeName: '/ChangePassword',
        options: {
            title: 'Change Password',
            headerStyle: { elevation: 0, shadowColor: 0, backgroundColor: Theme.primaryBackground },
            headerTitleAlign: "center",
            headerShown: true,
        }
    },
    COMPLETE_PROFILE: {
        routeName: '/CompleteProfile',
        options: {
            title: 'Complete Profile',
            headerStyle: { elevation: 0, shadowColor: 0, backgroundColor: Theme.transparent},
            headerTitleAlign: "center",
            headerShown: false,
            headerTintColor: Theme.white
        }
    }
});

const headerProps = { 
    headerStyle: {
        elevation: 0, shadowColor: 0, backgroundColor: Theme.primaryBackground
    },
    headerTitleAlign: "left",
    headerShown: true,
    headerTintColor: Theme.white,
    headerTitleStyle: {
        fontSize: Fonts.size.large,
        fontFamily: Fonts.type.SemiBold
    },
    contentStyle: {
        backgroundColor: Theme.primaryBackground
    }
};

export const APP_ROUTES = Object.freeze({
    //@desc tab routes config
    TABBOOKS: {
        routeName: '/TabBooks',
        options: {
            title: 'Books',
        }
    },
    TABLIBRARY: {
        routeName: '/TabLibrary',
        options: {
            title: 'Library',
        }
    },
    TABHOME: {
        routeName: '/TabHome',
        options: {
            title: 'Home',
        }
    },
    TABCATEGORY: {
        routeName: '/TabCATEGORY',
        options: {
            title: 'Category',
        }
    },
    TABPROFILE: {
        routeName: '/TabProfile',
        options: {
            title: 'Profile',
        }
    },
    //@desc home stack routes
    PLAYER_SCREEN: {
        routeName: '/PlayerScreen',
        options: {
            title: null,
            customTitle: 'Now Playing',
            ...headerProps,
        }
    },
    CART: {
        routeName: '/Cart',
        options: {
            title: null,
            customTitle: 'Cart',
            ...headerProps,
        }
    },
    CARD: {
        routeName: '/Card',
        options: {
            title: null,
            customTitle: 'Card Details',
            ...headerProps,
        }
    },
    ADD_CARD: {
        routeName: '/AddCard',
        options: {
            title: null,
            customTitle: 'Add Card',
            ...headerProps,
        }
    },
    PAYMENT_OPTIONS: {
        routeName: '/PaymentOptions',
        options: {
            title: null,
            customTitle: 'Payment Options',
            ...headerProps,
        }
    },
    HOME: {
        routeName: '/Home',
        options: {
            title: null,
            customTitle: 'Home',
            ...headerProps,
        }
    },
    BOOK_DETAILS: {
        routeName: '/BookDetails',
        options: {
            title: null,
            customTitle: 'Book',
            ...headerProps,
            headerStyle: {
                backgroundColor: Theme.themeFontColor
            }
        }
    },
    BOOKS: {
        routeName: '/Books',
        options: {
            title: null,
            customTitle: 'Books',
            ...headerProps,
        }
    },
    LIBRARY: {
        routeName: '/Library',
        options: {
            title: null,
            customTitle: 'Library',
            ...headerProps,
        }
    },
    CATEGORY: {
        routeName: '/Category',
        options: {
            title: null,
            customTitle: 'Category',
            ...headerProps,
        }
    },
    CATEGORY_BOOKS: {
        routeName: '/CategoryBooks',
        options: {
            title: null,
            customTitle: 'Books',
            ...headerProps,
        }
    },
    PROFILE: {
        routeName: '/Profile',
        options: {
            title: null,
            customTitle: 'PROFILE',
            ...headerProps,
            headerStyle: {
                backgroundColor: Theme.themeFontColor
            }
        }
    },
    PRIVACY_POLICY: {
        routeName: '/PrivacyPolicy',
        options: {
            title: null,
            ...headerProps,
            presentation: 'modal',
            headerStyle: {
                backgroundColor: Theme.themeFontColor
            }
        }
    },
    TERMS_CONDITIONS: {
        routeName: '/TermsConditions',
        options: {
            title: null,
            ...headerProps,
            presentation: 'modal',
            headerStyle: {
                backgroundColor: Theme.themeFontColor
            }
        }
    },
    SHIPPING: {
        routeName: '/Shipping',
        options: {
            title: 'Ship-to Information',
            headerTitleAlign: "center",
            headerShown: true
        }
    },
    ADDRESS: {
        routeName: '/Address',
        options: {
            ...headerProps
        }
    },
    ADDRESSES: {
        routeName: '/Addresses',
        options: {
            title: 'ADDRESS',
            ...headerProps
        }
    },
    PAYMENT: {
        routeName: '/Payment',
        options: {
            ...headerProps
        }
    },
    PAYMENT_TYPE: {
        routeName: '/PaymentType',
        options: {
            title: 'PAYMENT',
            ...headerProps
        }
    },
    CREATE_PROFILE: {
        routeName: '/ProfileCreation',
        options: {
            title: 'Profile Creation',
            headerTitleAlign: 'center',
            headerStyle: {
                backgroundColor: Theme.primaryBackground,
            },
        },
    },
    CHANGE_PASSWORD: {
        routeName: '/ChangePassword',
        options: {
            title: null,
            ...headerProps,
            headerStyle: {
                backgroundColor: Theme.themeFontColor
            }
        },
    },
    COMPLETE_PROFILE: {
        routeName: '/CompleteProfile',
        options: {
            title: null,
            customTitle: 'Edit Profile',
            presentation: 'modal',
            animation: 'slide_from_bottom',
            ...headerProps,
        },
    },
});
