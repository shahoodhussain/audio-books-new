import React from 'react';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { NavigationComponents } from '../../components';
import { Theme } from '../../assets/theme';
import { APP_ROUTES, ROUTES } from '../RoutesConfig';

// @App logic component imports
import AllBooks from '../../screens/Books';
import UserHome from '../../screens/home';
import BookDetails from '../../screens/book_details';
import Library from '../../screens/library';
import NowPlaying from '../../screens/player';
import CategoryHome from '../../screens/category';
import profile from '../../screens/profile';
import TermCondition from '../../screens/authentication/Agreements/TermCondition';
import PrivacyPolicy from '../../screens/authentication/Agreements/Policy';
import ChangePassword from "../../screens/authentication/ChangePassword";
import Cart from '../../screens/cart';
import CardPayment from '../../screens/payment';
import AddCard from '../../screens/add_card';
import ConfirmPayment from '../../screens/confirm_payment';
import EditProfile from '../../screens/edit_profile';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function HomeStack({navigation}) {
  const searchIcon = <NavigationComponents.HeaderSearchIcon {...navigation} />;
  return (
      <Stack.Navigator screenOptions={{headerShadowVisible: false}}>
        <Stack.Group>
          <Stack.Screen
            name={APP_ROUTES.HOME.routeName}
            component={UserHome}
            options={{
              ...APP_ROUTES.HOME.options,
              headerLeft: ()=> <NavigationComponents.HeaderTitle title={APP_ROUTES.HOME.options.customTitle}/>,
              headerRight: () => <NavigationComponents.CartItem />,
            }}
          />
          <Stack.Screen
            name={APP_ROUTES.BOOKS.routeName}
            component={AllBooks}
            options={({route})=>({
              ...APP_ROUTES.BOOKS.options,
              headerLeft: ()=> <NavigationComponents.HeaderBack title={route.params?.name ? route.params?.name : APP_ROUTES.BOOKS.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>
            })}
          />
          <Stack.Screen
            name={APP_ROUTES.BOOK_DETAILS.routeName}
            component={BookDetails}
            options={({route})=>({
              ...APP_ROUTES.BOOK_DETAILS.options,
              headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.BOOK_DETAILS.options.customTitle}/>,
              headerRight: ()=> <NavigationComponents.BookMarkIcon route={route.params}/>
            })}
          />
          <Stack.Screen
            name={APP_ROUTES.PLAYER_SCREEN.routeName}
            component={NowPlaying}
            options={{
              ...APP_ROUTES.PLAYER_SCREEN.options,
              headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.PLAYER_SCREEN.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
              // headerRight: () => <NavigationComponents.Options />,
            }}
          />
          <Stack.Screen
            name={APP_ROUTES.CART.routeName}
            component={Cart}
            options={{
              ...APP_ROUTES.CART.options,
              headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.CART.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
            }}
          />
          <Stack.Screen
            name={APP_ROUTES.PAYMENT_OPTIONS.routeName}
            component={ConfirmPayment}
            options={{
              ...APP_ROUTES.PAYMENT_OPTIONS.options,
              headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.PAYMENT_OPTIONS.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
            }}
          />
          <Stack.Screen
            name={APP_ROUTES.ADD_CARD.routeName}
            component={AddCard}
            options={{
              ...APP_ROUTES.ADD_CARD.options,
              presentation: 'modal',
              headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.ADD_CARD.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
            }}
          />
        </Stack.Group>
      </Stack.Navigator>
  );
}

function CategoryStack() {
  return(
    <Stack.Navigator screenOptions={{headerShadowVisible: false}}>
      <Stack.Screen
        name={APP_ROUTES.CATEGORY.routeName}
        component={CategoryHome}
        options={({navigation})=>({
          ...APP_ROUTES.CATEGORY.options,
          headerLeft: ()=> <NavigationComponents.HeaderTitle title={APP_ROUTES.CATEGORY.options.customTitle}/>,
        })}
      />
      <Stack.Screen
        name={APP_ROUTES.CATEGORY_BOOKS.routeName}
        component={AllBooks}
        options={({navigation, route})=>({
          ...APP_ROUTES.CATEGORY_BOOKS.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={route.params?.title ? route.params?.title : APP_ROUTES.CATEGORY_BOOKS.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>
        })}
      />
      <Stack.Screen
        name={APP_ROUTES.BOOK_DETAILS.routeName}
        component={BookDetails}
        options={{
          ...APP_ROUTES.BOOK_DETAILS.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.BOOK_DETAILS.options.customTitle}/>
        }}
      />
      <Stack.Screen
        name={APP_ROUTES.PLAYER_SCREEN.routeName}
        component={NowPlaying}
        options={{
          ...APP_ROUTES.PLAYER_SCREEN.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.PLAYER_SCREEN.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
          // headerRight: () => <NavigationComponents.Options />,
        }}
      />
    </Stack.Navigator>
  )
}

function ProfileStack() {
  return(
    <Stack.Navigator screenOptions={{headerShadowVisible: false}}>
      <Stack.Screen
        name={APP_ROUTES.PROFILE.routeName}
        component={profile}
        options={({navigation})=>({
          ...APP_ROUTES.PROFILE.options,
          headerLeft: ()=> <NavigationComponents.HeaderTitle title={APP_ROUTES.PROFILE.options.customTitle} style={{color: Theme.white}}/>,
          headerRight: () => <NavigationComponents.EditIcon />,
        })}
      />
      <Stack.Screen
        name={ROUTES.PRIVACY_POLICY.routeName}
        component={PrivacyPolicy}
        options={({navigation})=>({
          ...APP_ROUTES.PRIVACY_POLICY.options,
          animation: 'fade_from_bottom',
          headerLeft: ()=> <NavigationComponents.HeaderBack title={ROUTES.PRIVACY_POLICY.options.title} style={{color: Theme.white}}/>,
        })}
      />
      <Stack.Screen
        name={ROUTES.TERMS_AND_CONDITIONS.routeName}
        component={TermCondition}
        options={({navigation})=>({
          ...APP_ROUTES.TERMS_CONDITIONS.options,
          animation: 'fade_from_bottom',
          headerLeft: ()=> <NavigationComponents.HeaderBack title={ROUTES.TERMS_AND_CONDITIONS.options.title} style={{color: Theme.white}}/>,
        })}
      />
      <Stack.Screen
        name={ROUTES.CHANGE_PASSWORD.routeName}
        component={ChangePassword}
        options={({navigation})=>({
          ...APP_ROUTES.CHANGE_PASSWORD.options,
          animation: 'fade_from_bottom',
          headerLeft: ()=> <NavigationComponents.HeaderBack title={ROUTES.CHANGE_PASSWORD.options.title} style={{color: Theme.white}}/>,
        })}
      />
      <Stack.Screen
        name={APP_ROUTES.CARD.routeName}
        component={CardPayment}
        options={{
          ...APP_ROUTES.CARD.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.CARD.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
        }}
      />
      <Stack.Screen
        name={APP_ROUTES.ADD_CARD.routeName}
        component={AddCard}
        options={{
          ...APP_ROUTES.ADD_CARD.options,
          presentation: 'modal',
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.ADD_CARD.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
        }}
      />
      <Stack.Screen
        name={APP_ROUTES.CART.routeName}
        component={Cart}
        options={{
          ...APP_ROUTES.CART.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.CART.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
        }}
      />
    </Stack.Navigator>
  )
}

function BookStack({ navigation }) {
  const searchIcon = <NavigationComponents.HeaderSearchIcon {...navigation} />;
  return(
    <Stack.Navigator screenOptions={{headerShadowVisible: false}}>
      <Stack.Screen
        name={APP_ROUTES.BOOKS.routeName}
        component={AllBooks}
        options={({navigation})=>({
          ...APP_ROUTES.BOOKS.options,
          // headerRight: () => searchIcon,
          headerLeft: ()=> <NavigationComponents.HeaderTitle title={APP_ROUTES.BOOKS.options.customTitle}/>
        })}
      />
      <Stack.Screen
        name={APP_ROUTES.BOOK_DETAILS.routeName}
        component={BookDetails}
        options={{
          ...APP_ROUTES.BOOK_DETAILS.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.BOOK_DETAILS.options.customTitle}/>
        }}
      />
      <Stack.Screen
        name={APP_ROUTES.PLAYER_SCREEN.routeName}
        component={NowPlaying}
        options={{
          ...APP_ROUTES.PLAYER_SCREEN.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.PLAYER_SCREEN.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
          // headerRight: () => <NavigationComponents.Options />,
        }}
      />
    </Stack.Navigator>
  )
}

function LibraryStack({ navigation }) {
  const searchIcon = <NavigationComponents.HeaderSearchIcon {...navigation} />;
  return(
    <Stack.Navigator screenOptions={{headerShadowVisible: false}}>
      <Stack.Screen
        name={APP_ROUTES.LIBRARY.routeName}
        component={Library}
        options={({navigation})=>({
          ...APP_ROUTES.LIBRARY.options,
          // headerRight: () => searchIcon,
          headerLeft: ()=> <NavigationComponents.HeaderTitle title={APP_ROUTES.LIBRARY.options.customTitle}/>
        })}
      />
      <Stack.Screen
        name={APP_ROUTES.BOOK_DETAILS.routeName}
        component={BookDetails}
        options={{
          ...APP_ROUTES.BOOK_DETAILS.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.BOOK_DETAILS.options.customTitle}/>
        }}
      />
      <Stack.Screen
        name={APP_ROUTES.PLAYER_SCREEN.routeName}
        component={NowPlaying}
        options={{
          ...APP_ROUTES.PLAYER_SCREEN.options,
          headerLeft: ()=> <NavigationComponents.HeaderBack title={APP_ROUTES.PLAYER_SCREEN.options.customTitle} textStyle={{color: Theme.themeFontColor}} imageStyle={{tintColor: Theme.themeFontColor}}/>,
          // headerRight: () => <NavigationComponents.Options />,
        }}
      />
    </Stack.Navigator>
  )
}

function UserRoutes() {
  return (
    <Tab.Navigator
      screenOptions={{ headerShown: false }}
      tabBar={props => <NavigationComponents.TabBar {...props} />}
      initialRouteName={APP_ROUTES.TABHOME.routeName}
    >
      <Tab.Screen name={APP_ROUTES.TABBOOKS.routeName} component={BookStack} 
        options={({navigation})=>({
          ...APP_ROUTES.TABBOOKS.options,
          // tabBarLabel: ({ focused }) => <NavigationComponents.HeaderTitle title={APP_ROUTES.TABBOOKS.options.title} style={focused ? activeLabelStyle : inactiveLabelStyle}/>,
          // tabBarIcon: ()=> <NavigationComponents.HeaderBack onPress={()=>navigation.goBack()}/>,
        })}
      />
      <Tab.Screen name={APP_ROUTES.TABLIBRARY.routeName} component={LibraryStack}
        options={({navigation})=>({
          ...APP_ROUTES.TABLIBRARY.options,
          // tabBarLabel: ({ focused }) => <NavigationComponents.HeaderTitle title={APP_ROUTES.TABLIBRARY.options.title} style={focused ? activeLabelStyle : inactiveLabelStyle}/>,
          // tabBarIcon: ()=> <NavigationComponents.HeaderBack onPress={()=>navigation.goBack()}/>,
        })}
      />
      <Tab.Screen name={APP_ROUTES.TABHOME.routeName} component={HomeStack}
        options={({navigation, route})=>{
          const routeName = getFocusedRouteNameFromRoute(route);
          return{
            ...APP_ROUTES.TABHOME.options,
            tabBarStyle: {
              display: routeName === APP_ROUTES.PLAYER_SCREEN.routeName ? 'none' : 'flex'
            }
          }
          // tabBarLabel: ({ focused }) => <NavigationComponents.HeaderTitle title={APP_ROUTES.TABHOME.options.title} style={focused ? activeLabelStyle : inactiveLabelStyle}/>,
          // tabBarIcon: ({ focused })=> <Image  source={Images.Home} style={{width: 25, height: 25, resizeMode: 'contain', tintColor: focused ? Theme.yellowBackground : Theme.white}}/>,
        }}
      />
      <Tab.Screen name={APP_ROUTES.TABCATEGORY.routeName} component={CategoryStack}
        options={({navigation})=>({
          ...APP_ROUTES.TABCATEGORY.options,
          // tabBarLabel: ({ focused }) => <NavigationComponents.HeaderTitle title={APP_ROUTES.TABCATEGORY.options.title} style={focused ? activeLabelStyle : inactiveLabelStyle}/>,
          // tabBarIcon: ()=> <NavigationComponents.HeaderBack onPress={()=>navigation.goBack()}/>,
        })}
      />
      <Tab.Screen name={APP_ROUTES.TABPROFILE.routeName} component={ProfileStack}
        options={({navigation})=>({
          ...APP_ROUTES.TABPROFILE.options,
          // tabBarLabel: ({ focused }) => <NavigationComponents.HeaderTitle title={APP_ROUTES.TABPROFILE.options.title} style={focused ? activeLabelStyle : inactiveLabelStyle}/>,
          // tabBarIcon: ()=> <NavigationComponents.HeaderBack onPress={()=>navigation.goBack()}/>,
        })}
      />
    </Tab.Navigator>
  );
}

export default UserRoutes;
