/**
 * @format
 */

import 'react-native-gesture-handler';

import {AppRegistry, Text, TextInput} from 'react-native';
import App from './src';
import {name as appName} from './app.json';
import TrackPlayer from 'react-native-track-player';

AppRegistry.registerComponent(appName, () => App);
TrackPlayer.registerPlaybackService(() => require('./service.js'))

// Text.defaultProps = Text.defaultProps || {};
// Text.defaultProps.allowFontScaling = false;
// TextInput.defaultProps.allowFontScaling = false;
